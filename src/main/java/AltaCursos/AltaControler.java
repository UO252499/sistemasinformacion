package AltaCursos;

import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;
import java.util.List;

import javax.swing.table.TableModel;

import cursos.AlumnosDisplayDTO;
import cursos.CursoDisplayDTO;
import cursos.CursoEntity;
import inscripcion.InscripcionModel;
import util.SwingUtil;

public class AltaControler {
	

	private AltaView vistaAlta;
	private InscripcionModel modelo;
	public AltaControler(InscripcionModel inscripcionModel, AltaView altaView) {
		vistaAlta = altaView;
		modelo = inscripcionModel;
		vistaAlta.controlador=this;
	}
	
	public void initController() {
		actualizarTablaAlta();
		//En el caso del mouse listener (para detectar seleccion de una fila) no es un interfaz funcional puesto que tiene varios metodos
		//ver discusion: https://stackoverflow.com/questions/21833537/java-8-lambda-expressions-what-about-multiple-methods-in-nested-class
		vistaAlta.table.addMouseListener(new MouseAdapter() {
			@Override
			public void mouseReleased(MouseEvent e) {
				//no usa mouseClicked porque al establecer seleccion simple en la tabla de carreras
				//el usuario podria arrastrar el raton por varias filas e interesa solo la ultima
				SwingUtil.exceptionWrapper(() -> updateDetail());
			}
		});
	}
	
	private void actualizarTablaAlta() {
		List<CursoDisplayDTO> cursos=modelo.getListaCursosTot();
		TableModel pmodel=SwingUtil.getTableModelFromPojos(cursos, new String[] {"id_curso", "titulo", "fecha_curso", "estado"});
		vistaAlta.table.setModel(pmodel);
	}

	public void abrirCurso(int selectedRow) {
		TableModel tablaContenido = vistaAlta.table.getModel();
		int cursoNumero = Integer.parseInt((String) tablaContenido.getValueAt(selectedRow, 0));
		CursoEntity curso = modelo.obtenCurso(cursoNumero);
		modelo.modificarCurso(vistaAlta.mostrarFormulario(curso));
		actualizarTablaAlta();
		
	}
	
	/**
	 * Al seleccionar un item de la tabla muestra el detalle con el valor del porcentaje de descuento
	 * de la carrera seleccinada y los valores de esta entidad
	 */
	public void updateDetail() {
		int selectedRow=vistaAlta.table.getSelectedRow();
		TableModel tablaContenido = vistaAlta.table.getModel();
		int id_curso=Integer.parseInt((String) tablaContenido.getValueAt(selectedRow, 0));
		if(modelo.admiteInscripcion(id_curso)) {
			List<AlumnosDisplayDTO> alumnos=modelo.getListaAlumnos(id_curso);
			if(alumnos.isEmpty()) {
				vistaAlta.NoTieneInscripciones();
				vistaAlta.setIngresoNoInscripcion();
			}
			else {
				TableModel tmodel=SwingUtil.getTableModelFromPojos(alumnos, new String[] {"nombre", "fecha_inscripcion", "estado", "precio"});
				int ingreso=modelo.getIngresosCurso(id_curso);
				vistaAlta.getTabAlumnos().setModel(tmodel);
				vistaAlta.setIngreso(String.valueOf(ingreso));
			}
		}
	}
}
