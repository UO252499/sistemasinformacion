package AltaCursos;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.FocusAdapter;
import java.awt.event.FocusEvent;
import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.JTable;
import javax.swing.ListSelectionModel;
import javax.swing.border.EmptyBorder;

import cursos.CursoEntity;

public class AltaView extends JFrame {
	
	/**
	 * 
	 */
	private static final long serialVersionUID = -4733947479616513730L;
	private JPanel contentPane;
	protected JTable table;
	protected AltaControler controlador;
	private JButton btnAbrir;
	private JTable tabAlumnos;
	private JLabel ingreso;
	
	public AltaView() {
		setTitle("Gestionar cursos");
		setLocationRelativeTo(null);
		setVisible(true);
		
		setDefaultCloseOperation(JFrame.DISPOSE_ON_CLOSE);
		setBounds(100, 100, 554, 430);
		contentPane = new JPanel();
		contentPane.setBorder(new EmptyBorder(5, 5, 5, 5));
		setContentPane(contentPane);
		contentPane.setLayout(null);
		setLocationRelativeTo(null);
		table = new JTable();
		{
			JLabel lblNewLabel = new JLabel("Cursos disponibles: ");
			lblNewLabel.setBounds(10, 11, 126, 21);
			contentPane.add(lblNewLabel);
		}
		{
			JScrollPane scrollPane = new JScrollPane();
			scrollPane.addFocusListener(new FocusAdapter() {
				@Override
				public void focusLost(FocusEvent e) {
					btnAbrir.setEnabled(false);
				}
			});
			scrollPane.setBounds(10, 43, 508, 125);
			contentPane.add(scrollPane);
			{
				table.addFocusListener(new FocusAdapter() {
					@Override
					public void focusGained(FocusEvent e) {
						btnAbrir.setEnabled(true);
					}
				});
				table.setSelectionMode(ListSelectionModel.SINGLE_SELECTION);
				scrollPane.setViewportView(table);
				table.setName("tabDetalle");
				table.setRowSelectionAllowed(true);
				{
					btnAbrir = new JButton("Abrir curso");
					btnAbrir.addActionListener(new ActionListener() {
						public void actionPerformed(ActionEvent e) {
							if (table.getSelectedRow()!=-1)
								controlador.abrirCurso(table.getSelectedRow());
							else
								FilaNoSeleccionada();
						}
					});
					btnAbrir.setEnabled(false);
					btnAbrir.setBounds(411, 179, 107, 23);
					contentPane.add(btnAbrir);
				}
				
				ingreso = new JLabel("##");
				ingreso.setBounds(177, 352, 49, 14);
				contentPane.add(ingreso);
				
				JLabel lblIngresos = new JLabel("Ingresos totales del curso:");
				lblIngresos.setBounds(10, 352, 152, 14);
				contentPane.add(lblIngresos);
				
				tabAlumnos = new JTable();
				tabAlumnos.setName("tabAlumnos");
				tabAlumnos.setSelectionMode(ListSelectionModel.SINGLE_SELECTION);
				tabAlumnos.setDefaultEditor(Object.class, null); //readonly
				
				JScrollPane tablePanel2 = new JScrollPane(tabAlumnos);
				tablePanel2.setBounds(10, 223, 508, 118);
				this.getContentPane().add(tablePanel2);
				
								
				JLabel lblInforCursos = new JLabel("Alumnos inscritos:");
				lblInforCursos.setBounds(10, 198, 126, 14);
				contentPane.add(lblInforCursos);
				table.setDefaultEditor(Object.class, null); //readonly;
			}
		}
	}
	
	private void FilaNoSeleccionada() {
		JOptionPane.showMessageDialog(this, "Seleccione un curso");							
	}

	public CursoEntity mostrarFormulario(CursoEntity curso) {
			Formulario formulario = new Formulario(curso, this, true);
			return formulario.showDialog();
	}
	
	public void NoAdmiteInscripcion() {
		JOptionPane.showMessageDialog(this, "Este curso no admite inscripciones");
	}
	
	public void NoTieneInscripciones() {
		JOptionPane.showMessageDialog(this, "Este curso aun no tiene inscripciones");
	}
	
	public JLabel getIngreso() {return ingreso;}
	public void setIngreso(String ingreso) {this.ingreso.setText(ingreso+"€");}
	public void setIngresoNoInscripcion() {this.ingreso.setText("N/A");	}
	public JTable getTabAlumnos() { return tabAlumnos; }
	public void setTabAlumnos(JTable tabAlumnos) { this.tabAlumnos = tabAlumnos; }
}
