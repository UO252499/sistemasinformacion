package AltaCursos;

import javax.swing.JDialog;

import cursos.CursoEntity;
import javax.swing.JLabel;
import javax.swing.JOptionPane;

import java.awt.Font;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;

import javax.swing.SwingConstants;
import javax.swing.text.MaskFormatter;
import javax.swing.JButton;
import javax.swing.JFormattedTextField;
import java.awt.event.FocusAdapter;
import java.awt.event.FocusEvent;
import java.awt.event.ActionListener;
import java.awt.event.ActionEvent;

public class Formulario extends JDialog {
	/**
	 * 
	 */
	private static final long serialVersionUID = -8223522432782005065L;
	private JFormattedTextField ftfPlazas;
	JFormattedTextField ftfFin;
	JFormattedTextField ftfInicio;
	SimpleDateFormat formato;
	CursoEntity curso;
	private Date fechaFin;
	private Date fechaInicio;

	public Formulario(CursoEntity curso, java.awt.Frame parent, boolean modal) {
		super(parent, modal);
		this.curso=curso;
		setAlwaysOnTop(true);
		setDefaultCloseOperation(JDialog.DISPOSE_ON_CLOSE);
		
		formato = new SimpleDateFormat("yyyy/MM/dd");
		
		setTitle("Formulario de apertura de curso");
		setBounds(100, 100, 387, 255);
		getContentPane().setLayout(null);
		{
			JLabel lblNewLabel = new JLabel("Curso: ");
			lblNewLabel.setBounds(10, 11, 46, 14);
			getContentPane().add(lblNewLabel);
		}
		{
			JLabel label = new JLabel(curso.getTitulo());
			label.setHorizontalAlignment(SwingConstants.CENTER);
			label.setFont(new Font("Tahoma", Font.BOLD, 14));
			label.setBounds(10, 27, 351, 35);
			getContentPane().add(label);
		}
		{
			JLabel lblInicioInscripciones = new JLabel("Inicio plazo inscripciones: ");
			lblInicioInscripciones.setBounds(10, 73, 199, 23);
			getContentPane().add(lblInicioInscripciones);
		}
		{
			JLabel lblFinInscripciones = new JLabel("Fin plazo inscripciones");
			lblFinInscripciones.setBounds(10, 107, 173, 23);
			getContentPane().add(lblFinInscripciones);
		}
		{
			JLabel lblPlazas = new JLabel("Plazas: ");
			lblPlazas.setBounds(10, 141, 146, 23);
			getContentPane().add(lblPlazas);
		}
		{
			JButton btnAbrirCurso = new JButton("Abrir curso");
			btnAbrirCurso.addActionListener(new ActionListener() {
				public void actionPerformed(ActionEvent e) {
					if (ftfInicio.getText().equals("")||ftfFin.getText().equals("")||ftfPlazas.getText().equals("")) 
						CamposSinRellenar();
					else {
						if(fechaFin.before(fechaInicio)) {
							FechasInconsistentes();
							resetTF(ftfInicio);
							resetTF(ftfFin);
						}
						curso.setFecha_inicio_inscripcion(ftfInicio.getText());
						curso.setFecha_fin_inscripcion(ftfFin.getText());
						curso.setPlazas(ftfPlazas.getText());
						curso.setEstado("Abierta");
						setVisible(false);
						dispose();
					}
				}
			});
			btnAbrirCurso.setBounds(242, 182, 119, 23);
			getContentPane().add(btnAbrirCurso);
		}
		{
			ftfPlazas = new JFormattedTextField();
			ftfPlazas.setBounds(216, 142, 33, 20);
			getContentPane().add(ftfPlazas);
		}
		{
			try {
				ftfFin = new JFormattedTextField(new MaskFormatter("####/##/##"));
				ftfFin.addFocusListener(new FocusAdapter() {

					@Override
					public void focusLost(FocusEvent e) {
						try {
							fechaFin = formato.parse(ftfFin.getText());
							if(fechaFin.before(fechaInicio)) {	
								FechasInconsistentes();
								resetTF(ftfFin);
							}
						} catch (ParseException e1) {
							NoEsUnaFecha();
							resetTF(ftfFin);
						}
					}
				});
			} catch (ParseException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
			ftfFin.setBounds(206, 108, 82, 20);
			getContentPane().add(ftfFin);
		}
		{
			try {
				ftfInicio = new JFormattedTextField(new MaskFormatter("####/##/##"));
				ftfInicio.addFocusListener(new FocusAdapter() {
					@Override
					public void focusLost(FocusEvent e) {
							try {
								fechaInicio = formato.parse(ftfInicio.getText());
								if(fechaInicio.before(new Date())) {
									FechaNoValida();
									resetTF(ftfInicio);
								}
							} catch (ParseException e1) {
								NoEsUnaFecha();
								resetTF(ftfInicio);
							}
					}
				});
			} catch (ParseException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
			ftfInicio.setBounds(206, 74, 82, 20);
			getContentPane().add(ftfInicio);
		}
	}
	private void FechaNoValida() {
		JOptionPane.showMessageDialog(this, "La fecha debe ser posterior a la actual");		
		
	}
	
	private void NoEsUnaFecha() {
		JOptionPane.showMessageDialog(this, "No es una fecha válida");	
		
	}
	private void CamposSinRellenar() {
		JOptionPane.showMessageDialog(this, "Rellene todos los campos");	
		
	}
	private void FechasInconsistentes() {
		JOptionPane.showMessageDialog(this, "La fecha de fin no puede ser anterior a la fecha de inicio");	
		
	}
	private void resetTF(JFormattedTextField TF) {
		TF.setText("");
		TF.requestFocus();
	}
	public CursoEntity showDialog() {
		setVisible(true);
	    return curso;
	}

}
