package AsignarPericialesPeritos;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;
import java.util.Date;
import java.util.List;

import javax.swing.JOptionPane;
import javax.swing.table.TableModel;

import InscripcionPeritos.InscripcionPeritosDisplayDTO;
import SolicitudInformePericial.SolicitudInformePericialDisplayDTO;
import util.SwingUtil;

public class AsignarPericialesPeritosController {
	private AsignarPericialesPeritosModel model;
	private AsignarPericialesPeritosView view;
	private int perito;
	private int pericial;
	
	public AsignarPericialesPeritosController(AsignarPericialesPeritosModel m, AsignarPericialesPeritosView v) {
		this.model = m;
		this.view = v;
		//no hay inicializacion especifica del modelo, solo de la vista
		this.initView();
	}
	
	public void initView() {
		this.getListaPeritos();
		this.getListaPericiales();
		
		//Abre la ventana (sustituye al main generado por WindowBuilder)
		view.getFrame().setVisible(true); 
	}
	
	public void initController() {
		Date fechaActual = new Date();
		view.getBAsignar().addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				if(view.getTPeritos().getSelectedRow()==-1) {
					perito=Integer.parseInt(String.valueOf(view.getTPeritos().getValueAt(0, 5)));
					JOptionPane.showMessageDialog(null, "Se ha asignado el pericial de "+String.valueOf(view.getTPericiales().getValueAt(view.getTPericiales().getSelectedRow(), 0))+" al perito con numero "+
					String.valueOf(view.getTPeritos().getValueAt(0, 0))+" el "+fechaActual);
				}else {
					perito=Integer.parseInt(String.valueOf(view.getTPeritos().getValueAt(view.getTPeritos().getSelectedRow(), 5)));
					JOptionPane.showMessageDialog(null, "Se ha asignado el pericial de "+String.valueOf(view.getTPericiales().getValueAt(view.getTPericiales().getSelectedRow(), 0))+" al perito con numero "+
					String.valueOf(view.getTPeritos().getValueAt(view.getTPeritos().getSelectedRow(), 0))+" el "+fechaActual);
				}
				pericial=Integer.parseInt(String.valueOf(view.getTPericiales().getValueAt(view.getTPericiales().getSelectedRow(), 4)));
				model.asignarPericia(perito, pericial, fechaActual);
				model.asignadoPerito(perito);
				model.asignadoPericial(pericial);
				getListaPericiales();
				getListaPeritos();
				view.getBAsignar().setEnabled(false);
			}
		});
		view.getTPeritos().addMouseListener(new MouseAdapter() {
			@Override
			public void mouseClicked(MouseEvent e) {
				activarBoton();
			}
		});
		
		view.getTPericiales().addMouseListener(new MouseAdapter() {
			@Override
			public void mouseClicked(MouseEvent e) {
				activarBoton();
			}
		});
		
		view.getBSalir().addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				view.getFrame().setVisible(false);
			}
		});
	}
	
	public void getListaPeritos() {
		List<InscripcionPeritosDisplayDTO> peritos=model.getListaPeritos();
		TableModel tmodel=SwingUtil.getTableModelFromPojos(peritos, new String[] {"numero_colegiado","nombre", "apellidos","experiencia","fecha","id_perito"});
		view.getTPeritos().setModel(tmodel);
		SwingUtil.autoAdjustColumns(view.getTPeritos());
	}
	
	public void getListaPericiales() {
		List<SolicitudInformePericialDisplayDTO> periciales=model.getListaPericiales();
		TableModel tmodel=SwingUtil.getTableModelFromPojos(periciales, new String[] {"nombre", "descripcion","fechaemision","fechalimite","id_pericial"});
		view.getTPericiales().setModel(tmodel);
		SwingUtil.autoAdjustColumns(view.getTPericiales());
	}
	
	public void activarBoton() {
		if(view.getTPericiales().getSelectedRow()>-1) {
			view.getBAsignar().setEnabled(true);
		}else{
			view.getBAsignar().setEnabled(false);
		}
	}
	
}
