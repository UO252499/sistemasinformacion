package AsignarPericialesPeritos;

import java.util.Date;
import java.util.List;

import InscripcionPeritos.InscripcionPeritosDisplayDTO;
import SolicitudInformePericial.SolicitudInformePericialDisplayDTO;
import util.Database;
import util.Util;

public class AsignarPericialesPeritosModel {
	
	private Database db=new Database();
	
	public List<InscripcionPeritosDisplayDTO> getListaPeritos() {
		String sql="SELECT c.Numero_colegiado, c.Nombre, c.Apellidos, p.Experiencia, p.Fecha, p.id_perito "
				+ "FROM perito p inner JOIN colegiado c on c.numero_colegiado=p.numero_colegiado"
				+ " WHERE p.estado=='Pendiente' ORDER BY p.orden";
		return db.executeQueryPojo(InscripcionPeritosDisplayDTO.class, sql);
	}
	
	public List<SolicitudInformePericialDisplayDTO> getListaPericiales() {
		String sql="SELECT nombre, descripcion, fechaemision, fechalimite, id_pericial "
				+ "FROM  pericial "
				+ "WHERE estado='Pendiente' ORDER BY fechaemision, fechalimite";
		return db.executeQueryPojo(SolicitudInformePericialDisplayDTO.class, sql);
	}
	
	public void asignarPericia(int id_perito,int id_pericial,Date fecha) {
		int idPercia=obtenerId();
		String date=Util.dateToIsoString(fecha);
		String sql="insert into pericia (id_pericia,id_perito,id_pericial,fecha) VALUES (?,?,?,?)";
		db.executeUpdate(sql,idPercia,id_perito,id_pericial,date);
	}
	
	public int obtenerId() {
		String sql="SELECT MAX(id_pericia) FROM pericia";
		List<Object[]>rows=db.executeQueryArray(sql);
		if(rows.get(0)[0]==null) {
			return 1;
		}else {
			return (Integer)rows.get(0)[0]+1;
		}
	}
	
	public int obtenerOrden() {
		String sql="SELECT MAX(orden) FROM perito";
		List<Object[]>rows=db.executeQueryArray(sql);
		if(rows.get(0)[0]==null) {
			return 1;
		}else {
			return (Integer)rows.get(0)[0]+1;
		}
	}

	public void asignadoPerito(int perito) {
		int orden=obtenerOrden();
		String sql="UPDATE perito SET orden=? WHERE id_perito=?";
		db.executeUpdate(sql,orden,perito);
		
	}
	
	public void asignadoPericial(int pericial) {
		String sql="UPDATE pericial SET estado='Asignado' WHERE id_pericial=?";
		db.executeUpdate(sql,pericial);
	}
}
