package AsignarPericialesPeritos;

import javax.swing.JFrame;
import javax.swing.JScrollPane;
import javax.swing.JTable;
import javax.swing.ListSelectionModel;
import javax.swing.JButton;
import java.awt.event.ActionListener;
import java.awt.event.ActionEvent;
import javax.swing.JLabel;
import java.awt.Font;
import javax.swing.JSeparator;
import javax.swing.SwingConstants;

public class AsignarPericialesPeritosView {
	private JFrame frame;
	private JScrollPane SPPeritos;
	private JScrollPane SPPericiales;
	private JTable TPeritos;
	private JTable TPericiales;
	private JButton BAsignar;
	private JButton BSalir;
	
	/**
	 * Create the application.
	 */
	public AsignarPericialesPeritosView() {
		initialize();
	}

	/**
	 * Initialize the contents of the frame.
	 */
	private void initialize() {
		frame = new JFrame();
		frame.setTitle("Asignar Periciales a Peritos");
		frame.setName("Asignar");
		frame.setBounds(0, 0, 600, 450);
		frame.setDefaultCloseOperation(JFrame.DISPOSE_ON_CLOSE);
		frame.getContentPane().setLayout(null);
		frame.setLocationRelativeTo(null);
		
		TPeritos = new JTable();
		TPeritos.setName("Tabla Peritos ");
		TPeritos.setSelectionMode(ListSelectionModel.SINGLE_SELECTION);
		TPeritos.setDefaultEditor(Object.class, null); //readonly
		SPPeritos = new JScrollPane(TPeritos);
		SPPeritos.setBounds(300, 118, 264, 161);
		frame.getContentPane().add(SPPeritos);
		
		TPericiales = new JTable();
		TPericiales.setName("Tabla Periciales ");
		TPericiales.setSelectionMode(ListSelectionModel.SINGLE_SELECTION);
		TPericiales.setDefaultEditor(Object.class, null); //readonly
		SPPericiales = new JScrollPane(TPericiales);
		SPPericiales.setBounds(12, 118, 264, 161);
		frame.getContentPane().add(SPPericiales);
		
		BAsignar = new JButton("Asignar");
		BAsignar.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
			}
		});
		BAsignar.setBounds(134, 315, 123, 36);
		frame.getContentPane().add(BAsignar);
		BAsignar.setEnabled(false);
		
		JLabel LPeritos = new JLabel("Peritos");
		LPeritos.setHorizontalAlignment(SwingConstants.LEFT);
		LPeritos.setFont(new Font("Verdana", Font.PLAIN, 17));
		LPeritos.setBounds(392, 82, 82, 27);
		frame.getContentPane().add(LPeritos);
		
		JLabel lblPerciales = new JLabel("Perciales");
		lblPerciales.setFont(new Font("Verdana", Font.PLAIN, 17));
		lblPerciales.setBounds(106, 86, 92, 19);
		frame.getContentPane().add(lblPerciales);
		
		JLabel LTitulo = new JLabel("Asignar pericailes a peritos");
		LTitulo.setHorizontalAlignment(SwingConstants.CENTER);
		LTitulo.setFont(new Font("Verdana", Font.BOLD, 18));
		LTitulo.setBounds(134, 13, 298, 27);
		frame.getContentPane().add(LTitulo);
		
		JSeparator separator = new JSeparator();
		separator.setBounds(0, 38, 582, 2);
		frame.getContentPane().add(separator);
		
		BSalir = new JButton("Salir");
		BSalir.setBounds(309, 315, 123, 36);
		frame.getContentPane().add(BSalir);
		
		
	}
	
	public JFrame getFrame() { return this.frame; }
	public JScrollPane getSPPeritos() { return this.SPPeritos; }
	public JScrollPane getSPPericiales() { return this.SPPericiales; }
	public JTable getTPeritos() { return this.TPeritos; }
	public JTable getTPericiales() { return this.TPericiales; }
	public JButton getBAsignar() { return this.BAsignar; }
	public JButton getBSalir() { return this.BSalir; }
}
