package Colegiado;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.FocusEvent;
import java.awt.event.FocusListener;
import java.util.Date;

import javax.swing.JOptionPane;

public class ColegiadoAltaController {
	private ColegiadoAltaModel model;
	private ColegiadoAltaView view;
	String nl = System.getProperty("line.separator");
	
	public ColegiadoAltaController(ColegiadoAltaModel Model, ColegiadoAltaView View) {
		this.model=Model;
		this.view=View;
		
		this.initView();
	}
	
	public void initView() {
		//Abre la ventana (sustituye al main generado por WindowBuilder)
		view.getFrame().setVisible(true);
	}
	
	public void initController() {
		Date fechaActual = new Date();
		
		
		//Comprobacion de DNI
		view.getDNI().addFocusListener(new FocusListener() {
			@Override
			public void focusLost(FocusEvent e) {
				if(!view.getDNI().getText().isEmpty()) {
					if(!validarDNI()) {
						JOptionPane.showMessageDialog(null, "DNI mal introducido","Valor mal introducido",JOptionPane.WARNING_MESSAGE);
						view.getDNI().setText(null);
					}else {
						view.HabilitarBoton();
					}
				}
			}

			@Override
			public void focusGained(FocusEvent e) {
			}
		});
		
		//Comprobacion del año obtenido del titulo 
		view.getAñoObtencion().addFocusListener(new FocusListener() {
			@Override
			public void focusLost(FocusEvent e) {
				if(!view.getAñoObtencion().getText().isEmpty()) {
					if(!view.año()) {
						JOptionPane.showMessageDialog(null, "Año mal introducido","Valor mal introducido",JOptionPane.WARNING_MESSAGE);
						view.getAñoObtencion().setText(null);
					}
				}
			}

			@Override
			public void focusGained(FocusEvent e) {
			}
		});
		
		//Comprobacion del telefono
		view.getTelefono().addFocusListener(new FocusListener() {
			@Override
			public void focusLost(FocusEvent e) {
				if(!view.getTelefono().getText().isEmpty()) {
					if((view.getTelefono().getText().length()!=9)) {
						JOptionPane.showMessageDialog(null, "Telefono mal introducido","Valor mal introducido",JOptionPane.WARNING_MESSAGE);
						view.getTelefono().setText(null);
					}
				}
			}

			@Override
			public void focusGained(FocusEvent e) {
			}
		});
		
		//Comprobaciones al pulsar el boton Aceptar
		view.getBAceptar().addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent evt) {
				if(model.existeDNI(view.getDNI().getText())) {
					JOptionPane.showMessageDialog(null, "El solicitante con DNI: "+view.getDNI().getText()
					+ " ya ha realizado una solicitud","Solicitud ya creada",JOptionPane.WARNING_MESSAGE);
				}else {
					String estado;
					boolean esColegiado;
					if (view.tipoSocio().getSelectedItem()=="Colegiado") {
						estado="Pendiente";
						esColegiado=true;
						JOptionPane.showMessageDialog(null, "El solicitante "+view.getNombre().getText() +" "+view.getApellidos().getText()+" se encuentra en estado pendiente"+ nl
								+" Realizado el "+fechaActual);
					}else {
						estado="PreColegiado";
						esColegiado=false;
						JOptionPane.showMessageDialog(null, "El solicitante "+view.getNombre().getText() +" "+view.getApellidos().getText()+" ha sido aceptado"+ nl
								+" Realizado el "+fechaActual+nl
								+"Numero de Precolegiado: "+"P"+String.valueOf(model.obtenerNColegiado(esColegiado)+1));
					}
					model.nuevoColegiado(view.getDNI().getText(), view.getNombre().getText(),
							view.getApellidos().getText(), view.getDireccion().getText(), view.getPoblacion().getText(),
							Integer.parseInt(view.getTelefono().getText()), view.getTitulacion().getText(),
							view.getCentro().getText(), Integer.parseInt(view.getAñoObtencion().getText()),
							view.getCuentaBancaria().getText(),fechaActual,estado,esColegiado);
				}
				view.getDNI().setText(null);
				view.getNombre().setText(null);
				view.getApellidos().setText(null);
				view.getDireccion().setText(null);
				view.getPoblacion().setText(null);
				view.getTelefono().setText(null);
				view.getTitulacion().setText(null);
				view.getCentro().setText(null);
				view.getAñoObtencion().setText(null);
				view.getAñoObtencion().setText(null);
				view.getCuentaBancaria().setText(null);
				view.getBAceptar().setEnabled(false);
			}
			
		});
	
	}
	
	public boolean validarDNI() {
		
		String letraMayuscula = ""; //Guardaremos la letra introducida en formato mayúscula
		
		// Aquí excluimos cadenas distintas a 9 caracteres que debe tener un dni y también si el último caracter no es una letra
		if((view.getDNI().getText().length() != 9) || (Character.isLetter(view.getDNI().getText().charAt(8)) == false)) {
			return false;
		}


		// Al superar la primera restricción, la letra la pasamos a mayúscula
		letraMayuscula = (view.getDNI().getText().substring(8)).toUpperCase();
		
		// Por último validamos que sólo tengo 8 dígitos entre los 8 primeros caracteres y que la letra introducida es igual a la de la ecuación
		// Llamamos a los métodos privados de la clase soloNumeros() y letraDNI()
		if((soloNumeros() == true) && (letraDNI().equals(letraMayuscula))) {
			return true;
		}
		else {
			return false;
		}
	}
	
	private boolean soloNumeros() {

		int i, j = 0;
		String numero = ""; // Es el número que se comprueba uno a uno por si hay alguna letra entre los 8 primeros dígitos
		String miDNI = ""; // Guardamos en una cadena los números para después calcular la letra
		String[] unoNueve = {"0","1","2","3","4","5","6","7","8","9"};

		for(i = 0; i < view.getDNI().getText().length() - 1; i++) {
			numero = view.getDNI().getText().substring(i, i+1);

			for(j = 0; j < unoNueve.length; j++) {
				if(numero.equals(unoNueve[j])) {
					miDNI += unoNueve[j];
				}
			}
		}

		if(miDNI.length() != 8) {
			return false;
		}
		else {
			return true;
		}
	}
	
	private String letraDNI() {
		// El método es privado porque lo voy a usar internamente en esta clase, no se necesita fuera de ella

		// pasar miNumero a integer
		int miDNI = Integer.parseInt(view.getDNI().getText().substring(0,8));
		int resto = 0;
		String miLetra = "";
		String[] asignacionLetra = {"T", "R", "W", "A", "G", "M", "Y", "F", "P", "D", "X", "B", "N", "J", "Z", "S", "Q", "V", "H", "L", "C", "K", "E"};

		resto = miDNI % 23;

		miLetra = asignacionLetra[resto];

		return miLetra;
	}

}
