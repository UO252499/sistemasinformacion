package Colegiado;

import inscripcion.Persona;

public class ColegiadoAltaEntity extends Persona{
	private String numero_colegiado;
	private String DNI;
	private String Nombre;
	private String Apellidos;
	private String Direccion;
	private String Poblacion;
	private int Telefono;
	private String Titulacion;
	private String Centro;
	private int AñoObtencion;
	private String CuentaBancaria;
	private String estado;
	
	
	public ColegiadoAltaEntity(){
	}
	public ColegiadoAltaEntity(String dNI) {
		super();
		DNI = dNI;
	}
	
	public String getDNI() { return DNI; }
	public void setDNI(String dni) { DNI = dni; }
	
	public String getNombre() { return Nombre; }
	public void setNombre(String nombre) { Nombre = nombre; }
	
	public String getApellidos() { return Apellidos; }
	public void setApellidos(String apellidos) { Apellidos = apellidos; }
	
	public String getDireccion() { return Direccion; }
	public void setDireccion(String direccion) { Direccion = direccion; }
	
	public String getPoblacion() { return Poblacion; }
	public void setPoblacion(String poblacion) { Poblacion = poblacion; }
	
	public int getTelefono() { return Telefono; }
	public void setTelefono(int telefono) { Telefono = telefono; }
	
	public String getTitulacion() { return Titulacion; }
	public void setTitulacion(String titulacion) { Titulacion = titulacion; }
	
	public String getCentro() { return Centro; }
	public void setCentro(String centro) { Centro = centro; }
	
	public int getAñoObtencion() { return AñoObtencion; }
	public void setAñoObtencion(int añoObtencion) { AñoObtencion = añoObtencion; }
	
	public String getCuentaBancaria() { return CuentaBancaria; }
	public void setCuentaBancaria(String cuentaBancaria) { CuentaBancaria = cuentaBancaria; }

	public String getNumero_Colegiado() { return numero_colegiado; }
	public void setNumero_Colegiado(String nc) { numero_colegiado=nc; }
	public String getEstado() {
		return estado;
	}
	public void setEstado(String estado) {
		this.estado = estado;
	}
}
