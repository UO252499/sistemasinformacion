package Colegiado;

import java.util.Calendar;
import java.util.Date;
import java.util.List;

import util.Database;
import util.Util;

public class ColegiadoAltaModel {
	private Database db= new Database();
	private String ncS;
	
	public void nuevoColegiado(String dni,String nombre,String apellidos, String direccion, String poblacion,int telefono,String titulacion,String centro,int añoobtencion,
			String cuentabancaria,Date fecha,String estado, boolean esColegiado) {
		if(esColegiado) {
			ncS=String.valueOf(obtenerNColegiado(esColegiado)+1);
		}else {
			ncS="P"+String.valueOf(obtenerNColegiado(esColegiado)+1);
		}
		String date=Util.dateToIsoString(fecha);
		String sql="insert into colegiado(numero_colegiado,"
				+ "dni,nombre,apellidos,direccion,poblacion,telefono,"
				+ "titulacion,centro,añoobtencion,cuentabancaria,fecha,estado) "
				+ "values(?,?,?,?,?,?,?,?,?,?,?,?,?)";
		db.executeUpdate(sql,ncS,dni,nombre,apellidos,direccion,poblacion,telefono,titulacion,centro,añoobtencion,cuentabancaria,date,estado);
	}
	
	
	public int obtenerNColegiado(boolean esColegiado) {
		String sql;
		if(!esColegiado) {
			sql="SELECT MAX(numero_colegiado) FROM colegiado WHERE estado=='PreColegiado'";
		}else {
			sql="SELECT MAX(numero_colegiado) FROM colegiado WHERE LENGTH(numero_colegiado)==10";
		}
		Calendar fecha = Calendar.getInstance();
        int año = fecha.get(Calendar.YEAR);
		List<Object[]>rows=db.executeQueryArray(sql);
		if (rows.get(0)[0]==null) {
			return año*1000000;
		}
		else {
			int NC;
			if(esColegiado) {
				NC= Integer.parseInt((String) rows.get(0)[0]);
			}else {
				String SNC=(String) rows.get(0)[0];
				NC= Integer.parseInt(SNC.substring(1));
			}
			if(año!=NC/(1000000)) {
				return año*1000000;
			}else {
				return NC;
			}
		}
	}


	public boolean existeDNI(String DNI) {
		String sql= "SELECT MAX(dni) FROM colegiado WHERE dni=?";
		List<Object[]>rows=db.executeQueryArray(sql,DNI);
		if (rows.get(0)[0]==null) {
			return false;
		}
		else {
			return true;
		}
	}
	
	public String ObtenerColegiado(String DNI) {
		String sql= "SELECT estado FROM colegiado WHERE dni=?";
		List<Object[]>rows=db.executeQueryArray(sql,DNI);
		return (String) rows.get(0)[0];
	}
}
