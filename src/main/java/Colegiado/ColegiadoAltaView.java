package Colegiado;

import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JTextField;
import javax.swing.SwingConstants;

import java.awt.event.KeyAdapter;
import java.awt.event.KeyEvent;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.FocusEvent;
import java.awt.event.FocusListener;
import java.awt.Font;

import javax.swing.JComboBox;
import javax.swing.JSeparator;
import java.awt.event.ItemListener;
import java.awt.event.ItemEvent;


public class ColegiadoAltaView {
	private JFrame frame;
	private JComboBox<?> CBColegiado;
	private JLabel LDNI;
	private JTextField TFDNI;
	private JLabel LNombre;
	private JTextField TFNombre;
	private JLabel LApellidos;
	private JTextField TFApellidos;
	private JLabel LDireccion;
	private JTextField TFDireccion;
	private JLabel LPoblacion;
	private JTextField TFPoblacion;
	private JLabel LTelefono;
	private JTextField TFTelefono;
	private JLabel LTitulacion;
	private JTextField TFTitulacion;
	private JLabel LCentro;
	private JTextField TFCentro;
	private JLabel LAñoObtencion;
	private JTextField TFAñoObtencion;
	private JButton BAceptar;
	private JButton BCancelar;
	private JLabel LTitulo;
	private JLabel LCuentaBancaria;
	private JTextField TFCuentaBancaria;

	/**
	 * Create the application.
	 */
	public ColegiadoAltaView() {
		initialize();
	}

	/**
	 * Initialize the contents of the frame.
	 */
	private void initialize() {
		frame = new JFrame();
		frame.setTitle("Colegiado Alta");
		frame.setName("Colegiado");
		frame.setBounds(0, 0, 500, 450);
		frame.setDefaultCloseOperation(JFrame.DISPOSE_ON_CLOSE);
		frame.getContentPane().setLayout(null);
		frame.setLocationRelativeTo(null);
		
		LTitulo = new JLabel("Solicitud de Alta Colegiado");
		LTitulo.setHorizontalAlignment(SwingConstants.CENTER);
		LTitulo.setFont(new Font("Verdana Pro", Font.BOLD, 18));
		frame.getContentPane().add(LTitulo);
		LTitulo.setBounds(0, 16, 478, 20);
		
		JSeparator separator = new JSeparator();
		separator.setBounds(0, 44, 478, 11);
		frame.getContentPane().add(separator);
		
		JLabel LComo = new JLabel("Dar de alta como:");
		LComo.setBounds(84, 63, 143, 20);
		frame.getContentPane().add(LComo);
		
		String[] CB = {"Colegiado","Pre Colegiado"};
		CBColegiado = new JComboBox<Object>(CB);
		CBColegiado.addItemListener(new ItemListener() {
			public void itemStateChanged(ItemEvent e) {
				if(CBColegiado.getSelectedItem()=="Colegiado") {
					LAñoObtencion.setText("Año del titulo");
					LAñoObtencion.setBounds(10, 246, 102, 20);
				}else {
					LAñoObtencion.setText("<html>Año de inico<br>de estudios</html>");
					LAñoObtencion.setBounds(10, 225, 92, 63);
				}
				if(!año()) {
					TFAñoObtencion.setText(null);
				}
				HabilitarBoton();
			}
		});
		CBColegiado.setBounds(242, 60, 109, 26);
		frame.getContentPane().add(CBColegiado);
		
		//DNI
		LDNI = new JLabel("DNI");
		frame.getContentPane().add(LDNI);
		LDNI.setBounds(15, 138, 69, 20);
		
		TFDNI = new JTextField();
		TFDNI.addKeyListener(new KeyAdapter() {
			@Override
			public void keyReleased(KeyEvent e) {
				BAceptar.setEnabled(false);
			}
			@Override
			public void keyTyped(KeyEvent e) {
				if(TFDNI.getText().length()>=9) {
					e.consume();
				}
			}
		});
		frame.getContentPane().add(TFDNI);
		TFDNI.setBounds(84, 138, 125, 20);
		
		//Nombre
		LNombre = new JLabel("Nombre");
		frame.getContentPane().add(LNombre);
		LNombre.setBounds(15, 102, 69, 20);
		
		TFNombre = new JTextField();
		TFNombre.addKeyListener(new KeyAdapter() {
			public void keyTyped(KeyEvent evt) {
				char c=evt.getKeyChar();
		        if (Character.isDigit(c)){
		            evt.consume();
		            JOptionPane.showMessageDialog(null, "Ingresar solo letras","Valor mal introducido",JOptionPane.WARNING_MESSAGE);
		        }
			}
			@Override
			public void keyReleased(KeyEvent e) {
				HabilitarBoton();
			}
		});
		frame.getContentPane().add(TFNombre);
		TFNombre.setBounds(84, 102, 125, 20);
		
		//Apellidos
		LApellidos = new JLabel("Apellidos");
		frame.getContentPane().add(LApellidos);
		LApellidos.setBounds(224, 102, 69, 20);
		
		TFApellidos = new JTextField();
		TFApellidos.addKeyListener(new KeyAdapter() {
			public void keyTyped(KeyEvent evt) {
				char c=evt.getKeyChar();
		        if (Character.isDigit(c)){
		            evt.consume();
		            JOptionPane.showMessageDialog(null, "Ingresar solo letras","Valor mal introducido",JOptionPane.WARNING_MESSAGE);
		        }
			}
			@Override
			public void keyReleased(KeyEvent e) {
				HabilitarBoton();
			}
		});
		frame.getContentPane().add(TFApellidos);
		TFApellidos.setBounds(320, 102, 143, 20);
		
		//Direccion
		LDireccion = new JLabel("Direccion");
		frame.getContentPane().add(LDireccion);
		LDireccion.setBounds(224, 138, 69, 20);
		
		TFDireccion = new JTextField();
		TFDireccion.addKeyListener(new KeyAdapter() {
			@Override
			public void keyReleased(KeyEvent e) {
				HabilitarBoton();
			}
		});
		frame.getContentPane().add(TFDireccion);
		TFDireccion.setBounds(320, 138, 143, 20);
		
		//Poblacion
		LPoblacion = new JLabel("Poblacion");
		frame.getContentPane().add(LPoblacion);
		LPoblacion.setBounds(10, 174, 69, 20);
		
		TFPoblacion = new JTextField();
		TFPoblacion.addKeyListener(new KeyAdapter() {
			public void keyTyped(KeyEvent evt) {
				char c=evt.getKeyChar();
		        if (Character.isDigit(c)){
		            evt.consume();
		            JOptionPane.showMessageDialog(null, "Ingresar solo letras","Valor mal introducido",JOptionPane.WARNING_MESSAGE);
		        }
			}
			@Override
			public void keyReleased(KeyEvent e) {
				HabilitarBoton();
			}
		});
		frame.getContentPane().add(TFPoblacion);
		TFPoblacion.setBounds(84, 174, 125, 20);
		
		//Telefono
		LTelefono = new JLabel("Telefono");
		frame.getContentPane().add(LTelefono);
		LTelefono.setBounds(224, 174, 69, 20);
		
		TFTelefono = new JTextField();
		TFTelefono.addKeyListener(new KeyAdapter() {
			public void keyTyped(KeyEvent evt) {
				char c=evt.getKeyChar();
		        if (Character.isLetter(c)){
		            evt.consume();
		            JOptionPane.showMessageDialog(null, "Ingresar solo numeros","Valor mal introducido",JOptionPane.WARNING_MESSAGE);
		        }
		        if(TFTelefono.getText().length()>=9) {
					evt.consume();
				}
		        HabilitarBoton();
			}
			@Override
			public void keyReleased(KeyEvent arg0) {
				HabilitarBoton();
			}
		});
		frame.getContentPane().add(TFTelefono);
		TFTelefono.setBounds(320, 174, 143, 20);
		
		//Titulacion
		LTitulacion = new JLabel("Titulacion");
		frame.getContentPane().add(LTitulacion);
		LTitulacion.setBounds(10, 210, 69, 20);
		
		TFTitulacion = new JTextField();
		TFTitulacion.addKeyListener(new KeyAdapter() {
			public void keyTyped(KeyEvent evt) {
				char c=evt.getKeyChar();
		        if (Character.isDigit(c)){
		            evt.consume();
		            JOptionPane.showMessageDialog(null, "Ingresar solo letras","Valor mal introducido",JOptionPane.WARNING_MESSAGE);
		        }
			}
			@Override
			public void keyReleased(KeyEvent e) {
				HabilitarBoton();
			}
		});
		frame.getContentPane().add(TFTitulacion);
		TFTitulacion.setBounds(84, 210, 125, 20);
		
		
		//Centro
		LCentro = new JLabel("Centro");
		frame.getContentPane().add(LCentro);
		LCentro.setBounds(224, 210, 69, 20);
		
		TFCentro = new JTextField();
		TFCentro.addKeyListener(new KeyAdapter() {
			public void keyTyped(KeyEvent evt) {
				char c=evt.getKeyChar();
		        if (Character.isDigit(c)){
		            evt.consume();
		            JOptionPane.showMessageDialog(null, "Ingresar solo letras","Valor mal introducido",JOptionPane.WARNING_MESSAGE);
		        }
			}
			@Override
			public void keyReleased(KeyEvent e) {
				HabilitarBoton();
			}
		});
		frame.getContentPane().add(TFCentro);
		TFCentro.setBounds(320, 210, 143, 20);
		
		//Año de obtencion del titulo
		LAñoObtencion = new JLabel("Año del titulo");
		frame.getContentPane().add(LAñoObtencion);
		LAñoObtencion.setBounds(10, 246, 102, 20);
		
		
		TFAñoObtencion = new JTextField();
		TFAñoObtencion.addKeyListener(new KeyAdapter() {
			public void keyTyped(KeyEvent evt) {
				char c=evt.getKeyChar();
		        if (Character.isLetter(c)){
		            evt.consume();
		            JOptionPane.showMessageDialog(null, "Ingresar solo numeros","Valor mal introducido",JOptionPane.WARNING_MESSAGE);
		        }
		        if(TFAñoObtencion.getText().length()>=4) {
					evt.consume();
				}
		        HabilitarBoton();
			}
			@Override
			public void keyReleased(KeyEvent e) {
				HabilitarBoton();
			}
		});
		frame.getContentPane().add(TFAñoObtencion);
		TFAñoObtencion.setBounds(112, 246, 57, 20);
		
		//Cuenta Bancaria
		LCuentaBancaria = new JLabel("Cuenta Bancaria");
		frame.getContentPane().add(LCuentaBancaria);
		LCuentaBancaria.setBounds(184, 246, 119, 20);
		
		TFCuentaBancaria = new JTextField();
		frame.getContentPane().add(TFCuentaBancaria);
		TFCuentaBancaria.setBounds(302, 246, 161, 20);
		TFCuentaBancaria.addKeyListener(new KeyAdapter() {
			public void keyTyped(KeyEvent evt) {
		        if(TFCuentaBancaria.getText().length()>=20) {
					evt.consume();
				}
		        char c=evt.getKeyChar();
		        if (Character.isLetter(c)){
		            evt.consume();
		            JOptionPane.showMessageDialog(null, "Ingresar solo numeros","Valor mal introducido",JOptionPane.WARNING_MESSAGE);
		        }
		        HabilitarBoton();
			}
		});
		TFCuentaBancaria.addFocusListener(new FocusListener() {
			@Override
			public void focusLost(FocusEvent e) {
				if(!TFCuentaBancaria.getText().isEmpty()) {
					if(TFCuentaBancaria.getText().length()<20) {
						JOptionPane.showMessageDialog(null, "Cuenta Bancaria mal introducida","Valor mal introducido",JOptionPane.WARNING_MESSAGE);
						HabilitarBoton();
					}else {
						HabilitarBoton();
					}
				}
			}

			@Override
			public void focusGained(FocusEvent e) {
			}
		});
		
		
		//Botones
		BAceptar = new JButton("Aceptar");
		BAceptar.setEnabled(false);
		frame.getContentPane().add(BAceptar);
		BAceptar.setBounds(107, 324, 102, 34);
		
		BCancelar = new JButton("Salir");
		frame.getContentPane().add(BCancelar);
		BCancelar.setBounds(269, 324, 111, 34);
	

		BCancelar.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent evt) {
				frame.setVisible(false);
			}
		});
	}

	//Getters y Setters anyadidos para acceso desde el controlador (repersentacion compacta)
	public JFrame getFrame() { return this.frame; }
	public JComboBox<?> tipoSocio() { return this.CBColegiado; }
	public JTextField getDNI() { return this.TFDNI; }
	public JTextField getNombre() { return this.TFNombre; }
	public JTextField getApellidos() { return this.TFApellidos; }
	public JTextField getDireccion() { return this.TFDireccion; }
	public JTextField getPoblacion() { return this.TFPoblacion; }
	public JTextField getTelefono() { return this.TFTelefono; }
	public JTextField getTitulacion() { return this.TFTitulacion; }
	public JTextField getCentro() { return this.TFCentro; }
	public JTextField getAñoObtencion() { return this.TFAñoObtencion; }
	public JTextField getCuentaBancaria() { return this.TFCuentaBancaria; }
	public JButton getBAceptar() { return this.BAceptar; }
	
	
	//Funciones
	public void HabilitarBoton() {
		if(!TFDNI.getText().isEmpty() && !TFNombre.getText().isEmpty()
				 && !TFApellidos.getText().isEmpty() && !TFDireccion.getText().isEmpty()
				 && !TFPoblacion.getText().isEmpty() && !TFTelefono.getText().isEmpty()
				 && !TFTitulacion.getText().isEmpty() && !TFCentro.getText().isEmpty()
				 && (TFCuentaBancaria.getText().length()==20) && (TFTelefono.getText().length()==9)
				 && (TFDNI.getText().length()==9) && ((TFAñoObtencion.getText().length()==4))
				 && año()
				 && !TFAñoObtencion.getText().isEmpty() && !TFCuentaBancaria.getText().isEmpty()) {
			BAceptar.setEnabled(true);
		}
		else {
			BAceptar.setEnabled(false);
		}
	}
	public boolean año() {
		if(TFAñoObtencion.getText().isEmpty()) {
			return false;
		}
		if(CBColegiado.getSelectedItem()=="Colegiado") {
			if((Integer.parseInt(TFAñoObtencion.getText())>=1965) && (Integer.parseInt(TFAñoObtencion.getText())<=2020)) {
				return true;
			}
			return false;
		}else {
			if((Integer.parseInt(TFAñoObtencion.getText())>=2012) && (Integer.parseInt(TFAñoObtencion.getText())<=2020)) {
				return true;
			}
	
			return false;
		}
	}
}
