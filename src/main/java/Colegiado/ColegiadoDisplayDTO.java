package Colegiado;

public class ColegiadoDisplayDTO {
		private String numero_colegiado;
		private String dni;
		private String nombre;
		private String apellido;
		private String direccion;
		private String poblacion;
		private int telefono;
		private String titulacion;
		private String centro;
		private int añoobtencion;
		private String cuentabancaria;
		private String fecha;
		private String estado;
		
		public ColegiadoDisplayDTO() {}
		public ColegiadoDisplayDTO(String numero_colegiado, String dni, String nombre, String apellido,
				String direccion, String poblacion, int telefono, String titulacion, String centro, int añoobtencion,
				String cuentabancaria, String fecha, String estado) {
			super();
			this.numero_colegiado = numero_colegiado;
			this.dni = dni;
			this.nombre = nombre;
			this.apellido = apellido;
			this.direccion = direccion;
			this.poblacion = poblacion;
			this.telefono = telefono;
			this.titulacion = titulacion;
			this.centro = centro;
			this.añoobtencion = añoobtencion;
			this.cuentabancaria = cuentabancaria;
			this.fecha = fecha;
			this.estado = estado;
		}
		
		public ColegiadoDisplayDTO(String numColeg, String dni, String nombre, String fecha) {
			this.numero_colegiado=numColeg;
			this.dni=dni;
			this.nombre=nombre;
			this.fecha=fecha;
		}

		public String getNumero_colegiado() {
			return numero_colegiado;
		}

		public void setNumero_colegiado(String numero_colegiado) {
			this.numero_colegiado = numero_colegiado;
		}

		public String getDni() {
			return dni;
		}

		public void setDni(String dni) {
			this.dni = dni;
		}

		public String getNombre() {
			return nombre;
		}

		public void setNombre(String nombre) {
			this.nombre = nombre;
		}

		public String getApellido() {
			return apellido;
		}

		public void setApellido(String apellido) {
			this.apellido = apellido;
		}

		public String getDireccion() {
			return direccion;
		}

		public void setDireccion(String direccion) {
			this.direccion = direccion;
		}

		public String getPoblacion() {
			return poblacion;
		}

		public void setPoblacion(String poblacion) {
			this.poblacion = poblacion;
		}

		public int getTelefono() {
			return telefono;
		}

		public void setTelefono(int telefono) {
			this.telefono = telefono;
		}

		public String getTitulacion() {
			return titulacion;
		}

		public void setTitulacion(String titulacion) {
			this.titulacion = titulacion;
		}

		public String getCentro() {
			return centro;
		}

		public void setCentro(String centro) {
			this.centro = centro;
		}

		public int getAñoobtencion() {
			return añoobtencion;
		}

		public void setAñoobtencion(int añoobtencion) {
			this.añoobtencion = añoobtencion;
		}

		public String getCuentabancaria() {
			return cuentabancaria;
		}

		public void setCuentabancaria(String cuentabancaria) {
			this.cuentabancaria = cuentabancaria;
		}

		public String getFecha() {
			return fecha;
		}

		public void setFecha(String fecha) {
			this.fecha = fecha;
		}

		public String getEstado() {
			return estado;
		}

		public void setEstado(String estado) {
			this.estado = estado;
		}
		
}