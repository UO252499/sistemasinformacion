package Colegiado;

import java.io.BufferedWriter;
import java.io.File;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.text.Format;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.Iterator;
import java.util.List;
import javax.swing.JFileChooser;
import javax.swing.filechooser.FileNameExtensionFilter;
import com.opencsv.CSVReader;
import util.SwingUtil;

public class GestionCuotasController {
	private GestionCuotasModel model;
	private GestionCuotasView view;
	String rutaSalida = "src/main/resources/respuestasCuotas.txt";
	String rutaError = "src/main/resources/erroresCuotas.txt";
	ArrayList<String> respuestas;
	ArrayList<String> errores;
	final String rutaFichero="src/main/resources/cuotas.txt";
	final String delim = ",";
	final String NEXT_LINE = "\n";


	public GestionCuotasController(GestionCuotasModel m, GestionCuotasView v) {
		this.model = m;
		this.view = v;
		respuestas = new ArrayList<String>();
		errores = new ArrayList<String>();
		//no hay inicializacion especifica del modelo, solo de la vista
		this.initView();
	}

	public void initController() {
		view.getBtnEmitir().addActionListener(e -> SwingUtil.exceptionWrapper(() -> EmitirCuota()));

	}

	public void EmitirCuota() {
		String nombre;
		String dni;
		String cuentaBanco;
		String tipo;
		int cuota;

		Date fecha= new Date();
		Format formatter = new SimpleDateFormat("yyyy-MM");
		String mesActual=formatter.format(fecha);

		int id_recibo;
		int i=0;
		String ultimoMes=model.obtenerFecha();
		List<ColegiadoDisplayDTO> colegiados=model.getListaColegiados();
		if(!colegiados.isEmpty()) {
			if(!mesActual.equals(ultimoMes)) {
				try {
					FileWriter fw = new FileWriter(rutaFichero);
					for (ColegiadoDisplayDTO colegiado : colegiados) {
						id_recibo=model.obtenerMaxID()+1;
						nombre=colegiado.getNombre();
						dni=colegiado.getDni();
						cuentaBanco=colegiado.getCuentabancaria();
						fw.append(Integer.toString(id_recibo)).append(delim).append(mesActual).append(delim);
						fw.append(nombre).append(delim).append(dni).append(delim);
						tipo=colegiado.getEstado();
						if(tipo.equals("Colegiado")) {
							cuota=40;
							fw.append(Integer.toString(cuota)).append(delim);
						}
						else {
							cuota=20;
							fw.append(Integer.toString(cuota)).append(delim);
						}
						fw.append(delim).append(cuentaBanco);
						model.nuevoRecibo(id_recibo, dni, mesActual, Integer.toString(cuota));
						if(i<colegiados.size()-1) { 
							fw.append(NEXT_LINE);
						}
						i++;
					}
					fw.flush();
					fw.close();
					view.ficheroGenerado(mesActual, rutaFichero);
				}
				catch (IOException e) {
					// Error al crear el archivo, por ejemplo, el archivo 
					// está actualmente abierto.
					e.printStackTrace();
				}
			}
			else {
				view.ficheroNoGenerado(mesActual);
			}
		}
	}

	public void initView() {
		view.controladorCuotas = this;
		//Abre la ventana (sustituye al main generado por WindowBuilder)
		view.getFrm().setVisible(true); 
	}
	public void ObtenerFichero() {
		JFileChooser selectorArchivos = new JFileChooser();
		selectorArchivos.setFileSelectionMode(JFileChooser.FILES_ONLY);
		FileNameExtensionFilter filtro = new FileNameExtensionFilter("Archivo .csv", "csv");
		selectorArchivos.setFileFilter(filtro);
		int resultado = selectorArchivos.showOpenDialog(view);
		File archivo = selectorArchivos.getSelectedFile();
		String name = archivo.getName();
		int lastIndexOf = name.lastIndexOf(".csv");
		if (resultado ==JFileChooser.APPROVE_OPTION && lastIndexOf != -1)
			archivoSeleccionado(archivo);	
		else
			view.SeleccionErronea();
	}
	
	private void archivoSeleccionado(File archivo) {
		CSVReader br = null;
		try {
			br =new CSVReader(new FileReader(archivo));
			String[] line = br.readNext();
			while (null!=line) {
				ProcesarLinea(line);
				line = br.readNext();
			}

		} catch (Exception e) {
		} finally {
			if (null!=br)
				try {
					br.close();
				} catch (IOException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
		}
		generarArchivoRespuestas();
	}		

	private void ProcesarLinea(String[] fields) {
		String id_recibo;
		if (!(fields[0].length()==2))
			id_recibo = fields[0];
		else
			id_recibo=fields[0].substring(1);
		String DNI = fields[1];
		String Nombre = fields[2];
		String Apellidos = fields[3];
		int precio = Integer.parseInt(fields[4]);
		String comentario = fields[5];
		if (model.buscarDNI(DNI)!=null) {
			if (comentario.equals(" "))
				procesarPagoCorrecto(id_recibo, DNI, Nombre, Apellidos, precio);
			else
				errorEnRecepion(id_recibo, DNI, Nombre, Apellidos, precio, comentario);
		}
		else
			errores.add("El DNI "+DNI+" NO figura en la base de datos");
	}
	private void procesarPagoCorrecto(String id_recibo, String dNI, String nombre, String apellidos, int precio) {
		model.marcarPagado(id_recibo);
		respuestas.add("Al miembro " + nombre + " " + apellidos + " con DNI: " + dNI + " se le ha cobrado correctamente su cuota de " + precio + "€");
	}

	private void errorEnRecepion(String id_recibo, String dNI, String nombre, String apellidos, int precio, String comentario) {
		if (precio == 0)
			errores.add("Al miembro " + nombre + " " + apellidos + " con DNI: " + dNI + " no se le ha podido cobrar por el siguiente motivo: " + comentario);	
		else
			errores.add("Al miembro " + nombre + " " + apellidos + " con DNI: " + dNI + "se le ha cobrado " + precio + "€ por el siguiente motivo: " + comentario);
		model.marcarError(id_recibo, comentario);
	}

	private void generarArchivoRespuestas() {
		File archivo = new File(rutaSalida);
		BufferedWriter bw;
		try {
			bw = new BufferedWriter(new FileWriter(archivo));
			Iterator<String> it = respuestas.iterator();
			while (it.hasNext()) {
				bw.write(it.next()+"\n\n");
			}
			bw.close();		
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		if (errores.isEmpty())
			view.solicitudesSinErrores(rutaSalida);
		else {
			rellenarErrores();
			view.solicitudesConErrores(rutaSalida,rutaError);
		}
	}

	private void rellenarErrores() {
		File archivo = new File(rutaError);
		BufferedWriter bw;
		try {
			bw = new BufferedWriter(new FileWriter(archivo));
			Iterator<String> it = errores.iterator();
			while (it.hasNext()) {
				bw.write(it.next()+"\n\n");
			}
			bw.close();		
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}		
	}
}
