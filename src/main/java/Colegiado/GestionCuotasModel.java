package Colegiado;

import java.util.*;
import util.Database;
import inscripcion.Externo;
import inscripcion.Persona;

public class GestionCuotasModel {
	private Database db=new Database();
	
	public Persona buscarDNI(String dNI) {
		String sql="select * from colegiado WHERE dni=? and(estado='Colegiado' or estado='PreColegiado')";
		String sql2="select * from externo WHERE dni=?";
		List<ColegiadoAltaEntity> colegiados = db.executeQueryPojo(ColegiadoAltaEntity.class, sql,dNI);
		if (colegiados.isEmpty()) {
			List<Externo> externos = db.executeQueryPojo(Externo.class, sql2,dNI);
			if (externos.isEmpty())
				return null;
			return externos.get(0);
		}
		return colegiados.get(0);
	}

	public void marcarPagado(String id_recibo) {
		db.executeUpdate("update recibo SET estado='pagado' where id_recibo=?", Integer.parseInt(id_recibo));		
	}
	
	public List<ColegiadoDisplayDTO> getListaColegiados() {
		String sql="SELECT numero_colegiado,apellidos||' '||nombre as nombre, dni,cuentabancaria, estado"
				+" from colegiado where estado='Colegiado' or estado='PreColegiado' ORDER BY fecha ASC";
		return db.executeQueryPojo(ColegiadoDisplayDTO.class, sql);
	}
	
	public int obtenerMaxID() {
		String sql="SELECT MAX(id_recibo) FROM recibo";
		List<Object[]>rows=db.executeQueryArray(sql);
		if (rows.get(0)[0]==null) {
			return 0;
		}else {
			return (int)rows.get(0)[0];
		}
	}
	
	public String obtenerFecha() {
		String sql="SELECT MAX(fecha) FROM recibo";
		List<Object[]>rows=db.executeQueryArray(sql);
		if (rows.get(0)[0]==null) {
			return "Vacio";
		}else {
			return (String)rows.get(0)[0];
		}
	}
	
	public void nuevoRecibo(int id_recibo, String dni, String fecha, String cuota) {
		String sql="insert into recibo(id_recibo,dni,fecha,cuota,estado) values"+
				" (?,?,?,?,'Emitido')";
		db.executeUpdate(sql, id_recibo, dni, fecha, cuota);
	}

	public void marcarError(String id_recibo, String comentario) {
		db.executeUpdate("update recibo SET estado='no_pagado', observaciones=? where id_recibo=?", comentario, id_recibo);		
	}
	
}
