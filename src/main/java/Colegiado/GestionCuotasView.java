package Colegiado;

import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JButton;
import java.awt.event.ActionListener;
import java.awt.event.ActionEvent;
import java.awt.Font;
import javax.swing.JOptionPane;
import javax.swing.JSeparator;

public class GestionCuotasView extends JFrame{

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	private JFrame frmGestionSolicitudes;
	GestionSolicitudController controlador;
	private JButton btnEmitir;
	protected GestionCuotasController controladorCuotas;

	/**
	 * Create the application.
	 */
	public GestionCuotasView() {
		initialize();
	}

	/**
	 * Initialize the contents of the frame.
	 */
	private void initialize() {
		frmGestionSolicitudes = new JFrame();
		frmGestionSolicitudes.setTitle("Gestion recibos de cuotas");
		frmGestionSolicitudes.setBounds(100, 100, 368, 224);
		frmGestionSolicitudes.setDefaultCloseOperation(JFrame.DISPOSE_ON_CLOSE);
		frmGestionSolicitudes.getContentPane().setLayout(null);
		frmGestionSolicitudes.setLocationRelativeTo(null);
		
		JLabel lblNewLabel_1 = new JLabel("Emitir recibos:");
		lblNewLabel_1.setFont(new Font("Tahoma", Font.BOLD, 14));
		lblNewLabel_1.setBounds(123, 11, 110, 14);
		frmGestionSolicitudes.getContentPane().add(lblNewLabel_1);
		
		JSeparator separator = new JSeparator();
		separator.setBounds(10, 88, 332, 14);
		frmGestionSolicitudes.getContentPane().add(separator);
		
		btnEmitir = new JButton("Emitir");
		btnEmitir.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
			}
		});
		btnEmitir.setBounds(123, 48, 89, 23);
		frmGestionSolicitudes.getContentPane().add(btnEmitir);
		
		JLabel lblNewLabel_1_1 = new JLabel("Recepcionar recibos:");
		lblNewLabel_1_1.setFont(new Font("Tahoma", Font.BOLD, 14));
		lblNewLabel_1_1.setBounds(106, 113, 153, 14);
		frmGestionSolicitudes.getContentPane().add(lblNewLabel_1_1);
		
		JButton btnNewButton_1 = new JButton("Recepcionar");
		btnNewButton_1.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				controladorCuotas.ObtenerFichero();
			}
		});
		btnNewButton_1.setBounds(123, 138, 110, 23);
		frmGestionSolicitudes.getContentPane().add(btnNewButton_1);
	}
	
	public void ficheroNoGenerado(String fecha) {
		JOptionPane.showMessageDialog(this.getFrm(), "Los recibos para el mes actual ("+fecha+") ya se han emitido", "No se han emitido los recibos", 1);
	}
	
	public void ficheroGenerado(String fecha, String rutaFichero) {
		JOptionPane.showMessageDialog(this.getFrm(), "Se ha generado un fichero con los recibos del mes actual ("+fecha+") en: "+rutaFichero, "Se han emitido los recibos", 1);
	}
	
	public void SeleccionErronea() {
		JOptionPane.showMessageDialog(this, "Seleccione un archivo .csv");
	}

	public void solicitudesSinErrores(String rutaSalida) {
		JOptionPane.showMessageDialog(this, "El informe con los resultados se ha creado en "+rutaSalida, "Se ha procesado correctamente el archivo", 1);
	}

	public void solicitudesConErrores(String rutaSalida, String rutaError) {
		JOptionPane.showMessageDialog(this, "El informe con los resultados se ha creado en "+rutaSalida + "\nEl informe de errores se ha creado en "+ rutaError, "No se han podido procesar correctamente algunas entradas", 0);		
	}

	public JFrame getFrm() {return frmGestionSolicitudes;}
	public void setFrmGestionSolicitudes(JFrame frmGestionSolicitudes) {this.frmGestionSolicitudes = frmGestionSolicitudes;}
	public JButton getBtnEmitir() {return btnEmitir;}
	public void setBtnEmitir(JButton btnEmitir) {this.btnEmitir = btnEmitir;}

}
