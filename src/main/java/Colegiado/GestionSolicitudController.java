package Colegiado;


import java.io.BufferedWriter;
import java.io.File;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

import javax.swing.table.TableModel;

import com.opencsv.CSVReader;

import util.ApplicationException;
import util.SwingUtil;

/**
 * Controlador para la funcionalidad de visualizacion de carreras para la inscripcion.
 * Es el punto de entrada de esta pantalla que se invocará:
 * -instanciando el controlador con la vista y el modelo
 * -ejecutando initController que instalara los manejadores de eventos
 */


public class GestionSolicitudController {
	private GestionSolicitudModel model;
	private GestionSolicitudView view;
	final String rutaArchivo="src/main/resources/solicitudes.txt";
	String rutaSalida = "src/main/resources/respuestas.txt";
	String rutaError = "src/main/resources/errores.txt";
	ArrayList<String> respuestas;
	ArrayList<String> errores;
	final String NEXT_LINE = "\n";
	final String delim = ",";

	public GestionSolicitudController(GestionSolicitudModel m, GestionSolicitudView v) {
		this.model = m;
		this.view = v;
		view.controlador=this;
		respuestas = new ArrayList<String>();
		errores = new ArrayList<String>();
		//no hay inicializacion especifica del modelo, solo de la vista
		this.initView();
	}
	
	public void initController() {
		view.getEnviar().addActionListener(e -> SwingUtil.exceptionWrapper(() -> EnviarColeg()));
		view.getCancelar().addActionListener(e -> SwingUtil.exceptionWrapper(() -> Cancelar()));
	}
	
	public void initView() {
		this.getListaPendientes();
		//Abre la ventana (sustituye al main generado por WindowBuilder)
		view.getFrm().setVisible(true); 
	}
	
	public void getListaPendientes() {
		List<ColegiadoDisplayDTO> pendientes=model.getListaPendientes();
		TableModel tmodel=SwingUtil.getTableModelFromPojos(pendientes, 
				new String[] {"numero_colegiado","nombre","dni","fecha"});
		view.getTable().setModel(tmodel);
		view.getTable().removeColumn(view.getTable().getColumnModel().getColumn(0));
		view.getTable().getColumnModel().getColumn(0).setPreferredWidth(125);
		view.getTable().getColumnModel().getColumn(1).setPreferredWidth(40);
		view.getTable().getColumnModel().getColumn(2).setPreferredWidth(40);
	}
	
	public void Cancelar() {
		view.getFrm().setVisible(false);
	}
	
	public void EnviarColeg() {
		int filasSeleccionadas=view.getTable().getSelectedRowCount();
		int i=0;
		String numcoleg;
		ColegiadoAltaEntity coleg;

		if(filasSeleccionadas==0) {
			view.NoSeleccion();
		}
		else {
			int[] indicesFilas=view.getTable().getSelectedRows();
			try {
				FileWriter fw = new FileWriter(rutaArchivo);
				for (int indice : indicesFilas) { 
					numcoleg=(String) view.getTable().getModel().getValueAt(indice, 0);
					coleg=model.getColegiado(numcoleg);
					fw.append(coleg.getDNI()).append(delim).append(coleg.getNombre());
					model.modificarColegiado(numcoleg);
					if(i<indicesFilas.length-1) { 
						fw.append(NEXT_LINE);
					}
					i++;
				}
				fw.flush();
				fw.close();
				this.getListaPendientes();
				view.ficheroGenerado(rutaArchivo);
			}
			catch (IOException e) {
				// Error al crear el archivo, por ejemplo, el archivo 
				// está actualmente abierto.
				e.printStackTrace();
			}
		} 
	}

	public void ArchivoSeleccionado(File archivo) {
		CSVReader br = null;
	      try {
	         br =new CSVReader(new FileReader(archivo));
	         String[] line = br.readNext();
	         while (null!=line) {
	            ProcesarLinea(line);
	            line = br.readNext();
	         }
	         
	      } catch (Exception e) {
	      } finally {
	         if (null!=br)
				try {
					br.close();
				} catch (IOException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
	      }
	      generarArchivoRespuestas();
	}

	private void ProcesarLinea(String[] fields) {
		String DNI;
		if (fields[0].length()==9)
			DNI = fields[0];
		else
			DNI=fields[0].substring(1);
		String titulaciones = fields[1];
		String centro = fields[2];
		int año = Integer.parseInt(fields[3]);
		ColegiadoAltaEntity colegiado = model.comprobarColegiado(DNI,centro,año);
		if (colegiado !=  null) {
			if (!colegiado.getEstado().equals("Emitido")) {
				errores.add("El DNI "+DNI+" NO figura como enviado en la base de datos");
				return;
			}
			if (titulaciones.lastIndexOf("Master en Ingenieria Informatica")!=-1)
				contestarSolicitud(true, colegiado);
			else {
				//model.eliminarColegiado(DNI);
				contestarSolicitud(false, colegiado);
			}
		}
		else {
			errores.add("El DNI "+DNI+" NO figura en la base de datos");
			throw new ApplicationException("El DNI no figura en la base de datos");
		}		
	}

	private void generarArchivoRespuestas() {
		File archivo = new File(rutaSalida);
        BufferedWriter bw;
        try {
        	bw = new BufferedWriter(new FileWriter(archivo));
        	Iterator<String> it = respuestas.iterator();
        	while (it.hasNext()) {
        		bw.write(it.next()+"\n\n");
			}
			bw.close();		
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
        if (errores.isEmpty())
        	view.solicitudesSinErrores(rutaSalida);
        else {
        	rellenarErrores();
        	view.solicitudesConErrores(rutaSalida,rutaError);
        }
	}

	private void rellenarErrores() {
		File archivo = new File(rutaError);
        BufferedWriter bw;
        try {
        	bw = new BufferedWriter(new FileWriter(archivo));
        	Iterator<String> it = errores.iterator();
        	while (it.hasNext()) {
        		bw.write(it.next()+"\n\n");
			}
			bw.close();		
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}		
	}

	private void contestarSolicitud(boolean b, ColegiadoAltaEntity colegiado) {
		if (b) {
			respuestas.add(colegiado.getNombre() + " " + colegiado.getApellidos()+ " con DNI: "+ colegiado.getDNI() +" ha sido dado de alta en el COIIPA! Su número de colegiado es: "+ colegiado.getNumero_Colegiado());
			model.actualizarColegiado(colegiado.getNumero_Colegiado());
		}
		else {
			respuestas.add(colegiado.getNombre() + " " + colegiado.getApellidos()+ " con DNI: "+ colegiado.getDNI() +" no ha sido dado de alta en el COIIPA por no poseer ninguna de las titulaciones que dan acceso a la colegiatura");
		}
	}
}
