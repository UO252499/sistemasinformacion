package Colegiado;

import java.util.*;
import util.Database;

public class GestionSolicitudModel {
	private Database db=new Database();
	
	public ColegiadoAltaEntity comprobarColegiado(String dNI, String centro, int año) {
		String sql="SELECT * FROM colegiado";
		List<ColegiadoAltaEntity> contesta=db.executeQueryPojo(ColegiadoAltaEntity.class, sql);
		ColegiadoAltaEntity cole = null;
		for (ColegiadoAltaEntity colegiado : contesta) {
			if (dNI.equals(colegiado.getDNI()))
				cole=colegiado;
		}
		if (cole!=null) {
			String sql2="UPDATE colegiado set estado='Colegiado' where dni=? and estado='Enviado'";
			db.executeUpdate(sql2, cole.getDNI());
			return cole;
		}
		return null;
		
	}
	public void eliminarColegiado(String dNI) {
		String sql2="DELETE from colegiado where Dni=?";
		db.executeUpdate(sql2, dNI);		
	}
	
	public List<ColegiadoDisplayDTO> getListaPendientes() {
		String sql="SELECT numero_colegiado,apellidos||', '||nombre as nombre, dni,fecha"
				+" from colegiado where estado='Pendiente' ORDER BY fecha ASC";
		return db.executeQueryPojo(ColegiadoDisplayDTO.class, sql);
	}
	
	public ColegiadoAltaEntity getColegiado(String num_coleg) {
		String sql="SELECT nombre ||' '|| apellidos as nombre,dni "
				+ "from colegiado where numero_colegiado=?";
		List<ColegiadoAltaEntity> colegiado=db.executeQueryPojo(ColegiadoAltaEntity.class, sql, num_coleg);
		//validateCondition(!curso.isEmpty(),"Id de carrera no encontrado: "+id);
		return colegiado.get(0);
	}
	
	public void modificarColegiado(String numcoleg) {
		String sql="update colegiado set estado='Enviado' where numero_colegiado=?";
		db.executeUpdate(sql,numcoleg);
	}
	
	public void actualizarColegiado(String numcoleg) {
		String sql="update colegiado set estado='Colegiado' where numero_colegiado=?";
		db.executeUpdate(sql,numcoleg);
	}
}
