package Colegiado;

import java.awt.EventQueue;

import javax.swing.JFrame;
import javax.swing.filechooser.FileNameExtensionFilter;
import javax.swing.JMenuBar;
import javax.swing.JLabel;
import javax.swing.JScrollPane;
import javax.swing.JTable;
import javax.swing.ListSelectionModel;
import javax.swing.JButton;
import java.awt.event.ActionListener;
import java.awt.event.ActionEvent;
import java.awt.Font;
import javax.swing.JFileChooser;
import javax.swing.JMenuItem;
import javax.swing.JOptionPane;
import javax.swing.JMenu;
import java.io.File;
import java.awt.SystemColor;

public class GestionSolicitudView extends JFrame{

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	private JFrame frmGestionSolicitudes;
	private JTable table;
	private JButton enviar;
	private JButton cancelar;
	private JMenuBar menuBar;
	private JMenu mnNewMenu;
	private JMenuItem mntmNewMenuItem;
	GestionSolicitudController controlador;

	

	/**
	 * Launch the application.
	 */
	public static void main(String[] args) {
		EventQueue.invokeLater(new Runnable() {
			public void run() {
				try {
					GestionSolicitudView window = new GestionSolicitudView();
					window.frmGestionSolicitudes.setVisible(true);
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		});
	}

	/**
	 * Create the application.
	 */
	public GestionSolicitudView() {
		initialize();
	}

	/**
	 * Initialize the contents of the frame.
	 */
	private void initialize() {
		frmGestionSolicitudes = new JFrame();
		frmGestionSolicitudes.setTitle("Gestion Solicitudes");
		frmGestionSolicitudes.setBounds(100, 100, 368, 326);
		frmGestionSolicitudes.setDefaultCloseOperation(JFrame.DISPOSE_ON_CLOSE);
		frmGestionSolicitudes.getContentPane().setLayout(null);
		frmGestionSolicitudes.setLocationRelativeTo(null);
		
		table = new JTable();
		table.setName("Pendientes");
		table.setSelectionMode(ListSelectionModel.MULTIPLE_INTERVAL_SELECTION);
		table.setDefaultEditor(Object.class, null); //readonly

		JScrollPane scrollPane = new JScrollPane(table);
		scrollPane.setBounds(10, 54, 332, 161);
		frmGestionSolicitudes.getContentPane().add(scrollPane);
		
		JLabel lblNewLabel = new JLabel("Colegiados pendientes:");
		lblNewLabel.setBounds(10, 36, 147, 14);
		frmGestionSolicitudes.getContentPane().add(lblNewLabel);
		
		enviar = new JButton("Enviar");
		enviar.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
			}
		});
		enviar.setBounds(154, 233, 89, 23);
		frmGestionSolicitudes.getContentPane().add(enviar);
		
		cancelar = new JButton("Cancelar");
		cancelar.setBounds(253, 233, 89, 23);
		frmGestionSolicitudes.getContentPane().add(cancelar);
		
		JLabel lblNewLabel_1 = new JLabel("Emision de solicitud:");
		lblNewLabel_1.setFont(new Font("Tahoma", Font.BOLD, 14));
		lblNewLabel_1.setBounds(104, 11, 157, 14);
		frmGestionSolicitudes.getContentPane().add(lblNewLabel_1);
		
		menuBar = new JMenuBar();
		menuBar.setBackground(SystemColor.menu);
		menuBar.setToolTipText("T");
		frmGestionSolicitudes.setJMenuBar(menuBar);
		
		mnNewMenu = new JMenu("Titulaciones recibidas");
		menuBar.add(mnNewMenu);
		
		mntmNewMenuItem = new JMenuItem("Abrir");
		mntmNewMenuItem.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				ObtenerFichero();	
			}
		});
		mnNewMenu.add(mntmNewMenuItem);
	}

	public JFrame getFrm() {return frmGestionSolicitudes;}
	public void setFrmGestionSolicitudes(JFrame frmGestionSolicitudes) {this.frmGestionSolicitudes = frmGestionSolicitudes;}
	public JTable getTable() {return table;}
	public void setTable(JTable table) {this.table = table;}
	public JButton getEnviar() {return enviar;}
	public void setEnviar(JButton enviar) {this.enviar = enviar;}
	public JButton getCancelar() {return cancelar;}
	public void setCancelar(JButton cancelar) {this.cancelar = cancelar;}
	
	private void ObtenerFichero() {
		JFileChooser selectorArchivos = new JFileChooser();
		selectorArchivos.setFileSelectionMode(JFileChooser.FILES_ONLY);
		FileNameExtensionFilter filtro = new FileNameExtensionFilter("Archivo .csv", "csv");
		selectorArchivos.setFileFilter(filtro);
		int resultado = selectorArchivos.showOpenDialog(this);
		File archivo = selectorArchivos.getSelectedFile();
		String name = archivo.getName();
	    int lastIndexOf = name.lastIndexOf(".csv");
	    if (resultado ==JFileChooser.APPROVE_OPTION && lastIndexOf != -1)
	    	controlador.ArchivoSeleccionado(archivo);	
	    else
			JOptionPane.showMessageDialog(this, "Seleccione un archivo .csv");
	    }

	public void solicitudesConErrores(String rutaSalida, String rutaError) {
		JOptionPane.showMessageDialog(this, "El informe con los resultados se ha creado en "+rutaSalida + "\nEl informe de errores se ha creado en "+ rutaError, "No se han podido procesar correctamente algunas entradas", 0);
	}
	
	public void NoSeleccion() {
		JOptionPane.showMessageDialog(this.getFrm(), "No se ha seleccionado ningun colegiado","No se puede enviar",JOptionPane.WARNING_MESSAGE);	
	}

	public void solicitudesSinErrores(String rutaSalida) {
		JOptionPane.showMessageDialog(this, "El informe con los resultados se ha creado en "+rutaSalida, "Se ha procesado correctamente el archivo", 1);
	}
	
	public void ficheroGenerado(String rutaArchivo) {
		JOptionPane.showMessageDialog(this, "Se ha generado un fichero con los colegiados a enviar en: "+rutaArchivo, "El archivo se ha enviado correctamente", 1);
	}	
}
