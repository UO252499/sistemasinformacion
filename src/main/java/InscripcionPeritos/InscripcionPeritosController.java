package InscripcionPeritos;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.lang.reflect.InvocationTargetException;
import java.util.Date;

import javax.swing.JOptionPane;

public class InscripcionPeritosController {
	private InscripcionPeritosModel model;
	private InscripcionPeritosView view;
	
	public InscripcionPeritosController(InscripcionPeritosModel Model, InscripcionPeritosView View) {
		this.model=Model;
		this.view=View;
		
		initView();
	}
	public void initController() {
		Date fechaActual = new Date();
		view.getBuscarColegiado().addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent evt) {
				try {
					if(model.existeColegiado(view.getNColegiado().getText())) {
						if(model.ColegiadoAceptado(view.getNColegiado().getText())) {
							ValoresColegiado();
						}else {
							JOptionPane.showMessageDialog(null, "El colegiado con el numero "+view.getNColegiado().getText()
									+ " todavia no ha sido aceptado","Colegiado en estado pendiente",JOptionPane.WARNING_MESSAGE);
						}
					}else {
						JOptionPane.showMessageDialog(null, "El colegiado con el numero "+view.getNColegiado().getText()
								+ " no existe","Colegiado no existe",JOptionPane.WARNING_MESSAGE);
					}
				} catch (IllegalAccessException | InvocationTargetException | NoSuchMethodException e) {
					e.printStackTrace();
				}
			}
		});
		view.getInscribirme().addActionListener(new ActionListener() {
			@Override
			public void actionPerformed(ActionEvent e) {
				if(model.existeEnPeritos(view.getNColegiado().getText())) {
					JOptionPane.showMessageDialog(null, "El colegiado con el numero "+view.getNColegiado().getText()
					+ " se encuentra en estado pendiente en la lista de peritos","Colegiado ya inscrito",JOptionPane.WARNING_MESSAGE);
				}else {
					model.actualizarColegiado(view.getDireccion().getText(), view.getPoblacion().getText(),
							Integer.parseInt(view.getTelefono().getText()), view.getCuentaBancaria().getText(),
							view.getNColegiado().getText());
					model.inscripcionPeritos(view.getNColegiado().getText(),fechaActual,
							view.getExperiencia().getText());
					JOptionPane.showMessageDialog(null, "El colegiado con el numero: "+view.getNColegiado().getText()
					+ " ha sido inscrito en la lista de periciales");
				}
				Habilitar(false, null);
				view.getBuscarColegiado().setEnabled(false);
			}
		});
		view.getSalir().addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				view.getFrame().setVisible(false);
			}
		});
		
		view.getReiniciar().addActionListener(new ActionListener() {
			@Override
			public void actionPerformed(ActionEvent e) {
				Habilitar(false, null);
			}
		});
	}

	
	public void initView() {
		view.getFrame().setVisible(true); 
	}
	
	public void ValoresColegiado() throws IllegalAccessException, InvocationTargetException, NoSuchMethodException {
		InscripcionPeritosEntity colegiado = model.getColegiado(view.getNColegiado().getText());
		if(colegiado==null) {
			JOptionPane.showMessageDialog(null, "Numero de colegiado mal introducido","Colegiado no existe",JOptionPane.WARNING_MESSAGE);
			Habilitar(false, colegiado);
		}else {
			Habilitar(true, colegiado);
		}
	}
	
	
	public void Habilitar(boolean bol, InscripcionPeritosEntity colegiado) {
		if(bol==true) {
			//Añadir los valores del colegiado
			view.getDNI().setText(colegiado.getDNI());
			view.getNombre().setText(colegiado.getNombre());
			view.getApellidos().setText(colegiado.getApellidos());
			view.getDireccion().setText(colegiado.getDireccion());
			view.getPoblacion().setText(colegiado.getPoblacion());
			view.getTelefono().setText(String.valueOf(colegiado.getTelefono()));
			view.getTitulacion().setText(colegiado.getTitulacion());
			view.getCentro().setText(colegiado.getCentro());
			view.getAñoObtencion().setText(String.valueOf(colegiado.getAñoObtencion()));
			view.getCuentaBancaria().setText(colegiado.getCuentaBancaria());

			//Habilitar las campos modificables
			view.getDireccion().setEditable(true);
			view.getPoblacion().setEditable(true);
			view.getTelefono().setEditable(true);
			view.getCuentaBancaria().setEditable(true);
			view.getExperiencia().setEditable(true);
			
			view.getNColegiado().setEditable(false);
			view.getBuscarColegiado().setEnabled(false);
			view.getReiniciar().setEnabled(true);
		}else {
			//Vaciar los campos del colegiado
			view.getNColegiado().setText(null);
			view.getDNI().setText(null);
			view.getNombre().setText(null);
			view.getApellidos().setText(null);
			view.getDireccion().setText(null);
			view.getPoblacion().setText(null);
			view.getTelefono().setText(null);
			view.getTitulacion().setText(null);
			view.getCentro().setText(null);
			view.getAñoObtencion().setText(null);
			view.getCuentaBancaria().setText(null);
			view.getExperiencia().setText(null);
			
			//Deshabilitar el boton inscribirme
			view.getInscribirme().setEnabled(false);
			
			//Deshabilitar los campos modificables
			view.getDireccion().setEditable(false);
			view.getPoblacion().setEditable(false);
			view.getTelefono().setEditable(false);
			view.getCuentaBancaria().setEditable(false);
			view.getExperiencia().setEditable(false);
			
			view.getNColegiado().setEditable(true);
			view.getReiniciar().setEnabled(false);
		}
	}
}
