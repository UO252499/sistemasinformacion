package InscripcionPeritos;

public class InscripcionPeritosDisplayDTO {
	private String numero_colegiado;
	private String nombre;
	private String apellidos;
	private String experiencia;
	private String fecha;
	private int id;
	
	
	public InscripcionPeritosDisplayDTO() {}
	public InscripcionPeritosDisplayDTO(String nc,String nombre, String apellidos, String experiencia,String fecha,int ID) {
		this.numero_colegiado=nc;
		this.nombre=nombre;
		this.apellidos=apellidos;
		this.experiencia=experiencia;
		this.fecha=fecha;
		this.id=ID;
	}
	
	public String getNumero_colegiado() { return this.numero_colegiado; }
	public void setNumero_colegiado(String nc) { this.numero_colegiado = nc; }
	
	public String getNombre() { return this.nombre; }
	public void setNombre(String nombre) { this.nombre = nombre; }
	
	public String getApellidos() { return this.apellidos; }
	public void setApellidos(String apellidos) { this.apellidos = apellidos; }
	
	public String getExperiencia() { return this.experiencia; }
	public void setExperiencia(String experiencia) { this.experiencia = experiencia; }
	
	public String getFecha() { return this.fecha; }
	public void setFecha(String fecha) { this.fecha = fecha; }
	
	public int getId_perito() { return this.id; }
	public void setId_perito(int ID) { this.id=ID; }

}