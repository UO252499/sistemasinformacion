package InscripcionPeritos;

import java.util.Date;
import java.util.List;

import util.Database;
import util.Util;

public class InscripcionPeritosModel {
	private Database db= new Database();
	
	public InscripcionPeritosEntity getColegiado(String nc) {
		String sql="SELECT dni,nombre, apellidos, direccion, poblacion, "
				+ "telefono, titulacion,centro, añoobtencion, cuentabancaria "
				+ "FROM colegiado WHERE numero_colegiado=?";
		List<InscripcionPeritosEntity> colegiado=db.executeQueryPojo(InscripcionPeritosEntity.class, sql, nc);
		if(colegiado.isEmpty()) {
			return null;
		}else {
			return colegiado.get(0);
		}
	}
	public void actualizarColegiado(String direccion,String poblacion,int telefono,String cuentabancaria,String numerocolegiado) {
		String sql="UPDATE colegiado SET direccion=?,poblacion=?,"
				+ "telefono=?,cuentabancaria=? "
				+ "WHERE numero_colegiado=?";
		db.executeUpdate(sql,direccion,poblacion,telefono,cuentabancaria,numerocolegiado);
	}
	
	public void inscripcionPeritos(String numerocolegiado,Date fecha,String experiencia) {
		int id=obtenerId();
		int orden=obtenerOrden();
		String date=Util.dateToIsoString(fecha);
		String sql="INSERT INTO perito(id_perito,numero_colegiado,experiencia,fecha,estado,orden) "
				+ "VALUES (?,?,?,?,'Pendiente',?)";
		db.executeUpdate(sql,id,numerocolegiado,experiencia,date,orden);
	}
	
	public boolean existeEnPeritos(String numerocolegiado) {
		String sql= "SELECT MAX(numero_colegiado) FROM perito WHERE numero_colegiado=? AND estado = 'Pendiente'";
		List<Object[]>rows=db.executeQueryArray(sql,numerocolegiado);
		if (rows.get(0)[0]==null) {
			return false;
		}
		else {
			return true;
		}
	}
	public boolean ColegiadoAceptado(String nc) {
		String sql= "SELECT estado FROM colegiado WHERE numero_colegiado=?";
		String e="Pendiente";
		List<Object[]>rows=db.executeQueryArray(sql,nc);
		if (e.equals(rows.get(0)[0])) {
			return false;
		}else {
			return true;
		}
	}
	
	public boolean existeColegiado(String nc) {
		String sql= "SELECT MAX(numero_colegiado) FROM colegiado WHERE numero_colegiado=?";
		List<Object[]>rows=db.executeQueryArray(sql,nc);
		if (rows.get(0)[0]==null) {
			return false;
		}
		else {
			return true;
		}
	}
	
	public int obtenerId() {
		String sql="SELECT MAX(id_perito) FROM perito";
		List<Object[]>rows=db.executeQueryArray(sql);
		if(rows.get(0)[0]==null) {
			return 1;
		}else {
			return (Integer)rows.get(0)[0]+1;
		}
	}
	
	public int obtenerOrden() {
		String sql="SELECT MAX(orden) FROM perito";
		List<Object[]>rows=db.executeQueryArray(sql);
		if(rows.get(0)[0]==null) {
			return 1;
		}else {
			return (Integer)rows.get(0)[0]+1;
		}
	}
}
