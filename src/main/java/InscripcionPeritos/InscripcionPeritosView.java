package InscripcionPeritos;

import javax.swing.JFrame;

import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JTextField;

import java.awt.event.FocusEvent;
import java.awt.event.FocusListener;
import java.awt.event.KeyAdapter;
import java.awt.event.KeyEvent;

import javax.swing.JButton;

public class InscripcionPeritosView {
	private JFrame frame;
	private JTextField TFNColegiado;
	private JTextField TFDNI;
	private JLabel LNombre;
	private JTextField TFNombre;
	private JTextField TFApellidos;
	private JLabel LApellidos;
	private JLabel LDireccion;
	private JTextField TFDireccion;
	private JTextField TFPoblacion;
	private JTextField TFTelefono;
	private JTextField TFTitulacion;
	private JTextField TFCentro;
	private JTextField TFAñoObtencion;
	private JTextField TFCuentaBancaria;
	private JTextField TFExperiencia;
	private JButton BBuscarColegiado;
	private JButton BInscribirme;
	private JButton BSalir;
	private JButton BReiniciar;
	
	public InscripcionPeritosView() {
		initialize();
	}

	private void initialize() {
		frame = new JFrame();
		frame.setTitle("Inscripcion Peritos");
		frame.setName("Peritos");
		frame.setBounds(0, 0, 500, 500);
		frame.setDefaultCloseOperation(JFrame.DISPOSE_ON_CLOSE);
		frame.getContentPane().setLayout(null);
		frame.setLocationRelativeTo(null);
		//Numero de colegiado
		JLabel LNColegiado = new JLabel("Nº de Colegiado");
		LNColegiado.setBounds(15, 40, 127, 20);
		frame.getContentPane().add(LNColegiado);
		
		TFNColegiado = new JTextField();
		TFNColegiado.setBounds(136, 37, 106, 26);
		frame.getContentPane().add(TFNColegiado);
		TFNColegiado.setColumns(10);
		TFNColegiado.addKeyListener(new KeyAdapter() {
			public void keyTyped(KeyEvent evt) {
				char c=evt.getKeyChar();
		        if (Character.isLetter(c)){
		            evt.consume();
		            JOptionPane.showMessageDialog(null, "Ingresar solo numeros","Valor mal introducido",JOptionPane.WARNING_MESSAGE);
		        }
			}
			@Override
			public void keyReleased(KeyEvent e) {
				if(TFNColegiado.getText().isEmpty()) {
					BBuscarColegiado.setEnabled(false);
				}else {
					BBuscarColegiado.setEnabled(true);
				}
			}
		});
		
		
		//DNI
		JLabel LDNI = new JLabel("DNI");
		LDNI.setBounds(15, 94, 69, 20);
		frame.getContentPane().add(LDNI);
		
		TFDNI = new JTextField();
		TFDNI.setBounds(99, 91, 106, 26);
		frame.getContentPane().add(TFDNI);
		TFDNI.setColumns(10);
		TFDNI.setEditable(false);
		TFDNI.addKeyListener(new KeyAdapter() {
			@Override
			public void keyReleased(KeyEvent e) {
				HabilitarBotonInscribirme();
			}
		});
		
		
		//Nombre
		LNombre = new JLabel("Nombre");
		LNombre.setBounds(233, 94, 69, 20);
		frame.getContentPane().add(LNombre);
		
		TFNombre = new JTextField();
		TFNombre.setBounds(350, 91, 113, 26);
		frame.getContentPane().add(TFNombre);
		TFNombre.setColumns(10);
		TFNombre.setEditable(false);
		TFNombre.addKeyListener(new KeyAdapter() {
			@Override
			public void keyReleased(KeyEvent e) {
				HabilitarBotonInscribirme();
			}
		});
		
		
		//Apellidos
		LApellidos = new JLabel("Apellidos");
		LApellidos.setBounds(15, 130, 69, 20);
		frame.getContentPane().add(LApellidos);
		
		TFApellidos = new JTextField();
		TFApellidos.setBounds(99, 127, 106, 26);
		frame.getContentPane().add(TFApellidos);
		TFApellidos.setColumns(10);
		TFApellidos.setEditable(false);
		TFApellidos.addKeyListener(new KeyAdapter() {
			@Override
			public void keyReleased(KeyEvent e) {
				HabilitarBotonInscribirme();
			}
		});
		
		
		//Direccion
		LDireccion = new JLabel("Direccion");
		LDireccion.setBounds(233, 130, 69, 20);
		frame.getContentPane().add(LDireccion);
		
		TFDireccion = new JTextField();
		TFDireccion.setBounds(350, 127, 113, 26);
		frame.getContentPane().add(TFDireccion);
		TFDireccion.setColumns(10);
		TFDireccion.setEditable(false);
		TFDireccion.addKeyListener(new KeyAdapter() {
			@Override
			public void keyReleased(KeyEvent e) {
				HabilitarBotonInscribirme();
			}
		});
		
		
		//Poblacion
		JLabel LPoblacion = new JLabel("Poblacion");
		LPoblacion.setBounds(15, 166, 69, 20);
		frame.getContentPane().add(LPoblacion);
		
		TFPoblacion = new JTextField();
		TFPoblacion.setBounds(99, 163, 106, 26);
		frame.getContentPane().add(TFPoblacion);
		TFPoblacion.setColumns(10);
		TFPoblacion.setEditable(false);
		TFPoblacion.addKeyListener(new KeyAdapter() {
			public void keyTyped(KeyEvent evt) {
				char c=evt.getKeyChar();
		        if (Character.isDigit(c)){
		            evt.consume();
		            JOptionPane.showMessageDialog(null, "Ingresar solo letras","Valor mal introducido",JOptionPane.WARNING_MESSAGE);
		        }
		        if(TFTelefono.getText().length()>=9) {
					evt.consume();
				}
			}
			
			@Override
			public void keyReleased(KeyEvent e) {
				HabilitarBotonInscribirme();
			}
		});
		
		
		//Telefono
		JLabel LTelefono = new JLabel("Telefono");
		LTelefono.setBounds(233, 166, 69, 20);
		frame.getContentPane().add(LTelefono);
		
		TFTelefono = new JTextField();
		TFTelefono.setBounds(350, 163, 113, 26);
		frame.getContentPane().add(TFTelefono);
		TFTelefono.setColumns(10);
		TFTelefono.setEditable(false);
		TFTelefono.addKeyListener(new KeyAdapter() {
			public void keyTyped(KeyEvent evt) {
				char c=evt.getKeyChar();
		        if (Character.isLetter(c)){
		            evt.consume();
		            JOptionPane.showMessageDialog(null, "Ingresar solo numeros","Valor mal introducido",JOptionPane.WARNING_MESSAGE);
		        }
		        if(TFTelefono.getText().length()>=9) {
					evt.consume();
				}
		        HabilitarBotonInscribirme();
			}
			@Override
			public void keyReleased(KeyEvent e) {
				HabilitarBotonInscribirme();
			}
		});
		
		
		//Titulacion
		JLabel LTitulacion = new JLabel("Titulacion");
		LTitulacion.setBounds(15, 202, 69, 20);
		frame.getContentPane().add(LTitulacion);
		
		TFTitulacion = new JTextField();
		TFTitulacion.setBounds(99, 199, 106, 26);
		frame.getContentPane().add(TFTitulacion);
		TFTitulacion.setColumns(10);
		TFTitulacion.setEditable(false);
		TFTitulacion.addKeyListener(new KeyAdapter() {
			@Override
			public void keyReleased(KeyEvent e) {
				HabilitarBotonInscribirme();
			}
		});
		
		
		//Centro
		JLabel LCentro = new JLabel("Centro");
		LCentro.setBounds(233, 202, 69, 20);
		frame.getContentPane().add(LCentro);
		
		TFCentro = new JTextField();
		TFCentro.setBounds(350, 199, 113, 26);
		frame.getContentPane().add(TFCentro);
		TFCentro.setColumns(10);
		TFCentro.setEditable(false);
		TFCentro.addKeyListener(new KeyAdapter() {
			@Override
			public void keyReleased(KeyEvent e) {
				HabilitarBotonInscribirme();
			}
		});
		
		
		//Año del Titulo
		JLabel LAñoObtencion = new JLabel("Año Titulo");
		LAñoObtencion.setBounds(15, 238, 82, 20);
		frame.getContentPane().add(LAñoObtencion);
		
		TFAñoObtencion = new JTextField();
		TFAñoObtencion.setBounds(99, 235, 52, 26);
		frame.getContentPane().add(TFAñoObtencion);
		TFAñoObtencion.setColumns(10);
		TFAñoObtencion.setEditable(false);
		TFAñoObtencion.addKeyListener(new KeyAdapter() {
			@Override
			public void keyReleased(KeyEvent e) {
				HabilitarBotonInscribirme();
			}
		});
		
		
		//Cuenta Bancaria
		JLabel LCuentaBancaria = new JLabel("Cuenta Bancaria");
		LCuentaBancaria.setBounds(166, 238, 119, 20);
		frame.getContentPane().add(LCuentaBancaria);
		
		TFCuentaBancaria = new JTextField();
		TFCuentaBancaria.setBounds(282, 235, 181, 26);
		frame.getContentPane().add(TFCuentaBancaria);
		TFCuentaBancaria.setColumns(10);
		TFCuentaBancaria.setEditable(false);
		TFCuentaBancaria.addKeyListener(new KeyAdapter() {
			public void keyTyped(KeyEvent evt) {
		        if(TFCuentaBancaria.getText().length()>=20) {
					evt.consume();
				}
		        HabilitarBotonInscribirme();
			}
		});
		TFCuentaBancaria.addFocusListener(new FocusListener() {
			@Override
			public void focusLost(FocusEvent e) {
				if(!TFCuentaBancaria.getText().isEmpty()) {
					if(TFCuentaBancaria.getText().length()<20) {
						JOptionPane.showMessageDialog(null, "Cuenta Bancaria mal introducida","Valor mal introducido",JOptionPane.WARNING_MESSAGE);
					}else {
						HabilitarBotonInscribirme();
					}
				}
			}

			@Override
			public void focusGained(FocusEvent e) {
				
			}
		});
		
		
		//Experiencia
		JLabel LExperiencia = new JLabel("Experiencia");
		LExperiencia.setBounds(15, 274, 218, 20);
		frame.getContentPane().add(LExperiencia);
		
		TFExperiencia = new JTextField();
		TFExperiencia.setBounds(109, 274, 354, 26);
		frame.getContentPane().add(TFExperiencia);
		TFExperiencia.setColumns(10);
		TFExperiencia.setEditable(false);
		TFExperiencia.addKeyListener(new KeyAdapter() {
			@Override
			public void keyReleased(KeyEvent e) {
				HabilitarBotonInscribirme();
			}
		});

		
		//Botones
		BBuscarColegiado = new JButton("Buscar Colegiado");
		BBuscarColegiado.setBounds(44, 354, 161, 29);
		frame.getContentPane().add(BBuscarColegiado);
		BBuscarColegiado.setEnabled(false);
		
		BInscribirme = new JButton("Inscribirme");
		BInscribirme.setBounds(293, 354, 115, 29);
		frame.getContentPane().add(BInscribirme);
		BInscribirme.setEnabled(false);
		
		BSalir = new JButton("Salir");
		BSalir.setBounds(187, 399, 115, 29);
		frame.getContentPane().add(BSalir);
		
		BReiniciar = new JButton("Reiniciar");
		BReiniciar.setBounds(259, 36, 93, 29);
		frame.getContentPane().add(BReiniciar);
		BReiniciar.setEnabled(false);
	}
	
	public JFrame getFrame() { return this.frame; }
	public JTextField getNColegiado() { return this.TFNColegiado; }
	public JTextField getDNI() { return this.TFDNI; }
	public JTextField getNombre() { return this.TFNombre; }
	public JTextField getApellidos() { return this.TFApellidos; }
	public JTextField getDireccion() { return this.TFDireccion; }
	public JTextField getPoblacion() { return this.TFPoblacion; }
	public JTextField getTelefono() { return this.TFTelefono; }
	public JTextField getTitulacion() { return this.TFTitulacion; }
	public JTextField getCentro() { return this.TFCentro; }
	public JTextField getAñoObtencion() { return this.TFAñoObtencion; }
	public JTextField getCuentaBancaria() { return this.TFCuentaBancaria; }
	public JTextField getExperiencia () { return this.TFExperiencia; }
	public JButton getBuscarColegiado() { return this.BBuscarColegiado; }
	public JButton getInscribirme() { return this.BInscribirme; }
	public JButton getSalir() { return this.BSalir; }
	public JButton getReiniciar() { return this.BReiniciar; }
	
	public void HabilitarBotonInscribirme() {
		if(!TFDNI.getText().isEmpty() && !TFNombre.getText().isEmpty()
				 && !TFApellidos.getText().isEmpty() && !TFDireccion.getText().isEmpty()
				 && !TFPoblacion.getText().isEmpty() && !TFTelefono.getText().isEmpty()
				 && !TFTitulacion.getText().isEmpty() && !TFCentro.getText().isEmpty()
				 && !TFAñoObtencion.getText().isEmpty() && (TFCuentaBancaria.getText().length()==20) 
				 && (TFTelefono.getText().length()==9) &&
				 !TFCuentaBancaria.getText().isEmpty() && !TFExperiencia.getText().isEmpty()) {
			BInscribirme.setEnabled(true);
		}
		else {
			BInscribirme.setEnabled(false);
		}
	}
}
