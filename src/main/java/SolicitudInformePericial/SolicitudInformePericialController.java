package SolicitudInformePericial;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.Date;

import javax.swing.JOptionPane;


public class SolicitudInformePericialController {
	private SolicitudInformePericialModel model;
	private SolicitudInformePericialView view;
	String nl = System.getProperty("line.separator");
	int dia,mes,año;
	String fechaLimite;
	
	public SolicitudInformePericialController(SolicitudInformePericialModel Model, SolicitudInformePericialView View) {
		this.model=Model;
		this.view=View;
		
		this.initView();
	}
	
	public void initView() {
		//Abre la ventana (sustituye al main generado por WindowBuilder)
		view.getFrame().setVisible(true);
	}
	
	public void initController() {
		Date fechaActual = new Date();
		view.getBAceptar().addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent evt) {
				if(!view.getDia().getText().isEmpty()) {
					dia=Integer.parseInt(view.getDia().getText());
					mes=Integer.parseInt(view.getMes().getText());
					año=Integer.parseInt(view.getAño().getText());
					fechaLimite=view.getAño().getText()+"-"+view.getMes().getText()+"-"+view.getDia().getText();
					if (model.fechaBien(dia,mes,año)) {
						model.nuevoInformePericial(view.getNombre().getText(),Integer.parseInt(view.getTelefono().getText()),
								view.getCorreoElectronico().getText(),view.getDescripcion().getText(),
								fechaLimite,fechaActual,"Pendiente");
						JOptionPane.showMessageDialog(null, "Solicitud tramitada");
						vaciarCampos();
						
					}else {
						JOptionPane.showMessageDialog(null,"Fecha erronea (tiene que existir y ser mayor que hoy)","Valor mal introducido",JOptionPane.WARNING_MESSAGE);
						view.getDia().setText(null);
						view.getMes().setText(null);
						view.getAño().setText(null);
					}
				}else {
					model.nuevoInformePericial(view.getNombre().getText(),Integer.parseInt(view.getTelefono().getText()),
							view.getCorreoElectronico().getText(),view.getDescripcion().getText(),
							null,fechaActual,"Pendiente");
					JOptionPane.showMessageDialog(null, "Solicitud tramitada");
					vaciarCampos();
				}
				
			}

		});
	}
	public void vaciarCampos() {
		view.getNombre().setText(null);
		view.getTelefono().setText(null);
		view.getCorreoElectronico().setText(null);
		view.getDescripcion().setText(null);
		view.getDia().setText(null);
		view.getMes().setText(null);
		view.getAño().setText(null);
		view.getBAceptar().setEnabled(false);
	}
}
