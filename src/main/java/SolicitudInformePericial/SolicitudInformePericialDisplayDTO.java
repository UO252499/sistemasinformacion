package SolicitudInformePericial;

public class SolicitudInformePericialDisplayDTO {
	private String nombre;
	private String descripcion;
	private String fechalimite;
	private String fechaemision;
	private int id;
	
	
	public SolicitudInformePericialDisplayDTO() {}
	public SolicitudInformePericialDisplayDTO(String Nombre, String Descripcion,String Fechalimite,String Fechaemision,int ID) {
		this.nombre=Nombre;
		this.descripcion=Descripcion;
		this.fechalimite=Fechalimite;
		this.fechaemision=Fechaemision;
		this.id=ID;
	}
	public String getNombre() { return this.nombre; }
	public void setNombre(String nombre) { this.nombre = nombre; }
	
	public String getDescripcion() { return this.descripcion; }
	public void setDescripcion(String Descripcion) { this.descripcion = Descripcion; }
	
	public String getFechalimite() { return this.fechalimite; }
	public void setFechalimite(String Fechalimite) { this.fechalimite = Fechalimite; }
	
	public String getFechaemision() { return this.fechaemision; }
	public void setFechaemision(String Fechaemision) { this.fechaemision = Fechaemision; }
	
	public int getId_pericial() { return this.id; }
	public void setId_pericial(int ID) { this.id=ID; }

}
