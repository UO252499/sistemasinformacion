package SolicitudInformePericial;

import java.util.Calendar;
import java.util.Date;
import java.util.List;

import util.Database;
import util.Util;

public class SolicitudInformePericialModel {
	private Database db= new Database();
	private static Calendar fecha = Calendar.getInstance();
	
	public void nuevoInformePericial(String nombre, int telefono, String correo, String descripcion, String fechaLimite,
		Date fechaActual, String estado) {
		int id=obtenerId();
		String actual=Util.dateToIsoString(fechaActual);
		String sql="insert into pericial(id_pericial,nombre,telefono,correoelectronico,descripcion,fechalimite,fechaemision,estado) "
				+ "values (?,?,?,?,?,?,?,?)";
		db.executeUpdate(sql,id,nombre,telefono,correo,descripcion,fechaLimite,actual,estado);
	}
	
	public int obtenerId() {
		String sql="SELECT MAX(id_pericial) FROM pericial";
		List<Object[]>rows=db.executeQueryArray(sql);
		if(rows.get(0)[0]==null) {
			return 1;
		}else {
			return (Integer)rows.get(0)[0]+1;
		}
	}
	
	public boolean fechaBien(int dia, int mes, int año) {
		if(mayorQueHoy(dia,mes,año)) {
			if(mes>=1 && mes<=12) {
				if(mes==1 || mes==3 || mes==5 || mes==7 || mes==8 || mes==10 || mes==12) {
					if (dia>=1 && dia <=31) {
						return true;
					}
				}else if(mes==2) {
					if(esBisiesto(año)) {
						if (dia>=1 && dia <=29) {
							return true;
						}
					}else {
						if (dia>=1 && dia <=28) {
							return true;
						}
					}
				}else {
					if (dia>=1 && dia <=30) {
						return true;
					}
				}
			}
		}
		return false;
	}


	private boolean esBisiesto(int año) {
		if ((año % 4 == 0) && ((año % 100 != 0) || (año % 400 == 0))) {
			return true;
		}else {
			return false;
		}
	}


	public static boolean mayorQueHoy(int dia, int mes, int año) {
		if(año==fecha.get(Calendar.YEAR)) {
			if(mes==(fecha.get(Calendar.MONTH)+1)) {
				if(dia>fecha.get(Calendar.DAY_OF_MONTH)) {
					return true;
				}
			}else if (mes>(fecha.get(Calendar.MONTH)+1)) {
					return true;
			}
		}else if(año>fecha.get(Calendar.YEAR)){
			return true;
		}
		return false;
	}
}
