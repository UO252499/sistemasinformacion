package SolicitudInformePericial;

import java.awt.Font;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.KeyAdapter;
import java.awt.event.KeyEvent;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.SwingConstants;
import javax.swing.JSeparator;
import javax.swing.JTextField;
import javax.swing.JTextArea;
import java.awt.event.FocusAdapter;
import java.awt.event.FocusEvent;

public class SolicitudInformePericialView {
	
	private JFrame frame;
	private JLabel LTitulo;
	private JSeparator separator;
	private JLabel LNombre;
	private JTextField TFNombre;
	private JLabel LTelefono;
	private JTextField TFTelefono;
	private JLabel LCorreoElectronico;
	private JTextField TFCorreoElectronico;
	private JLabel LDescripcion;
	private JTextArea TADescripcion;
	private JLabel LFechaLimite;
	private JTextField TFDia;
	private JTextField TFMes;
	private JTextField TFAño;
	private JButton BAceptar;
	private JButton BCancelar;
	private JLabel LPersona;
	private JLabel lblmximoCaracteres;
	
	public SolicitudInformePericialView() {
		initialize();
	}
	
	
	private void initialize() {
		//Frame
		frame = new JFrame();
		frame.setTitle("Solicitud Informe Pericial");
		frame.setName("Informe Pericial");
		frame.setBounds(0, 0, 500, 450);
		frame.setDefaultCloseOperation(JFrame.DISPOSE_ON_CLOSE);
		frame.getContentPane().setLayout(null);
		frame.setLocationRelativeTo(null);
		
		
		//Titulo
		LTitulo = new JLabel("Solicitud de Informe Pericial");
		LTitulo.setHorizontalAlignment(SwingConstants.CENTER);
		LTitulo.setFont(new Font("Verdana Pro", Font.BOLD, 18));
		frame.getContentPane().add(LTitulo);
		LTitulo.setBounds(0, 16, 478, 20);
		
		
		//Separador
		separator = new JSeparator();
		separator.setBounds(0, 40, 478, 2);
		frame.getContentPane().add(separator);
		
		
		//Nombre
		LNombre = new JLabel("Nombre");
		frame.getContentPane().add(LNombre);
		LNombre.setBounds(15, 77, 69, 20);
		
		TFNombre = new JTextField();
		TFNombre.addKeyListener(new KeyAdapter() {
			public void keyTyped(KeyEvent evt) {
				char c=evt.getKeyChar();
		        if (Character.isDigit(c)){
		            evt.consume();
		            JOptionPane.showMessageDialog(null, "Ingresar solo letras","Valor mal introducido",JOptionPane.WARNING_MESSAGE);
		        }
		        HabilitarBoton();
			}
		});
		frame.getContentPane().add(TFNombre);
		TFNombre.setBounds(84, 77, 125, 20);
	
		
		//Telefono
		LTelefono = new JLabel("Telefono");
		frame.getContentPane().add(LTelefono);
		LTelefono.setBounds(224, 77, 69, 20);
		
		TFTelefono = new JTextField();
		TFTelefono.addKeyListener(new KeyAdapter() {
			public void keyTyped(KeyEvent evt) {
				char c=evt.getKeyChar();
		        if (Character.isLetter(c)){
		            evt.consume();
		            JOptionPane.showMessageDialog(null, "Ingresar solo numeros","Valor mal introducido",JOptionPane.WARNING_MESSAGE);
		        }
		        if(TFTelefono.getText().length()>=9) {
					evt.consume();
				}
		        HabilitarBoton();
			}
			@Override
			public void keyReleased(KeyEvent arg0) {
				HabilitarBoton();
			}
		});
		frame.getContentPane().add(TFTelefono);
		TFTelefono.setBounds(308, 77, 143, 20);
		
		
		//Correo Electrónico
		LCorreoElectronico = new JLabel("Correo Electrónico");
		frame.getContentPane().add(LCorreoElectronico);
		LCorreoElectronico.setBounds(58, 130, 136, 20);
		
		TFCorreoElectronico = new JTextField();
		TFCorreoElectronico.addFocusListener(new FocusAdapter() {
			@Override
			public void focusLost(FocusEvent e) {
				if(!esEmail(TFCorreoElectronico.getText())) {
					JOptionPane.showMessageDialog(null, "Correo Electronico mal introducido","Valor mal introducido",JOptionPane.ERROR_MESSAGE);
					TFCorreoElectronico.setText(null);
				}
			}
		});
		frame.getContentPane().add(TFCorreoElectronico);
		TFCorreoElectronico.setBounds(209, 130, 163, 20);
		TFCorreoElectronico.addKeyListener(new KeyAdapter() {
			public void keyTyped(KeyEvent evt) {
				HabilitarBoton();
			}
		});
		
		
		//Descripción de la información
		LDescripcion = new JLabel("Descripcion del Informe");
		frame.getContentPane().add(LDescripcion);
		LDescripcion.setBounds(12, 177, 182, 20);
		
		TADescripcion = new JTextArea();
		TADescripcion.setLineWrap(true);
		TADescripcion.setWrapStyleWord(true);
		TADescripcion.setBounds(209, 166, 254, 71);
		frame.getContentPane().add(TADescripcion);
		TADescripcion.addKeyListener(new KeyAdapter() {
			public void keyTyped(KeyEvent evt) {
				if(TADescripcion.getText().length()>=100) {
					evt.consume();
				}
				HabilitarBoton();
			}
		});
		
		
		
		//Fecha limite del informe
		LFechaLimite = new JLabel("Fecha limite del informe");
		frame.getContentPane().add(LFechaLimite);
		LFechaLimite.setBounds(15, 237, 176, 53);
		JLabel LOpcional = new JLabel("(Opcional)");
		LOpcional.setFont(new Font("Tahoma", Font.PLAIN, 10));
		frame.getContentPane().add(LOpcional);
		LOpcional.setBounds(71, 265, 79, 34);
		
		TFDia = new JTextField();
		frame.getContentPane().add(TFDia);
		TFDia.setBounds(209, 253, 20, 20);
		TFDia.addKeyListener(new KeyAdapter() {
			public void keyTyped(KeyEvent evt) {
				char c=evt.getKeyChar();
		        if (Character.isLetter(c)){
		            evt.consume();
		            JOptionPane.showMessageDialog(null, "Ingresar solo numeros","Valor mal introducido",JOptionPane.WARNING_MESSAGE);
		        }
		        if(TFDia.getText().length()>=2) {
					evt.consume();
				}
		        HabilitarBoton();
			}
			@Override
			public void keyReleased(KeyEvent arg0) {
				HabilitarBoton();
			}
		});
		
		TFMes = new JTextField();
		frame.getContentPane().add(TFMes);
		TFMes.setBounds(241, 253, 20, 20);
		TFMes.addKeyListener(new KeyAdapter() {
			public void keyTyped(KeyEvent evt) {
				char c=evt.getKeyChar();
		        if (Character.isLetter(c)){
		            evt.consume();
		            JOptionPane.showMessageDialog(null, "Ingresar solo numeros","Valor mal introducido",JOptionPane.WARNING_MESSAGE);
		        }
		        if(TFMes.getText().length()>=2) {
					evt.consume();
				}
		        HabilitarBoton();
			}
			@Override
			public void keyReleased(KeyEvent arg0) {
				HabilitarBoton();
			}
		});
		
		TFAño = new JTextField();
		frame.getContentPane().add(TFAño);
		TFAño.setBounds(269, 253, 42, 20);
		TFAño.addKeyListener(new KeyAdapter() {
			public void keyTyped(KeyEvent evt) {
				char c=evt.getKeyChar();
		        if (Character.isLetter(c)){
		            evt.consume();
		            JOptionPane.showMessageDialog(null, "Ingresar solo numeros","Valor mal introducido",JOptionPane.WARNING_MESSAGE);
		        }
		        if(TFAño.getText().length()>=4) {
					evt.consume();
				}
		        HabilitarBoton();
			}
			@Override
			public void keyReleased(KeyEvent arg0) {
				HabilitarBoton();
			}
		});
		
		
		//Botones
		BAceptar = new JButton("Aceptar");
		BAceptar.setEnabled(false);
		frame.getContentPane().add(BAceptar);
		BAceptar.setBounds(107, 324, 102, 34);
		
		BCancelar = new JButton("Salir");
		frame.getContentPane().add(BCancelar);
		BCancelar.setBounds(269, 324, 111, 34);
		
		BCancelar.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent evt) {
				frame.setVisible(false);
			}
		});
		
		//Separadores de la fecha
		JLabel label1 = new JLabel("-");
		label1.setBounds(232, 250, 12, 20);
		frame.getContentPane().add(label1);
		
		JLabel label2 = new JLabel("-");
		label2.setBounds(263, 250, 12, 20);
		frame.getContentPane().add(label2);
		
		LPersona = new JLabel("(Persona o Entidad)");
		LPersona.setFont(new Font("Tahoma", Font.PLAIN, 10));
		LPersona.setBounds(5, 95, 90, 20);
		frame.getContentPane().add(LPersona);
		
		lblmximoCaracteres = new JLabel("(Máximo 100 caracteres)");
		lblmximoCaracteres.setFont(new Font("Tahoma", Font.PLAIN, 10));
		lblmximoCaracteres.setBounds(44, 201, 128, 20);
		frame.getContentPane().add(lblmximoCaracteres);
	}
	
	//Getters y Setters anyadidos para acceso desde el controlador (repersentacion compacta)
	public JFrame getFrame() { return this.frame; }
	public JTextField getNombre() { return this.TFNombre; }
	public JTextField getTelefono() { return this.TFTelefono; }
	public JTextField getCorreoElectronico() { return this.TFCorreoElectronico; }
	public JTextArea getDescripcion() { return this.TADescripcion; }
	public JTextField getDia() { return this.TFDia; }
	public JTextField getMes() { return this.TFMes; }
	public JTextField getAño() { return this.TFAño; }
	public JButton getBAceptar() { return this.BAceptar; }


	//Funciones
	public void HabilitarBoton() {
		if(!TFNombre.getText().isEmpty() && !TFTelefono.getText().isEmpty()
				 && (TFTelefono.getText().length()==9) && !TFCorreoElectronico.getText().isEmpty()
				 && !TADescripcion.getText().isEmpty() && 
				 ((TFDia.getText().isEmpty() && TFMes.getText().isEmpty() && TFAño.getText().isEmpty())
						 || (!TFDia.getText().isEmpty() && !TFMes.getText().isEmpty() && !TFAño.getText().isEmpty()))
				 && esEmail(TFCorreoElectronico.getText())) {
			BAceptar.setEnabled(true);
		}
		else {
			BAceptar.setEnabled(false);
		}
	}
	
	public boolean esEmail(String correo) {
		Pattern pat=null;
		Matcher mat =null;
		pat= Pattern.compile("^[\\w\\-\\_\\+]+(\\.[\\w\\-\\_]+)*@([A-Za-z0-9-]+\\.)+[A-Za-z]{2,4}$");
		mat=pat.matcher(correo);
		if(mat.find()) {
			return true;
		}else {
			return false;
		}
	}
}
