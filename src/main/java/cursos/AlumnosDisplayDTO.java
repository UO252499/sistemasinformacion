package cursos;
/**
 * Cada una de las filas que muestran al usuario las carreras y su estado
 */

public class AlumnosDisplayDTO {
	
	private String nombre;
	private String fecha_inscripcion;
	private String estado;
	private String tipo_ext;
	private String precio;
	private String numero_colegiado;
	private String id_curso;
	private String fecha_cancelacion;
	private String observaciones;
	
	public AlumnosDisplayDTO() {}
	public AlumnosDisplayDTO(String rowNombre, String rowFecha, String rowEstado, 
			String rowPrecio, String numColeg, String idCurso) {
		this.nombre=rowNombre;
		this.fecha_inscripcion=rowFecha;
		this.estado=rowEstado;
		this.precio=rowPrecio;
		this.numero_colegiado=numColeg;
		this.setId_curso(idCurso);
	}
	public String getNombre() {
		return nombre;
	}
	public void setNombre(String nombre) {
		this.nombre = nombre;
	}
	public String getFecha_inscripcion() {
		return fecha_inscripcion;
	}
	public void setFecha_inscripcion(String fecha_inscripcion) {
		this.fecha_inscripcion = fecha_inscripcion;
	}
	public String getEstado() {
		return estado;
	}
	public void setEstado(String estado) {
		this.estado = estado;
	}
	public String getPrecio() {
		return precio;
	}
	public void setPrecio(String precio) {
		this.precio = precio;
	}
	public String getNumero_colegiado() {
		return numero_colegiado;
	}
	public void setNumero_colegiado(String numero_colegiado) {
		this.numero_colegiado = numero_colegiado;
	}
	public String getId_curso() {
		return id_curso;
	}
	public void setId_curso(String id_curso) {
		this.id_curso = id_curso;
	}
	public String getFecha_cancelacion() {
		return fecha_cancelacion;
	}
	public void setFecha_cancelacion(String fecha_cancelacion) {
		this.fecha_cancelacion = fecha_cancelacion;
	}
	public String getObservaciones() {
		return observaciones;
	}
	public void setObservaciones(String observaciones) {
		this.observaciones = observaciones;
	}
	public String getTipo_ext() {
		return tipo_ext;
	}
	public void setTipo_ext(String tipo_ext) {
		this.tipo_ext = tipo_ext;
	}
	
	
}
