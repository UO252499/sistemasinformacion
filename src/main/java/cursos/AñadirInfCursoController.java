package cursos;


import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;
import java.util.List;

import javax.swing.table.TableModel;

import util.SwingUtil;

/**
 * Controlador para la funcionalidad de visualizacion de carreras para la inscripcion.
 * Es el punto de entrada de esta pantalla que se invocará:
 * -instanciando el controlador con la vista y el modelo
 * -ejecutando initController que instalara los manejadores de eventos
 */
public class AñadirInfCursoController {
	private CursosModel model;
	private AñadirInfCursoView view;
	private int id_curso;

	public AñadirInfCursoController(CursosModel m, AñadirInfCursoView v, int id) {
		this.model = m;
		this.view = v;
		this.id_curso=id;
		//no hay inicializacion especifica del modelo, solo de la vista
		this.initView();
	}
	/**
	 * Inicializacion del controlador: anyade los manejadores de eventos a los objetos del UI.
	 * Cada manejador de eventos se instancia de la misma forma, para que invoque un metodo privado
	 * de este controlador, encerrado en un manejador de excepciones generico para mostrar ventanas
	 * emergentes cuando ocurra algun problema o excepcion controlada.
	 */
	public void initController() {
		//ActionListener define solo un metodo actionPerformed(), es un interfaz funcional que se puede invocar de la siguiente forma:
		//view.getBtnTablaCarreras().addActionListener(e -> getListaCarreras());
		//ademas invoco el metodo que responde al listener en el exceptionWrapper para que se encargue de las excepciones
		view.getBtnAceptar().addActionListener(e -> SwingUtil.exceptionWrapper(() -> AñadirInf()));
		view.getBtnCancelar().addActionListener(e -> SwingUtil.exceptionWrapper(() -> Cancelar()));
		
		//En el caso del mouse listener (para detectar seleccion de una fila) no es un interfaz funcional puesto que tiene varios metodos
		//ver discusion: https://stackoverflow.com/questions/21833537/java-8-lambda-expressions-what-about-multiple-methods-in-nested-class
		view.getTable().addMouseListener(new MouseAdapter() {
			@Override
			public void mouseReleased(MouseEvent e) {
				//no usa mouseClicked porque al establecer seleccion simple en la tabla de carreras
				//el usuario podria arrastrar el raton por varias filas e interesa solo la ultima
				SwingUtil.exceptionWrapper(() -> seleccion());
			}
		});


	}
	
	public void initView() {
		this.updatelabel();
		this.getListaProfesores();
		view.getBtnAceptar().setEnabled(false);
		//Abre la ventana (sustituye al main generado por WindowBuilder)		
		view.getFrame().setVisible(true); 
	}
	
	public void seleccion(){	
		view.getBtnAceptar().setEnabled(true);
	}
	
	public void updatelabel(){
		CursoEntity curso=model.getCurso(id_curso);	
		view.setLblCurso("CURSO "+id_curso+": "+curso.getTitulo());
	}
	
	public void getListaProfesores() {
		List<ProfesorDisplayDTO> profesores=model.getListaProfesores();
		TableModel tmodel=SwingUtil.getTableModelFromPojos(profesores, new String[] {"dni","nombre"});
		view.getTable().setModel(tmodel);
		view.getTable().removeColumn(view.getTable().getColumnModel().getColumn(0));
	}
	
	public void AñadirInf() {
		String dni=SwingUtil.getSelectedKey(view.getTable());
		String recurso=view.getTextFieldRecurso().getText();
		int precioColeg=(Integer) view.getSpinnerPrecioColeg().getValue();
		int precioPreColeg=(Integer) view.getSpinnerPrecioPreColeg().getValue();
		int precioEst=(Integer) view.getSpinnerPrecioEst().getValue();
		int precioPersAut=(Integer) view.getSpinnerPrecioPersAut().getValue();

		
		if(ValidarCampos(recurso, precioColeg,precioPreColeg,precioEst,precioPersAut)){
			model.añadirInf(id_curso,dni,recurso,precioColeg,precioPreColeg,precioEst,precioPersAut);
			CursoController controller= new CursoController(new CursosModel(), new CursosView());
			controller.initController();
			view.getFrame().setVisible(false);
		}
	}
	
	public void Cancelar() {
		CursoController controller= new CursoController(new CursosModel(), new CursosView());
		controller.initController();
		view.getFrame().setVisible(false);
	}
	
	public boolean ValidarCampos(String recurso, int precioColeg, int precioPreColeg, int precioEst, int precioPersAut) {
		if (recurso.isEmpty()) {
			view.NoHayRecurso();
			return false;
			}
		else if (precioColeg==0){
			view.NoHayPrecioColeg();
			return false;
		}
		else if (precioPreColeg==0){
			view.NoHayPrecioPreColeg();
			return false;
		}
		else if (precioEst==0){
			view.NoHayPrecioEst();
			return false;
		}
		else if (precioPersAut==0){
			view.NoHayPrecioPersAut();
			return false;
		}
		else return true;
	}
}
