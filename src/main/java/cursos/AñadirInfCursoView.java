package cursos;

import java.awt.EventQueue;

import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JTextField;
import javax.swing.ListSelectionModel;
import javax.swing.JSpinner;
import javax.swing.JButton;
import javax.swing.JScrollPane;
import javax.swing.JTable;
import javax.swing.JSeparator;
import java.awt.Font;
import javax.swing.SwingConstants;
import java.awt.Color;

public class AñadirInfCursoView {

	public void setSpinnerPrecio(JSpinner spinnerPrecio) {
		this.spinnerPrecioColeg = spinnerPrecio;
	}
	private JFrame frmAñadirInf;
	private JButton btnAceptar;
	private JButton btnCancelar;
	private JSpinner spinnerPrecioColeg; 
	private JSpinner spinnerPrecioPreColeg; 
	private JSpinner spinnerPrecioEst; 
	private JSpinner spinnerPrecioPersAut; 
	private JTextField textFieldRecurso;
	private JTable table;
	private JLabel lblCurso;
	

	/**
	 * Launch the application.
	 */
	public static void main(String[] args) {
		EventQueue.invokeLater(new Runnable() {
			public void run() {
				try {
					AñadirInfCursoView window = new AñadirInfCursoView();
					window.frmAñadirInf.setVisible(true);
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		});
	}

	/**
	 * Create the application.
	 */
	public AñadirInfCursoView() {
		initialize();
	}

	/**
	 * Initialize the contents of the frame.
	 */
	private void initialize() {
		frmAñadirInf = new JFrame();
		frmAñadirInf.setTitle("Añadir Informacion");
		frmAñadirInf.setBounds(100, 100, 287, 364);
		frmAñadirInf.setDefaultCloseOperation(JFrame.DISPOSE_ON_CLOSE);
		frmAñadirInf.getContentPane().setLayout(null);
		frmAñadirInf.setLocationRelativeTo(null);
		
		JLabel lblProfesor = new JLabel("Seleccionar docente:");
		lblProfesor.setBounds(10, 36, 141, 14);
		frmAñadirInf.getContentPane().add(lblProfesor);
		
		JLabel lblRecursos = new JLabel("Recursos :");
		lblRecursos.setBounds(10, 148, 80, 14);
		frmAñadirInf.getContentPane().add(lblRecursos);
		
		JLabel lblPrecioColeg = new JLabel("Precio para colegiados:");
		lblPrecioColeg.setBounds(10, 181, 141, 14);
		frmAñadirInf.getContentPane().add(lblPrecioColeg);
		
		spinnerPrecioColeg = new JSpinner();
		spinnerPrecioColeg.setBounds(177, 178, 60, 20);
		frmAñadirInf.getContentPane().add(spinnerPrecioColeg);
		
		btnAceptar = new JButton("Aceptar");
		btnAceptar.setBounds(20, 291, 96, 23);
		frmAñadirInf.getContentPane().add(btnAceptar);
		
		btnCancelar = new JButton("Cancelar");
		btnCancelar.setBounds(148, 291, 89, 23);
		frmAñadirInf.getContentPane().add(btnCancelar);
		
		textFieldRecurso = new JTextField();
		textFieldRecurso.setColumns(10);
		textFieldRecurso.setBounds(83, 145, 138, 20);
		frmAñadirInf.getContentPane().add(textFieldRecurso);
		
		JLabel lblPrecioPreColeg = new JLabel("Precio para precolegiados:");
		lblPrecioPreColeg.setBounds(10, 206, 157, 14);
		frmAñadirInf.getContentPane().add(lblPrecioPreColeg);
		
		JLabel lblPrecioPersAut = new JLabel("Precio para personal autorizado:");
		lblPrecioPersAut.setBounds(10, 256, 193, 14);
		frmAñadirInf.getContentPane().add(lblPrecioPersAut);
		
		JLabel lblPrecioEst = new JLabel("Precio para estudiantes:");
		lblPrecioEst.setBounds(10, 231, 157, 14);
		frmAñadirInf.getContentPane().add(lblPrecioEst);
		
		spinnerPrecioPreColeg = new JSpinner();
		spinnerPrecioPreColeg.setBounds(177, 203, 60, 20);
		frmAñadirInf.getContentPane().add(spinnerPrecioPreColeg);
		
		spinnerPrecioEst = new JSpinner();
		spinnerPrecioEst.setBounds(177, 228, 60, 20);
		frmAñadirInf.getContentPane().add(spinnerPrecioEst);
		
		spinnerPrecioPersAut = new JSpinner();
		spinnerPrecioPersAut.setBounds(202, 253, 60, 20);
		frmAñadirInf.getContentPane().add(spinnerPrecioPersAut);
		
		table = new JTable();
		table.setName("tabProf");
		table.setSelectionMode(ListSelectionModel.SINGLE_SELECTION);
		table.setDefaultEditor(Object.class, null); //readonly
		
		JScrollPane scrollPane = new JScrollPane(table);
		scrollPane.setBounds(10, 54, 240, 62);
		frmAñadirInf.getContentPane().add(scrollPane);
		
		JSeparator separator = new JSeparator();
		separator.setBounds(10, 127, 240, 2);
		frmAñadirInf.getContentPane().add(separator);
		
		lblCurso = new JLabel("");
		lblCurso.setForeground(Color.BLACK);
		lblCurso.setHorizontalAlignment(SwingConstants.CENTER);
		lblCurso.setFont(new Font("Tahoma", Font.BOLD, 13));
		lblCurso.setBounds(10, 11, 251, 20);
		frmAñadirInf.getContentPane().add(lblCurso);
	}
	
	public JFrame getFrame() { return this.frmAñadirInf; }
	public JButton getBtnAceptar() { return btnAceptar;	}
	public JButton getBtnCancelar() { return btnCancelar;	}
	public JTextField getTextFieldRecurso() {	return textFieldRecurso;}
	public void setTextFieldRecurso(JTextField textFieldRecurso) {this.textFieldRecurso = textFieldRecurso;}
	public JSpinner getSpinnerPrecioColeg() {return spinnerPrecioColeg;}
	public void setSpinnerPrecioColeg(JSpinner spinnerPrecioColeg) {this.spinnerPrecioColeg = spinnerPrecioColeg;}
	public JSpinner getSpinnerPrecioPreColeg() {return spinnerPrecioPreColeg;}
	public void setSpinnerPrecioPreColeg(JSpinner spinnerPrecioPreColeg) {this.spinnerPrecioPreColeg = spinnerPrecioPreColeg;}
	public JSpinner getSpinnerPrecioEst() {return spinnerPrecioEst;}
	public void setSpinnerPrecioEst(JSpinner spinnerPrecioEst) {this.spinnerPrecioEst = spinnerPrecioEst;}
	public JSpinner getSpinnerPrecioPersAut() {return spinnerPrecioPersAut;}
	public void setSpinnerPrecioPersAut(JSpinner spinnerPrecioPersAut) {this.spinnerPrecioPersAut = spinnerPrecioPersAut;}
	public JTable getTable() {return table;}
	public void setTable(JTable table) {this.table = table;}
	public JLabel getLblCurso() {return lblCurso;}
	public void setLblCurso(String lblCurso) {this.lblCurso.setText(lblCurso);;}

	public void NoHayRecurso() {
		JOptionPane.showMessageDialog(this.getFrame(), "Introduzca un recurso para el curso","No puede haber campos vacios",JOptionPane.WARNING_MESSAGE);	
	}
	
	public void NoHayPrecioColeg() {
		JOptionPane.showMessageDialog(this.getFrame(), "Introduzca un precio para el colegiado","No puede haber campos vacios",JOptionPane.WARNING_MESSAGE);
		
	}
	
	public void NoHayPrecioPreColeg() {
		JOptionPane.showMessageDialog(this.getFrame(), "Introduzca un precio para el precolegiado","No puede haber campos vacios",JOptionPane.WARNING_MESSAGE);
		
	}
	
	public void NoHayPrecioEst() {
		JOptionPane.showMessageDialog(this.getFrame(), "Introduzca un precio para el estudiante","No puede haber campos vacios",JOptionPane.WARNING_MESSAGE);
		
	}
	
	public void NoHayPrecioPersAut() {
		JOptionPane.showMessageDialog(this.getFrame(), "Introduzca un precio para el personal autorizado","No puede haber campos vacios",JOptionPane.WARNING_MESSAGE);
		
	}
}
