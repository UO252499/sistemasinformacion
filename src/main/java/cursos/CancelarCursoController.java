package cursos;

import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;
import java.io.FileWriter;
import java.io.IOException;
import java.util.Date;
import java.util.List;

import javax.swing.table.DefaultTableModel;
import javax.swing.table.TableModel;

import cursos.AlumnosDisplayDTO;
import cursos.CursoDisplayDTO;
import util.SwingUtil;
import util.Util;

public class CancelarCursoController {
	private CancelarCursoView view;
	private CursosModel model;
	final String rutaArchivo="src/main/resources/deudas.txt";
	final String delim = ",";
	final String NEXT_LINE = "\n";
	Date fechaActual = new Date();


	public CancelarCursoController(CursosModel m, CancelarCursoView v) {
		this.model = m;
		this.view = v;
		this.initView();
	}

	public void initController() {
		//En el caso del mouse listener (para detectar seleccion de una fila) no es un interfaz funcional puesto que tiene varios metodos
		//ver discusion: https://stackoverflow.com/questions/21833537/java-8-lambda-expressions-what-about-multiple-methods-in-nested-class
		view.table.addMouseListener(new MouseAdapter() {
			@Override
			public void mouseReleased(MouseEvent e) {
				//no usa mouseClicked porque al establecer seleccion simple en la tabla de carreras
				//el usuario podria arrastrar el raton por varias filas e interesa solo la ultima
				SwingUtil.exceptionWrapper(() -> updateDetail());
			}
		});
	}
	
	public void initView() {
		this.actualizarTablaCursos();
		
		//Abre la ventana (sustituye al main generado por WindowBuilder)
		view.controlador=this;
	}
	
	public void actualizarTablaCursos() {
		List<CursoDisplayDTO> cursos=model.getListaCursosAbiertos();
		TableModel pmodel=SwingUtil.getTableModelFromPojos(cursos, new String[] {"id_curso", "titulo", "fecha_curso", "estado"});
		view.table.setModel(pmodel);
	}
	
	public void actualizarTablaCursosCancelados() {
		List<CursoDisplayDTO> cursos=model.getListaCursosCancelados();
		TableModel pmodel=SwingUtil.getTableModelFromPojos(cursos, new String[] {"id_curso", "titulo", "fecha_cancelacion", "estado"});
		view.table.setModel(pmodel);
	}
	
	public void CancelarCurso(int selectedRow) {
		String id_colegiado;
		String nombre;
		String precio;
		int i=0;
		String id_curso=(String)view.table.getModel().getValueAt(selectedRow, 0);
		int opcion=view.ConfirmarCancelacion(id_curso);
		if(opcion==0) {
			List<AlumnosDisplayDTO> alumnos=model.getListaAlumnosPagado(id_curso);
			if(!alumnos.isEmpty()) {
				try {
					FileWriter fw = new FileWriter(rutaArchivo);
					for (AlumnosDisplayDTO alumno : alumnos) {
						nombre=alumno.getNombre();
						precio=alumno.getPrecio();
						id_colegiado=alumno.getNumero_colegiado();
						id_curso=alumno.getId_curso();
						fw.append(id_colegiado).append(delim).append(id_curso).append(delim);
						fw.append(nombre).append(delim).append(Util.dateToIsoString(fechaActual));
						fw.append(delim).append(precio);
						if(id_colegiado.length()==9) {
							model.modificarInscripcionExt(id_colegiado, id_curso, precio);
						}
						else {
							model.modificarInscripcion(id_colegiado, id_curso, precio);
						}
						if(i<alumnos.size()-1) { 
							fw.append(NEXT_LINE);
						}
						i++;
					}
					fw.flush();
					fw.close();
					view.ficheroGenerado(rutaArchivo);
				}
				catch (IOException e) {
					// Error al crear el archivo, por ejemplo, el archivo 
					// está actualmente abierto.
					e.printStackTrace();
				}
			}
			else {
				view.ficheroNoGenerado();
			}
			model.modificarInscripcionPreInscritos(id_curso);
			model.modificarInscripcionPreInscritosExt(id_curso);
			model.cancelarCurso(id_curso);
			this.actualizarTablaCursos();
			DefaultTableModel dtm = (DefaultTableModel) view.getTabAlumnos().getModel();
		    dtm.setRowCount(0);
		}
		else {
			view.NoCancelado();
		}
	} 


	/**
	 * Al seleccionar un item de la tabla muestra el detalle con el valor del porcentaje de descuento
	 * de la carrera seleccinada y los valores de esta entidad
	 */
	public void updateDetail() {
		int selectedRow=view.table.getSelectedRow();
		TableModel tablaContenido = view.table.getModel();
		String id_curso=(String) tablaContenido.getValueAt(selectedRow, 0);
		List<AlumnosDisplayDTO> alumnos;
		if(view.getRdbtnVerCancelados().isSelected()) {
			alumnos=model.getListaAlumnosCancelados(id_curso);
			TableModel tmodel=SwingUtil.getTableModelFromPojos(alumnos, new String[] {"nombre", "fecha_cancelacion", "estado", "observaciones"});
			view.getTabAlumnos().setModel(tmodel);
			view.setIngresoNoInscripcion();
		}
		else {
			alumnos=model.getListaAlumnos(id_curso);
			TableModel tmodel=SwingUtil.getTableModelFromPojos(alumnos, new String[] {"nombre", "fecha_inscripcion", "estado", "precio"});
			view.getTabAlumnos().setModel(tmodel);
			List<AlumnosDisplayDTO> alumnosPagados=model.getListaAlumnosPagado(id_curso);
			if(alumnosPagados.isEmpty()) {
				view.setIngresoNoInscripcion();
			}
			else {
				int ingreso=model.getIngresosCurso(id_curso);
				view.setIngreso(String.valueOf(ingreso));
			}
		}
	}
}
