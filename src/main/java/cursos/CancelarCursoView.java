package cursos;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.FocusAdapter;
import java.awt.event.FocusEvent;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;

import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.JTable;
import javax.swing.ListSelectionModel;
import javax.swing.border.EmptyBorder;

import javax.swing.JRadioButton;

public class CancelarCursoView extends JFrame {
	
	/**
	 * 
	 */
	private static final long serialVersionUID = -4733947479616513730L;
	private JPanel contentPane;
	protected JTable table;
	protected CancelarCursoController controlador;
	private JButton btnCancelar;
	private JTable tabAlumnos;
	private JLabel ingreso;
	private JRadioButton rdbtnVerCancelados;
	private JLabel lblCursos;
	
	public CancelarCursoView() {
		setTitle("Cancelar curso");
		setLocationRelativeTo(null);
		setVisible(true);
		
		setDefaultCloseOperation(JFrame.DISPOSE_ON_CLOSE);
		setBounds(100, 100, 554, 430);
		contentPane = new JPanel();
		contentPane.setBorder(new EmptyBorder(5, 5, 5, 5));
		setContentPane(contentPane);
		contentPane.setLayout(null);
		setLocationRelativeTo(null);
		table = new JTable();
		{
			lblCursos = new JLabel("Cursos abiertos: ");
			lblCursos.setBounds(10, 11, 126, 21);
			contentPane.add(lblCursos);
		}
		{
			JScrollPane scrollPane = new JScrollPane();
			scrollPane.addFocusListener(new FocusAdapter() {
				@Override
				public void focusLost(FocusEvent e) {
					btnCancelar.setEnabled(false);
				}
			});
			scrollPane.setBounds(10, 43, 508, 125);
			contentPane.add(scrollPane);
			{
				table.addFocusListener(new FocusAdapter() {
					@Override
					public void focusGained(FocusEvent e) {
						if(!rdbtnVerCancelados.isSelected())
							btnCancelar.setEnabled(true);
					}
				});
				table.setSelectionMode(ListSelectionModel.SINGLE_SELECTION);
				scrollPane.setViewportView(table);
				table.setName("tabDetalle");
				table.setRowSelectionAllowed(true);
				{
					btnCancelar = new JButton("Cancelar curso");
					btnCancelar.addActionListener(new ActionListener() {
						public void actionPerformed(ActionEvent e) {
							if (table.getSelectedRow()!=-1) 
								controlador.CancelarCurso(table.getSelectedRow());
							else 
								FilaNoSeleccionada();

						}
					});
					btnCancelar.setEnabled(false);
					btnCancelar.setBounds(392, 179, 126, 23);
					contentPane.add(btnCancelar);
				}
				
				ingreso = new JLabel("##");
				ingreso.setBounds(177, 352, 49, 14);
				contentPane.add(ingreso);
				
				JLabel lblIngresos = new JLabel("Ingresos totales del curso:");
				lblIngresos.setBounds(10, 352, 152, 14);
				contentPane.add(lblIngresos);
				
				tabAlumnos = new JTable();
				tabAlumnos.setName("tabAlumnos");
				tabAlumnos.setSelectionMode(ListSelectionModel.SINGLE_SELECTION);
				tabAlumnos.setDefaultEditor(Object.class, null); //readonly
				
				JScrollPane tablePanel2 = new JScrollPane(tabAlumnos);
				tablePanel2.setBounds(10, 223, 508, 118);
				this.getContentPane().add(tablePanel2);
				
								
				JLabel lblInforCursos = new JLabel("Alumnos inscritos:");
				lblInforCursos.setBounds(10, 198, 166, 14);
				contentPane.add(lblInforCursos);
				
				rdbtnVerCancelados = new JRadioButton("Ver cursos cancelados");
				rdbtnVerCancelados.addMouseListener(new MouseAdapter() {
					@Override
					public void mouseClicked(MouseEvent e) {
						if (rdbtnVerCancelados.isSelected()) {
							setLblCursos("Cursos Cancelados:");
							controlador.actualizarTablaCursosCancelados();
							btnCancelar.setEnabled(false);
							lblInforCursos.setText("Inscripciones canceladas:");
						}
						else {
							setLblCursos("Cursos Abiertos:");
							lblInforCursos.setText("Alumnos inscritos:");
							controlador.actualizarTablaCursos();
						}
					}
				});
				rdbtnVerCancelados.setBounds(324, 10, 173, 23);
				contentPane.add(rdbtnVerCancelados);
				table.setDefaultEditor(Object.class, null); //readonly;
			}
		}
	}
	
	private void FilaNoSeleccionada() {
		JOptionPane.showMessageDialog(this, "Seleccione un curso para cancelar");							
	}
	
	public void ficheroGenerado(String rutaArchivo) {
		JOptionPane.showMessageDialog(this, "Se ha generado un fichero con las deudas en: "+rutaArchivo, "Se ha cancelado el curso", 1);
	}
	
	public void ficheroNoGenerado() {
		JOptionPane.showMessageDialog(this, "El curso no tiene inscripciones pagadas, no se genero ninguna deuda", "Se ha cancelado el curso", 1);
	}
	
	public void NoCancelado() {
		JOptionPane.showMessageDialog(this, "No se ha cancelado el curso", "Curso no cancelado", 1);
	}
	
	public int ConfirmarCancelacion(String curso) {
		return JOptionPane.showConfirmDialog(this, "¿Seguro que desea cancelar el curso "+curso+" ?", "Confirmar cancelacion", JOptionPane.YES_NO_OPTION, JOptionPane.QUESTION_MESSAGE);
	}
	
	public JLabel getIngreso() {return ingreso;}
	public void setIngreso(String ingreso) {this.ingreso.setText(ingreso+"€");}
	public void setIngresoNoInscripcion() {this.ingreso.setText("N/A");	}
	public JTable getTabAlumnos() { return tabAlumnos; }
	public void setTabAlumnos(JTable tabAlumnos) { this.tabAlumnos = tabAlumnos; }
	public JLabel getLblCursos() {return lblCursos;}
	public void setLblCursos(String lblCursos) {this.lblCursos.setText(lblCursos);}
	public JRadioButton getRdbtnVerCancelados() {return rdbtnVerCancelados;}
	public void setRdbtnVerCancelados(JRadioButton rdbtnVerCancelados) {this.rdbtnVerCancelados = rdbtnVerCancelados;}
	
}
