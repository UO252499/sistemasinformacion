package cursos;


import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;
import java.util.List;
import javax.swing.table.TableModel;
import util.SwingUtil;

public class CursoController {
	private CursosModel model;
	private CursosView view;
	private String lastSelectedKey=""; //recuerda la ultima fila seleccionada para restaurarla cuando cambie la tabla de carreras

	
	public CursoController(CursosModel m, CursosView v) {
		this.model = m;
		this.view = v;
		//no hay inicializacion especifica del modelo, solo de la vista
		this.initView();
	}
	/**
	 * Inicializacion del controlador: anyade los manejadores de eventos a los objetos del UI.
	 * Cada manejador de eventos se instancia de la misma forma, para que invoque un metodo privado
	 * de este controlador, encerrado en un manejador de excepciones generico para mostrar ventanas
	 * emergentes cuando ocurra algun problema o excepcion controlada.
	 */
	public void initController() {
		//ActionListener define solo un metodo actionPerformed(), es un interfaz funcional que se puede invocar de la siguiente forma:
		//view.getBtnTablaCarreras().addActionListener(e -> getListaCarreras());
		//ademas invoco el metodo que responde al listener en el exceptionWrapper para que se encargue de las excepciones
		view.getBtnNuevoCurso().addActionListener(e -> SwingUtil.exceptionWrapper(() -> NuevoCurso()));
		view.getBtnAñadirInf().addActionListener(e -> SwingUtil.exceptionWrapper(() -> AñadirInf()));


		//En el caso del mouse listener (para detectar seleccion de una fila) no es un interfaz funcional puesto que tiene varios metodos
		//ver discusion: https://stackoverflow.com/questions/21833537/java-8-lambda-expressions-what-about-multiple-methods-in-nested-class
		view.getTabCurso().addMouseListener(new MouseAdapter() {
			@Override
			public void mouseReleased(MouseEvent e) {
				//no usa mouseClicked porque al establecer seleccion simple en la tabla de carreras
				//el usuario podria arrastrar el raton por varias filas e interesa solo la ultima
				SwingUtil.exceptionWrapper(() -> updateDetail());
			}
		});


	}
	
	public void initView() {
		this.getListaCursos();
		view.getBtnAñadirInf().setEnabled(false);
		
		//Abre la ventana (sustituye al main generado por WindowBuilder)
		view.getFrmCursos().setVisible(true); 
	}
	/**
	 * La obtencion de la lista de carreras solo necesita obtener la lista de objetos del modelo 
	 * y usar metodo de SwingUtil para crear un tablemodel que se asigna finalmente a la tabla.
	 */
	public void getListaCursos() {
		List<CursoDisplayDTO> cursos=model.getListaCursos();
		TableModel tmodel=SwingUtil.getTableModelFromPojos(cursos, new String[] {"id_curso","titulo", "fecha_curso","estado"});
		view.getTabCurso().setModel(tmodel);
		view.getTabCurso().removeColumn(view.getTabCurso().getColumnModel().getColumn(0));

	}
	
	public void NuevoCurso() {
		NuevoCursoController controller= new NuevoCursoController(new CursosModel(), new NuevoCursoView());
		controller.initController();
		view.getFrmCursos().setVisible(false);
	}
	
	public void AñadirInf() {
		this.lastSelectedKey=SwingUtil.getSelectedKey(view.getTabCurso());
		int idCurso=Integer.parseInt(this.lastSelectedKey);
		if(model.admiteModificacion(idCurso)) {
			AñadirInfCursoController controller= new AñadirInfCursoController(new CursosModel(), new AñadirInfCursoView(),idCurso);
			controller.initController();
			view.getFrmCursos().setVisible(false);
		}
		else {
			view.NoModificar();
		}

	}
	
	public void updateDetail() {
		//Obtiene la clave seleccinada y la guarda para recordar la seleccion en futuras interacciones
		this.lastSelectedKey=SwingUtil.getSelectedKey(view.getTabCurso());
		int idCurso=Integer.parseInt(this.lastSelectedKey);
				
		//Detalles de la carrera seleccionada
		CursoEntity curso=model.getCurso(idCurso);
		TableModel tmodel=SwingUtil.getRecordModelFromPojo(curso, new String[] {"id_curso", "titulo", "fecha_curso", "profesor",
				"recursos","precio_colegiado","precio_precolegiado","precio_estudiante","precio_pers_autorizado","estado"});
		view.getTabDetalle().setModel(tmodel);
		//SwingUtil.autoAdjustColumns(view.getDetalleCarrera());
		view.getBtnAñadirInf().setEnabled(true);
	}
}
