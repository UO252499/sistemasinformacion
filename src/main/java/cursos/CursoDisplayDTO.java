package cursos;
/**
 * Cada una de las filas que muestran al usuario las carreras y su estado
 */

public class CursoDisplayDTO {
	
	private String id_curso;
	private String titulo;
	private String fecha_inicio_inscripcion;
	private String fecha_fin_inscripcion;
	private String fecha_curso;
	private String fecha_cancelacion;
	private String precio_colegiado;
	private String precio_precolegiado;
	private String precio_estudiante;
	private String precio_pers_autorizado;
	private String plazas;
	private String plazas_disponibles;
	private String estado;
	
	public CursoDisplayDTO() {}
	public CursoDisplayDTO(String rowTitulo, String rowFechaInicio, String rowFechaFin, String fecha, String rowEstado) {
		this.titulo=rowTitulo;
		this.fecha_inicio_inscripcion=rowFechaInicio;
		this.fecha_fin_inscripcion=rowFechaFin;
		this.fecha_curso=fecha;
		this.estado=rowEstado;
	}
	
	public CursoDisplayDTO(String id_curso, String titulo, String fecha_inicio_inscripcion,
			String fecha_fin_inscripcion,  String precioColeg, String plazas, String estado) {
		super();
		this.id_curso = id_curso;
		this.titulo = titulo;
		this.fecha_inicio_inscripcion = fecha_inicio_inscripcion;
		this.fecha_fin_inscripcion = fecha_fin_inscripcion;
		this.precio_colegiado = precioColeg;
		this.plazas = plazas;
		this.estado = estado;
	}
	
	public CursoDisplayDTO(String id_curso, String titulo, String fecha_inicio_inscripcion,
			String fecha_fin_inscripcion, String fecha_curso, String precio_colegiado, String plazas_disponibles, String plazas, String estado) {
		super();
		this.id_curso = id_curso;
		this.titulo = titulo;
		this.fecha_inicio_inscripcion = fecha_inicio_inscripcion;
		this.fecha_fin_inscripcion = fecha_fin_inscripcion;
		this.fecha_curso = fecha_curso;
		this.precio_colegiado = precio_colegiado;
		this.plazas_disponibles=plazas_disponibles;
		this.plazas = plazas;
		this.estado = estado;
	}
	
	public String getFecha_curso() {
		return fecha_curso;
	}
	public void setFecha_curso(String fecha_curso) {
		this.fecha_curso = fecha_curso;
	}
	public String getId_curso() {
		return id_curso;
	}
	public void setId_curso(String id_curso) {
		this.id_curso = id_curso;
	}
	public String getTitulo() {
		return titulo;
	}
	public void setTitulo(String titulo) {
		this.titulo = titulo;
	}
	public String getFecha_inicio_inscripcion() {
		return fecha_inicio_inscripcion;
	}
	public void setFecha_inicio_inscripcion(String fecha_inicio_inscripcion) {
		this.fecha_inicio_inscripcion = fecha_inicio_inscripcion;
	}
	public String getFecha_fin_inscripcion() {
		return fecha_fin_inscripcion;
	}
	public void setFecha_fin_inscripcion(String fecha_fin_inscripcion) {
		this.fecha_fin_inscripcion = fecha_fin_inscripcion;
	}
	
	public String getPrecio_colegiado() {
		return precio_colegiado;
	}
	public void setPrecio_colegiado(String precio_colegiado) {
		this.precio_colegiado = precio_colegiado;
	}
	
	public String getPrecio_precolegiado() {
		return precio_precolegiado;
	}
	public void setPrecio_precolegiado(String precio_precolegiado) {
		this.precio_precolegiado = precio_precolegiado;
	}
	public String getPrecio_estudiante() {
		return precio_estudiante;
	}
	public void setPrecio_estudiante(String precio_estudiante) {
		this.precio_estudiante = precio_estudiante;
	}
	public String getPrecio_pers_autorizado() {
		return precio_pers_autorizado;
	}
	public void setPrecio_pers_autorizado(String precio_pers_autorizado) {
		this.precio_pers_autorizado = precio_pers_autorizado;
	}
	public String getEstado() {
		return estado;
	}
	public void setEstado(String estado) {
		this.estado = estado;
	}

	public String getPlazas() {
		return plazas;
	}

	public void setPlazas(String plazas) {
		this.plazas = plazas;
	}
	public String getPlazas_disponibles() {
		return plazas_disponibles;
	}
	public void setPlazas_disponibles(String i) {
		this.plazas_disponibles = i;
	}
	public String getFecha_cancelacion() {
		return fecha_cancelacion;
	}
	public void setFecha_cancelacion(String fecha_cancelacion) {
		this.fecha_cancelacion = fecha_cancelacion;
	}
}
