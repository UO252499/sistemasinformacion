package cursos;


public class CursoEntity {
	private String id_curso;
	private String titulo; //las fechas son string (vienen de sqlite)
	private String fecha_curso;
	private String profesor;
	private String precio_colegiado;
	private String precio_precolegiado;
	private String precio_estudiante;
	private String precio_pers_autorizado;
	private String recursos;
	private String plazas;
	private String estado;
	private String fecha_inicio_inscripcion;
	private String fecha_fin_inscripcion;
	
	public CursoEntity() {}
	
	public String getId_curso() { return id_curso; }
	public void setId_curso(String id_curso) { this.id_curso = id_curso; }
	public String getTitulo() { return titulo; }
	public void setTitulo(String titulo) { this.titulo = titulo; }
	public String getPrecio_colegiado() {return precio_colegiado;}
	public void setPrecio_colegiado(String precio_colegiado) {this.precio_colegiado = precio_colegiado;}
	public String getPrecio_precolegiado() {return precio_precolegiado;}
	public void setPrecio_precolegiado(String precio_precolegiado) {this.precio_precolegiado = precio_precolegiado;}
	public String getPrecio_estudiante() {return precio_estudiante;}
	public void setPrecio_estudiante(String precio_estudiante) {this.precio_estudiante = precio_estudiante;}
	public String getPrecio_pers_autorizado() {return precio_pers_autorizado;}
	public void setPrecio_pers_autorizado(String precio_personal_autorizado) {this.precio_pers_autorizado = precio_personal_autorizado;}
	public String getPlazas() { return plazas; }
	public void setPlazas(String plazas) { this.plazas = plazas; }
	public String getRecursos() {return recursos;}
	public void setRecursos(String recursos) {this.recursos = recursos;}
	public String getEstado() { return estado; }
	public void setEstado(String estado) { this.estado = estado; }
	public String getFecha_inicio_inscripcion() {return fecha_inicio_inscripcion;}
	public void setFecha_inicio_inscripcion(String fecha_inicio_inscripcion) {this.fecha_inicio_inscripcion = fecha_inicio_inscripcion;}
	public String getFecha_fin_inscripcion() {return fecha_fin_inscripcion;}
	public void setFecha_fin_inscripcion(String fecha_fin_inscripcion) {this.fecha_fin_inscripcion = fecha_fin_inscripcion;}
	public String getFecha_curso() {return fecha_curso;}
	public void setFecha_curso(String fecha_curso) {this.fecha_curso = fecha_curso;}
	public String getProfesor() {return profesor;}
	public void setProfesor(String profesor) {this.profesor = profesor;}
}
