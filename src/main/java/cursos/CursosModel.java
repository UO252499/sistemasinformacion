package cursos;

import java.util.*;
import util.Util;
import util.ApplicationException;
import util.Database;
/**
 * Acceso a los datos de carreras e inscripciones, 
 * utilizado como modelo para el ejemplo de swing y para las pruebas unitarias y de interfaz de usuario.
 * 
 * <br/>En los metodos de este ejemplo toda la logica de negocio se realiza mediante una unica query sql por lo que siempre
 * se utilizan los metodos de utilidad en la clase Database que usan apache commons-dbutils y controlan la conexion. 
 * En caso de que en un mismo metodo se realicen diferentes queries se deberia controlar la conexion desde esta clase 
 * (ver como ejemplo la implementacion en Database).
 * 
 * <br/>Si utilizase algún otro framework para manejar la persistencia, la funcionalidad proporcionada por esta clase sería la asignada
 * a los Servicios, Repositorios y DAOs.
 */
public class CursosModel {
	private static final String MSG_FECHA_INSCRIPCION_NO_NULA = "La fecha de inscripcion no puede ser nula";
	private static final String MSG_TITULO_NO_NULO = "El titulo no puede ser nulo";
	private static final String MSG_CURSO_NO_ABIERTO = "El curso no esta abierto no admite inscripciones";
	private static final String MSG_CURSO_NO_EXISTE = "El curso no existe";
	private static final String MSG_CURSO_CANCELADO = "El curso esta cancelado";
	private Database db=new Database();
	private int id_actual;

	 
	/**
	 * Obtiene la lista de completa de cursos en forma objetos para una fecha de inscripcion dada
	 */
	public List<CursoDisplayDTO> getListaCursos() {
		String sql="SELECT id_curso, titulo,fecha_curso,estado"
				+" from curso where estado='Planificada' or estado='Abierta' ORDER BY fecha_curso ASC";
		return db.executeQueryPojo(CursoDisplayDTO.class, sql);
	}
	
	public List<CursoDisplayDTO> getListaCursosAbiertos() {
		String sql="SELECT id_curso, titulo,fecha_curso,estado"
				+" from curso where estado='Abierta' ORDER BY fecha_curso ASC";
		return db.executeQueryPojo(CursoDisplayDTO.class, sql);
	}
	public List<CursoDisplayDTO> getListaCursosCancelados() {
		String sql="SELECT id_curso, titulo,fecha_cancelacion,estado"
				+" from curso where estado='Cancelado' ORDER BY fecha_cancelacion ASC";
		return db.executeQueryPojo(CursoDisplayDTO.class, sql);
	}
	
	public List<ProfesorDisplayDTO> getListaProfesores() {
		String sql="SELECT dni,profesor.apellidos||', '||profesor.nombre as nombre"
				+" from profesor ORDER BY nombre ASC";
		return db.executeQueryPojo(ProfesorDisplayDTO.class, sql);
	}
	
	public boolean admiteInscripcion(String id_curso) {
		String sql="SELECT estado FROM curso WHERE id_curso=?";
		List<Object[]>rows=db.executeQueryArray(sql, id_curso);
		String estado = (String)rows.get(0)[0];
		if(estado.equals("Abierta")){
			return true;
		}
		else return false;
	}
	
	public boolean existeCurso(String id_curso) {
		String sql="SELECT * FROM curso WHERE id_curso=?";
		List<Object[]>curso=db.executeQueryArray(sql, id_curso);
		if(!curso.isEmpty()){
			return true;
		}
		else return false;
	}
	
	public boolean cursoNoCancelado(String id_curso) {
		String sql="SELECT estado FROM curso WHERE id_curso=?";
		List<Object[]>rows=db.executeQueryArray(sql, id_curso);
		String estado = (String)rows.get(0)[0];
		if(!estado.equals("Cancelado")){
			return true;
		}
		else return false;
	}
	
	public List<AlumnosDisplayDTO> getListaAlumnos(String id_curso) {
		validateCondition(existeCurso(id_curso), MSG_CURSO_NO_EXISTE);
		validateCondition(admiteInscripcion(id_curso), MSG_CURSO_NO_ABIERTO);
		String sql="SELECT colegiado.apellidos||', '||colegiado.nombre as nombre, inscripcion.fecha_inscripcion, inscripcion.estado, "
				+"inscripcion.precio FROM inscripcion INNER JOIN colegiado on inscripcion.id_colegiado=colegiado.numero_colegiado "
				+"WHERE inscripcion.id_curso=? union ALL SELECT externo.nombre, externo.fecha, externo.estado_inscripcion,"
				+ "externo.precio FROM externo  WHERE externo.id_curso=? " 
				+"ORDER by nombre asc ";
		return db.executeQueryPojo(AlumnosDisplayDTO.class, sql, id_curso, id_curso);
	}

	public List<AlumnosDisplayDTO> getListaAlumnosCancelados(String id_curso) {
		String sql="SELECT colegiado.apellidos||', '||colegiado.nombre as nombre, inscripcion.fecha_cancelacion, inscripcion.estado, "
				+"inscripcion.observaciones FROM inscripcion INNER JOIN colegiado on inscripcion.id_colegiado=colegiado.numero_colegiado "
				+"WHERE inscripcion.id_curso=? UNION ALL  SELECT externo.nombre, externo.fecha_cancelacion, externo.estado_inscripcion,"
				+ "externo.observaciones FROM externo  WHERE externo.id_curso=? ORDER by nombre asc ";
		return db.executeQueryPojo(AlumnosDisplayDTO.class, sql, id_curso, id_curso);
	}
	
	public List<AlumnosDisplayDTO> getListaAlumnosPagado(String id_curso) {
		String sql="SELECT colegiado.numero_colegiado, inscripcion.id_curso, colegiado.apellidos||' '||colegiado.nombre as nombre,"
				+"inscripcion.precio FROM inscripcion INNER JOIN colegiado on inscripcion.id_colegiado=colegiado.numero_colegiado "
				+"WHERE inscripcion.id_curso=? and inscripcion.estado='Pagado' UNION ALL "
				+ "SELECT externo.dni, externo.id_curso, externo.nombre,externo.precio FROM externo  WHERE externo.id_curso=? "
				+ "and externo.estado_inscripcion='Pagado' ORDER by nombre asc ";
		return db.executeQueryPojo(AlumnosDisplayDTO.class, sql, id_curso, id_curso);
	}
	
	public int getIngresosCurso(String id_curso) {
		validateCondition(cursoNoCancelado(id_curso), MSG_CURSO_CANCELADO);
		String sql="SELECT SUM(precio) FROM inscripcion WHERE id_curso=? and estado='Pagado'";
		List<Object[]>rows=db.executeQueryArray(sql, id_curso);
		String sql2="SELECT SUM(precio) FROM externo WHERE id_curso=? and estado_inscripcion='Pagado'";
		List<Object[]>rows2=db.executeQueryArray(sql2, id_curso);
		if(rows.get(0)[0]==null && rows2.get(0)[0]==null) {
			return 0;
		}

		else if(rows.get(0)[0]==null) {
			return (int)rows2.get(0)[0];
		}
		else if(rows2.get(0)[0]==null) {
			return (int)rows.get(0)[0];
		}
		else{
			int precioTotColeg = (int)rows.get(0)[0];
			int precioTotExt = (int)rows2.get(0)[0];
			return precioTotColeg+precioTotExt;
		}		
	}

	public CursoEntity getCurso(int id) {
		String sql="SELECT curso.id_curso,curso.titulo,curso.fecha_curso,"
				+ "profesor.apellidos||', '||profesor.nombre as profesor,"
				+ "curso.recursos,curso.precio_colegiado,curso.precio_precolegiado,"
				+ "curso.precio_estudiante,curso.precio_pers_autorizado,curso.estado "
				+ "from curso left join profesor on curso.profesor=profesor.dni where id_curso=?";
		List<CursoEntity> curso=db.executeQueryPojo(CursoEntity.class, sql, id);
		//validateCondition(!curso.isEmpty(),"Id de carrera no encontrado: "+id);
		return curso.get(0);
	}
	
	public boolean admiteModificacion(int id_curso) {
		String sql="SELECT estado FROM curso WHERE id_curso=?";
		List<Object[]>rows=db.executeQueryArray(sql, id_curso);
		String estado = (String)rows.get(0)[0];
		if(estado.equals("Cerrada")){
			return false;
		}
		else return true;
	}
	
	public void cancelarCurso(String id_curso) {
		String sql="UPDATE curso SET estado='Cancelado', fecha_cancelacion=current_date WHERE id_curso=?";
		db.executeUpdate(sql,id_curso);
	}

	public void nuevoCurso(String titulo, Date Fecha_Imp) {
		validateNotNull(titulo,MSG_TITULO_NO_NULO);
		validateNotNull(Fecha_Imp,MSG_FECHA_INSCRIPCION_NO_NULA);
		id_actual=this.obtenerID()+1;
		String sql="insert into curso(id_curso,titulo,fecha_curso,estado) values"+
				" (?,?,?,'Planificada')";
		String f=Util.dateToIsoString(Fecha_Imp);
		db.executeUpdate(sql, id_actual, titulo, f);
	}
	
	public void modificarInscripcion(String id_colegiado, String id_curso, String precio) {
		String msg="Deuda de "+precio+"€";
		String sql="UPDATE inscripcion SET estado='Cancelado',observaciones='"+msg
				+ "',fecha_cancelacion=current_date WHERE id_colegiado=? and id_curso=?";
		db.executeUpdate(sql,id_colegiado,id_curso);		
	}
	
	public void modificarInscripcionExt(String dni, String id_curso, String precio) {
		String msg="Deuda de "+precio+"€";
		String sql="UPDATE externo SET estado_inscripcion='Cancelado',observaciones=?"
				+ ",fecha_cancelacion=current_date WHERE dni=? and id_curso=?";
		db.executeUpdate(sql,msg,dni,id_curso);		
	}
	
	public void modificarInscripcionPreInscritos(String id_curso) {
		String sql="UPDATE inscripcion SET estado='Cancelado'"
				+ ",fecha_cancelacion=current_date WHERE id_curso=?";
		db.executeUpdate(sql,id_curso);	
	}
	
	public void modificarInscripcionPreInscritosExt(String id_curso) {
		String sql="UPDATE externo SET estado_inscripcion='Cancelado',"
				+ "fecha_cancelacion=current_date WHERE id_curso=?";
		db.executeUpdate(sql,id_curso);	
	}
	
	public void añadirInf(int id_curso,String dni,String recurso, int precioColeg, int precioPreColeg, int precioEst, int precioPersAut) {
		String sql="UPDATE curso SET profesor=?,recursos=?,precio_colegiado=?,precio_precolegiado=?,"
				+ "precio_estudiante=?,precio_pers_autorizado=? WHERE id_curso=?";
		db.executeUpdate(sql,dni,recurso,precioColeg,precioPreColeg,precioEst,precioPersAut,id_curso);
	}

	public int obtenerID() {
		String sql="SELECT MAX(id_curso) FROM curso";
		List<Object[]>rows=db.executeQueryArray(sql);
		if (rows.get(0)[0]==null) {
			return 110;
		}else {
			return (int)rows.get(0)[0];
		}
	}

		
	/* De uso general para validacion de objetos */
	private void validateNotNull(Object obj, String message) {
		if (obj==null)
			throw new ApplicationException(message);
	}
	private void validateCondition(boolean condition, String message) {
		if (!condition)
			throw new ApplicationException(message);
	}
}
