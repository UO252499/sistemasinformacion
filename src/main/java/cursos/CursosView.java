package cursos;

import java.awt.EventQueue;
import java.awt.SystemColor;

import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JScrollPane;
import javax.swing.JTable;
import javax.swing.ListSelectionModel;
import javax.swing.JButton;
import java.awt.event.ActionListener;
import java.awt.event.ActionEvent;
import java.awt.Dimension;

public class CursosView {

	private JFrame frmCursos;
	private JTable tabCurso;
	private JButton btnNuevoCurso;
	private JTable tabDetalle;
	private JButton btnAñadirInf;



	/**
	 * Launch the application.
	 */
	public static void main(String[] args) {
		EventQueue.invokeLater(new Runnable() {
			public void run() {
				try {
					CursosView window = new CursosView();
					window.frmCursos.setVisible(true);
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		});
	}

	/**
	 * Create the application.
	 */
	public CursosView() {
		initialize();
	}

	/**
	 * Initialize the contents of the frame.
	 */
	private void initialize() {
		frmCursos = new JFrame();
		frmCursos.setTitle("Cursos");
		frmCursos.setBounds(100, 100, 457, 387);
		frmCursos.setDefaultCloseOperation(JFrame.DISPOSE_ON_CLOSE);
		frmCursos.getContentPane().setLayout(null);
		frmCursos.setLocationRelativeTo(null);
		
		JLabel lbltable = new JLabel("Cursos programados:");
		lbltable.setBounds(10, 11, 132, 14);
		frmCursos.getContentPane().add(lbltable);

		tabCurso = new JTable();
		tabCurso.setName("tabCurso");
		tabCurso.setSelectionMode(ListSelectionModel.SINGLE_SELECTION);
		tabCurso.setDefaultEditor(Object.class, null); //readonly

		JScrollPane tablePanel = new JScrollPane(tabCurso);
		tablePanel.setBounds(10, 36, 421, 118);
		frmCursos.getContentPane().add(tablePanel);

		btnNuevoCurso = new JButton("Nuevo Curso");
		btnNuevoCurso.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
			}
		});
		btnNuevoCurso.setBounds(72, 308, 115, 23);
		frmCursos.getContentPane().add(btnNuevoCurso);
		
		btnAñadirInf = new JButton("Añadir Informacion");
		btnAñadirInf.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
			}
		});
		btnAñadirInf.setBounds(228, 308, 148, 23);
		frmCursos.getContentPane().add(btnAñadirInf);
		
		tabDetalle = new JTable();
		tabDetalle.setName("tabDetalle");
		tabDetalle.setRowSelectionAllowed(false);
		tabDetalle.setDefaultEditor(Object.class, null); //readonly
		tabDetalle.setBackground(SystemColor.control);
		
		JScrollPane tableDetallePanel = new JScrollPane(tabDetalle);
		tableDetallePanel.setPreferredSize(new Dimension(300, 95));
		tableDetallePanel.setMinimumSize(new Dimension(200, 95));
		tableDetallePanel.setBounds(10, 202, 421, 95);
		frmCursos.getContentPane().add(tableDetallePanel, "cell 0 10");
		
		JLabel lblDetalle = new JLabel("Detalles del curso:");
		lblDetalle.setBounds(10, 177, 132, 14);
		frmCursos.getContentPane().add(lblDetalle);
	}
	
	public void NoModificar() {
		JOptionPane.showMessageDialog(this.getFrmCursos(), "No se puede modificar un curso ya comenzado","No se puede modificar",JOptionPane.WARNING_MESSAGE);	
	}

	public JFrame getFrmCursos() { return frmCursos; }
	public void setFrmCursos(JFrame frmCursos) { this.frmCursos = frmCursos; }
	public JTable getTabCurso() { return tabCurso; }
	public void setTabCurso(JTable tabCurso) { this.tabCurso = tabCurso; }
	public JTable getTabDetalle() {return tabDetalle;}
	public void setTabDetalle(JTable tabDetalle) {this.tabDetalle = tabDetalle;}
	public JButton getBtnNuevoCurso() { return btnNuevoCurso; }
	public void setBtnNuevoCurso(JButton btnNuevoCurso) { this.btnNuevoCurso = btnNuevoCurso; }
	public JButton getBtnAñadirInf() {return btnAñadirInf;}
	public void setBtnAñadirInf(JButton btnAñadirInf) {this.btnAñadirInf = btnAñadirInf;}
}
