package cursos;


import java.util.Date;
import util.SwingUtil;
import util.Util;

/**
 * Controlador para la funcionalidad de visualizacion de carreras para la inscripcion.
 * Es el punto de entrada de esta pantalla que se invocará:
 * -instanciando el controlador con la vista y el modelo
 * -ejecutando initController que instalara los manejadores de eventos
 */
public class NuevoCursoController {
	private CursosModel model;
	private NuevoCursoView view;

	public NuevoCursoController(CursosModel m, NuevoCursoView v) {
		this.model = m;
		this.view = v;
		//no hay inicializacion especifica del modelo, solo de la vista
		this.initView();
	}
	/**
	 * Inicializacion del controlador: anyade los manejadores de eventos a los objetos del UI.
	 * Cada manejador de eventos se instancia de la misma forma, para que invoque un metodo privado
	 * de este controlador, encerrado en un manejador de excepciones generico para mostrar ventanas
	 * emergentes cuando ocurra algun problema o excepcion controlada.
	 */
	public void initController() {
		//ActionListener define solo un metodo actionPerformed(), es un interfaz funcional que se puede invocar de la siguiente forma:
		//view.getBtnTablaCarreras().addActionListener(e -> getListaCarreras());
		//ademas invoco el metodo que responde al listener en el exceptionWrapper para que se encargue de las excepciones
		view.getBtnAceptar().addActionListener(e -> SwingUtil.exceptionWrapper(() -> GuardarCurso()));
		view.getBtnCancelar().addActionListener(e -> SwingUtil.exceptionWrapper(() -> CancelarCurso()));

		
	}
	
	public void initView() {
		
		//Abre la ventana (sustituye al main generado por WindowBuilder)
		view.getFrame().setVisible(true); 
	}
	/**
	 * La obtencion de la lista de carreras solo necesita obtener la lista de objetos del modelo 
	 * y usar metodo de SwingUtil para crear un tablemodel que se asigna finalmente a la tabla.
	 */
	public void GuardarCurso() {
		String titulo=view.getTextFieldTitulo().getText();
		String fecha=view.getTextFieldFecha().getText();
		
		if(ValidarCampos(titulo, fecha)&&ComprobarFecha(fecha)){
			model.nuevoCurso(titulo,Util.isoStringToDate(fecha));
			CursoController controller= new CursoController(new CursosModel(), new CursosView());
			controller.initController();
			view.getFrame().setVisible(false);
		}
	}
	
	public boolean ComprobarFecha(String fecha) {
		Date fechaImp=Util.isoStringToDate(fecha);
		if(fechaImp.before(new Date())) {
			view.FechaNoValida();
			return false;
		}
		else return true;

	}
	
	public void CancelarCurso() {
		CursoController controller= new CursoController(new CursosModel(), new CursosView());
		controller.initController();
		view.getFrame().setVisible(false);
	}
	
	public boolean ValidarCampos(String titulo, String fecha) {
		if (titulo.isEmpty()) {
			view.NoHayTitulo();
			return false;
			}
		else if (fecha.isEmpty()){
			view.NoHayFecha();
			return false;
		}
		
		else return true;
	}
}
