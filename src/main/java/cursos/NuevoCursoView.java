package cursos;

import java.awt.EventQueue;

import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JTextField;
import javax.swing.JButton;
import java.awt.event.ActionListener;
import java.awt.event.ActionEvent;

public class NuevoCursoView {

	private JFrame frmFormularioNuevosCursos;
	private JTextField textFieldTitulo;
	private JButton btnAceptar;
	private JButton btnCancelar;
	private JTextField textFieldFecha;
	

	/**
	 * Launch the application.
	 */
	public static void main(String[] args) {
		EventQueue.invokeLater(new Runnable() {
			public void run() {
				try {
					NuevoCursoView window = new NuevoCursoView();
					window.frmFormularioNuevosCursos.setVisible(true);
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		});
	}

	/**
	 * Create the application.
	 */
	public NuevoCursoView() {
		initialize();
	}

	/**
	 * Initialize the contents of the frame.
	 */
	private void initialize() {
		frmFormularioNuevosCursos = new JFrame();
		frmFormularioNuevosCursos.setTitle("Planificar nuevo curso");
		frmFormularioNuevosCursos.setBounds(100, 100, 451, 154);
		frmFormularioNuevosCursos.setDefaultCloseOperation(JFrame.DISPOSE_ON_CLOSE);
		frmFormularioNuevosCursos.getContentPane().setLayout(null);
		
		JLabel lblTitulo = new JLabel("Título del curso:");
		lblTitulo.setBounds(10, 10, 96, 14);
		frmFormularioNuevosCursos.getContentPane().add(lblTitulo);
		
		textFieldTitulo = new JTextField();
		textFieldTitulo.setBounds(112, 7, 216, 20);
		frmFormularioNuevosCursos.getContentPane().add(textFieldTitulo);
		textFieldTitulo.setColumns(10);
		
		JLabel lblFecha = new JLabel("Fecha de imparticion (yyyy-MM-dd) :");
		lblFecha.setBounds(10, 41, 206, 14);
		frmFormularioNuevosCursos.getContentPane().add(lblFecha);
		
		btnAceptar = new JButton("Aceptar");
		btnAceptar.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
			}
		});
		btnAceptar.setBounds(112, 79, 96, 23);
		frmFormularioNuevosCursos.getContentPane().add(btnAceptar);
		
		btnCancelar = new JButton("Cancelar");
		btnCancelar.setBounds(249, 79, 89, 23);
		frmFormularioNuevosCursos.getContentPane().add(btnCancelar);
		
		textFieldFecha = new JTextField();
		textFieldFecha.setColumns(10);
		textFieldFecha.setBounds(226, 38, 138, 20);
		frmFormularioNuevosCursos.getContentPane().add(textFieldFecha);
	}
	
	public JTextField getTextFieldTitulo() { return textFieldTitulo; }
	public void setTextFieldTitulo(JTextField textFieldTitulo) { this.textFieldTitulo = textFieldTitulo; }
	public JFrame getFrame() { return this.frmFormularioNuevosCursos; }
	public JButton getBtnAceptar() { return btnAceptar;	}
	public JButton getBtnCancelar() { return btnCancelar;	}
	public JTextField getTextFieldFecha() {	return textFieldFecha;}
	public void setTextFieldFecha(JTextField textFieldFecha) {this.textFieldFecha = textFieldFecha;}
	
	public void NoHayTitulo() {
		JOptionPane.showMessageDialog(this.getFrame(), "Introduzca un titulo para el curso","No puede haber campos vacios",JOptionPane.WARNING_MESSAGE);
		
	}
	
	public void NoHayFecha() {
		JOptionPane.showMessageDialog(this.getFrame(), "Introduzca una fecha para el curso","No puede haber campos vacios",JOptionPane.WARNING_MESSAGE);
		
	}
	
	public void FechaNoValida() {
		JOptionPane.showMessageDialog(this.getFrame(), "La fecha debe ser posterior a la actual");		
		
	}
	
	public void NoHayPrecio() {
		JOptionPane.showMessageDialog(this.getFrame(), "Introduzca un precio para el curso","No puede haber campos vacios",JOptionPane.WARNING_MESSAGE);
		
	}

}
