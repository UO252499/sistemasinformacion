package cursos;
/**
 * Cada una de las filas que muestran al usuario las carreras y su estado
 */

public class ProfesorDisplayDTO {
	
	private String dni;
	private String nombre;
	private String poblacion;
	private String telefono;
	
	public ProfesorDisplayDTO() {}
	public ProfesorDisplayDTO(String dni, String nombre, String poblacion, String telefono) {
		this.dni=dni;
		this.nombre=nombre;
		this.poblacion=poblacion;
		this.telefono=telefono;
	}
	
	
	public String getDni() {return dni;}
	public void setDni(String dni) {this.dni = dni;}
	public String getNombre() {return nombre;}
	public void setNombre(String nombre) {this.nombre = nombre;}
	public String getPoblacion() {return poblacion;}
	public void setPoblacion(String poblacion) {this.poblacion = poblacion;}
	public String getTelefono() {return telefono;}
	public void setTelefono(String telefono) {this.telefono = telefono;}
	
	
	
	
}
