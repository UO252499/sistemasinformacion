package inscripcion;

public class Externo extends Persona{
	int id_curso;
	String fecha;
	String estado_inscripcion;
	int precio;
	
	public Externo() {
		super();
	}

	public Externo(int id_curso, String dni, String nombre, String fecha, String estado, String estado_inscripcion,
			int precio) {
		super(nombre, dni, estado);
		this.id_curso = id_curso;
		this.fecha = fecha;
		this.estado_inscripcion = estado_inscripcion;
		this.precio = precio;
	}

	public int getId_curso() {
		return id_curso;
	}

	public void setId_curso(int id_curso) {
		this.id_curso = id_curso;
	}
	public String getFecha() {
		return fecha;
	}

	public void setFecha(String fecha) {
		this.fecha = fecha;
	}
	public String getEstado_inscripcion() {
		return estado_inscripcion;
	}

	public void setEstado_inscripcion(String estado_inscripcion) {
		this.estado_inscripcion = estado_inscripcion;
	}

	public int getPrecio() {
		return precio;
	}

	public void setPrecio(int precio) {
		this.precio = precio;
	}
}
