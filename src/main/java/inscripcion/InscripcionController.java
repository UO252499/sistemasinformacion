package inscripcion;

import java.util.List;

import javax.swing.table.TableModel;

import cursos.CursoDisplayDTO;
import util.SwingUtil;

public class InscripcionController {
	
	InscripcionView vista;
	InscripcionModel modelo;
	public InscripcionController(InscripcionModel inscripcionModel, InscripcionView inscripcionView) {
		vista = inscripcionView;
		modelo = inscripcionModel;
		vista.controlador=this;
	}

	public void initController() {
		actualizarTabla();
		
	}

	public void actualizarTabla() {
		List<CursoDisplayDTO> cursos=modelo.getListaCursos();
		formatearTabla(cursos);
	}
	
	public void actualizarTablaAñoActual() {
		List<CursoDisplayDTO> cursos=modelo.getCursosEsteAño();
		formatearTabla(cursos);
	}

	private void formatearTabla(List<CursoDisplayDTO> cursos) {
		for (int i = 0; i < cursos.size(); i++) {
		    if ("0".equals(cursos.get(i).getPlazas_disponibles())) {
		    	cursos.remove(i);
		         i--;
		    }     
		}
		TableModel tmodel=SwingUtil.getTableModelFromPojos(cursos, new String[] {"id_curso", "titulo", "fecha_inicio_inscripcion", "fecha_fin_inscripcion","fecha_curso", "precio_colegiado","precio_precolegiado", "precio_estudiante", "precio_pers_autorizado", "plazas_disponibles", "plazas", "estado"});
		vista.table.setModel(tmodel);
		vista.table.getTableHeader().setReorderingAllowed(false) ;
		vista.table.getColumnModel().getColumn(0).setMaxWidth(0);
		vista.table.getColumnModel().getColumn(0).setMinWidth(0);
		vista.table.getColumnModel().getColumn(0).setPreferredWidth(0);
		vista.table.getColumnModel().getColumn(2).setMaxWidth(0);
		vista.table.getColumnModel().getColumn(2).setMinWidth(0);
		vista.table.getColumnModel().getColumn(2).setPreferredWidth(0);
		vista.table.getColumnModel().getColumn(3).setMaxWidth(0);
		vista.table.getColumnModel().getColumn(3).setMinWidth(0);
		vista.table.getColumnModel().getColumn(3).setPreferredWidth(0);
		vista.table.getColumnModel().getColumn(1).setHeaderValue("titulo");
		vista.table.getColumnModel().getColumn(4).setHeaderValue("inicio curso");
		vista.table.getColumnModel().getColumn(5).setHeaderValue("precio colegiado");
		vista.table.getColumnModel().getColumn(6).setHeaderValue("precio precolegiado");
		vista.table.getColumnModel().getColumn(9).setHeaderValue("plazas disponibles");
	}
	
	/*
	 * Este metodo inscribe directamente a una persona en un curso deberia ir despues del pago. Ahora mismo al seleccionar en inscribir no hace nada.
	 */
	public void inscribir(int selectedRow) {
		InscripcionCursoController IC = new InscripcionCursoController(new InscripcionModel(), new InscripcionCursoView(), 
				String.valueOf(vista.table.getValueAt(selectedRow, 1)),Integer.parseInt(String.valueOf(vista.table.getValueAt(selectedRow, 0))));
		IC.initController();
		vista.setVisible(false);
	}
}
