package inscripcion;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.KeyAdapter;
import java.awt.event.KeyEvent;
import javax.swing.JOptionPane;

public class InscripcionCursoController {
	private InscripcionModel model;
	private InscripcionCursoView view;
	private int id_curso;
	
	private String NSocio;
	
	private String DNI;
	private String nombre;
	
	public InscripcionCursoController(InscripcionModel m, InscripcionCursoView v, String curso,int id) {
		this.model = m;
		this.view = v;
		//no hay inicializacion especifica del modelo, solo de la vista
		this.initView();
		view.getLCurso().setText("Curso: "+curso);
		view.getLCurso2().setText("Curso: "+curso);
		id_curso=id;
	}
	
	public void initView() {
		view.getFrame().setLocationRelativeTo(null);
		view.getFrame().setVisible(true);
	}
	
	public void initController() {
		//***********************************************************Socio****************************************************//
		view.getRBColegiado().addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				view.HabilitarBotonSocio();
			}
		});
		view.getRBPrecolegiado().addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				view.HabilitarBotonSocio();
			}
		});
		
		
		view.getTFNSocio().addKeyListener(new KeyAdapter() {
			public void keyTyped(KeyEvent evt) {
		        if(view.getTFNSocio().getText().length()>=11) {
					evt.consume();
				}
		        view.HabilitarBotonSocio();
			}
		});
		
		view.getBCancelarSocio().addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent evt) {
				view.getFrame().setVisible(false);
				InscripcionController controller= new InscripcionController(new InscripcionModel(), new InscripcionView());
				controller.initController();
			}
		});
		
		view.getBAceptarSocio().addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent evt) {
				NSocio=view.getTFNSocio().getText();
				if(!model.socioInscrito(NSocio,id_curso)) {
					if(view.getRBColegiado().isSelected()) {
						if(model.existeColegiado(NSocio)) {
							new PagoCursosControler(id_curso, NSocio, null, "colegiado");
						}else {
							JOptionPane.showMessageDialog(null,"El número de colegiado "+NSocio+" no existe");
							view.getTFNSocio().setText(null);
							view.getBAceptarSocio().setEnabled(false);
						}
					}else if(view.getRBPrecolegiado().isSelected()){
						if(model.existePreColegiado(NSocio)) {
							new PagoCursosControler(id_curso, NSocio, null, "precolegiado");
						}else {
							JOptionPane.showMessageDialog(null,"El número de precolegiado "+NSocio+" no existe");
							view.getTFNSocio().setText(null);
							view.getBAceptarSocio().setEnabled(false);
						}
					}
				}else {
					JOptionPane.showMessageDialog(null,"El socio con número "+NSocio+" ya esta inscrito en "+view.getLCurso().getText());
					view.getTFNSocio().setText(null);
					view.getBAceptarSocio().setEnabled(false);
				}
			}
		});
		
//*********************************************************************Externo*********************************************************//
		view.getRBEstudiante().addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				view.HabilitarBotonExterno();
			}
		});
		view.getRBExterno().addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				view.HabilitarBotonExterno();
			}
		});
		
		
		view.getBAceptarExterno().addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent evt) {
				if(validarDNI()) {
					DNI=view.getDNI().getText();
					nombre=view.getTFNombre().getText();
					if(!model.externoInscrito(DNI,id_curso)) {
						if(!model.existeDNI(DNI)) {
							if(view.getRBEstudiante().isSelected()) {
								new PagoCursosControler(id_curso, DNI, nombre, "estudiante");
							}else if(view.getRBExterno().isSelected()){
								new PagoCursosControler(id_curso, DNI, nombre, "externo");
							}
						}else {
							JOptionPane.showMessageDialog(null,"El DNI "+DNI+" ya se encuentra en nuestra base de datos");
							view.getDNI().setText(null);
							view.getTFNombre().setText(null);
							view.getBAceptarExterno().setEnabled(false);
						}
					}else {
						JOptionPane.showMessageDialog(null,"El externo con DNI "+DNI+" ya esta inscrito en "+view.getLCurso2().getText());
						view.getDNI().setText(null);
						view.getTFNombre().setText(null);
						view.getBAceptarExterno().setEnabled(false);
					}
				}else {
					JOptionPane.showMessageDialog(null,"DNI mal introducido");
					view.getDNI().setText(null);
					view.getBAceptarExterno().setEnabled(false);
				}
			}
		});
		
		view.getBCancelarExterno().addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent evt) {
				view.getFrame().setVisible(false);
				InscripcionController controller= new InscripcionController(new InscripcionModel(), new InscripcionView());
				controller.initController();
			}
		});
		
		view.getDNI().addKeyListener(new KeyAdapter() {
			public void keyTyped(KeyEvent evt) {
		        if(view.getDNI().getText().length()>=9) {
					evt.consume();
				}
		        view.HabilitarBotonExterno();
			}
		});
		
		
		view.getTFNombre().addKeyListener(new KeyAdapter() {
			public void keyTyped(KeyEvent evt) {
				char c=evt.getKeyChar();
		        if (Character.isDigit(c)){
		            evt.consume();
		            JOptionPane.showMessageDialog(null, "Ingresar solo letras","Valor mal introducido",JOptionPane.WARNING_MESSAGE);
		        }
		        view.HabilitarBotonExterno();
			}
		});
	}

	
//****************************************************************Otros métodos*************************************************************//
	public boolean validarDNI() {
		
		String letraMayuscula = ""; //Guardaremos la letra introducida en formato mayúscula
		
		// Aquí excluimos cadenas distintas a 9 caracteres que debe tener un dni y también si el último caracter no es una letra
		if((view.getDNI().getText().length() != 9) || (Character.isLetter(view.getDNI().getText().charAt(8)) == false)) {
			return false;
		}


		// Al superar la primera restricción, la letra la pasamos a mayúscula
		letraMayuscula = (view.getDNI().getText().substring(8)).toUpperCase();
		
		// Por último validamos que sólo tengo 8 dígitos entre los 8 primeros caracteres y que la letra introducida es igual a la de la ecuación
		// Llamamos a los métodos privados de la clase soloNumeros() y letraDNI()
		if((soloNumeros() == true) && (letraDNI().equals(letraMayuscula))) {
			return true;
		}
		else {
			return false;
		}
	}
	
	private boolean soloNumeros() {

		int i, j = 0;
		String numero = ""; // Es el número que se comprueba uno a uno por si hay alguna letra entre los 8 primeros dígitos
		String miDNI = ""; // Guardamos en una cadena los números para después calcular la letra
		String[] unoNueve = {"0","1","2","3","4","5","6","7","8","9"};

		for(i = 0; i < view.getDNI().getText().length() - 1; i++) {
			numero = view.getDNI().getText().substring(i, i+1);

			for(j = 0; j < unoNueve.length; j++) {
				if(numero.equals(unoNueve[j])) {
					miDNI += unoNueve[j];
				}
			}
		}

		if(miDNI.length() != 8) {
			return false;
		}
		else {
			return true;
		}
	}
	
	private String letraDNI() {
		// El método es privado porque lo voy a usar internamente en esta clase, no se necesita fuera de ella

		// pasar miNumero a integer
		int miDNI = Integer.parseInt(view.getDNI().getText().substring(0,8));
		int resto = 0;
		String miLetra = "";
		String[] asignacionLetra = {"T", "R", "W", "A", "G", "M", "Y", "F", "P", "D", "X", "B", "N", "J", "Z", "S", "Q", "V", "H", "L", "C", "K", "E"};

		resto = miDNI % 23;

		miLetra = asignacionLetra[resto];

		return miLetra;
	}
}
