package inscripcion;

import javax.swing.JFrame;
import javax.swing.JLabel;
import java.awt.Font;
import javax.swing.SwingConstants;
import javax.swing.JSeparator;
import javax.swing.JTabbedPane;
import javax.swing.JPanel;
import javax.swing.JRadioButton;
import javax.swing.ButtonGroup;
import javax.swing.JTextField;
import javax.swing.JButton;
import java.awt.event.ActionListener;
import java.awt.event.ActionEvent;

public class InscripcionCursoView {
	private JFrame frame;
	private JTabbedPane TPPestañas;
	private JPanel PSocio;
	private JLabel LInscribirmeEnCurso1;
	private JSeparator separator1;
	private JLabel LInscribirmeComo1;
	private ButtonGroup BGSocio;
	private JRadioButton RBColegiado;
	private JRadioButton RBPrecolegiado;
	private JLabel LNumeroDeSocio;
	private JTextField TFSocio;
	private JButton BAceptarSocio;
	private JButton BCancelar1;
	private JLabel LCurso1;

	
	private JPanel PExterno;
	private JLabel LInscribirmeEnCurso2;
	private JSeparator separator2;
	private JLabel LInscribirmeComo2;
	private ButtonGroup BGExternos;
	private JRadioButton RBEstudiante;
	private JRadioButton RBExterno;
	
	private JButton BAceptarExterno;
	private JButton BCancelar2;
	private JLabel LCurso2;
	private JLabel LDNI;
	private JTextField TFDNI;
	private JLabel LNombre;
	private JTextField TFNombre;
	
	public InscripcionCursoView() {
		initialize();
	}

	/**
	 * Initialize the contents of the frame.
	 */
	private void initialize() {
		frame = new JFrame();
		frame.setTitle("Inscripcion en Curso");
		frame.setName("Inscripcion");
		frame.setBounds(0, 0, 461, 348);
		frame.setDefaultCloseOperation(JFrame.DISPOSE_ON_CLOSE);
		frame.getContentPane().setLayout(null);
		frame.setLocationRelativeTo(null);
		
		TPPestañas = new JTabbedPane();
		TPPestañas.setBounds(0, 0, 582, 403);
		frame.getContentPane().add(TPPestañas);
		
//*******************************************************Socio******************************************************//
		PSocio = new JPanel();
		TPPestañas.addTab("Socio", null, PSocio, null);
		PSocio.setLayout(null);
		
		LInscribirmeEnCurso1 = new JLabel("Inscribirme en Curso");
		LInscribirmeEnCurso1.setHorizontalAlignment(SwingConstants.CENTER);
		LInscribirmeEnCurso1.setFont(new Font("Verdana", Font.BOLD, 18));
		LInscribirmeEnCurso1.setBounds(0,0,440,31);
		PSocio.add(LInscribirmeEnCurso1);
		
		separator1 = new JSeparator();
		separator1.setBounds(0, 31, 440, 2);
		PSocio.add(separator1);
		
		LCurso1 = new JLabel("");
		LCurso1.setFont(new Font("Verdana", Font.BOLD, 16));
		LCurso1.setHorizontalAlignment(SwingConstants.CENTER);
		LCurso1.setBounds(0, 44, 440, 31);
		PSocio.add(LCurso1);
		
		LInscribirmeComo1 = new JLabel("Inscribirme como:");
		LInscribirmeComo1.setBounds(53, 103, 116, 16);
		PSocio.add(LInscribirmeComo1);
		
		RBColegiado = new JRadioButton("Colegiado");
		RBColegiado.setBounds(177, 99, 113, 25);
		PSocio.add(RBColegiado);
		RBColegiado.setSelected(true);
		
		RBPrecolegiado = new JRadioButton("PreColegiado");
		RBPrecolegiado.setBounds(294, 99, 113, 25);
		PSocio.add(RBPrecolegiado);
		
		LNumeroDeSocio = new JLabel("Número de socio:");
		LNumeroDeSocio.setBounds(78, 157, 113, 16);
		PSocio.add(LNumeroDeSocio);
		
		TFSocio = new JTextField();
		TFSocio.setBounds(239, 154, 116, 22);
		PSocio.add(TFSocio);
		TFSocio.setColumns(10);
		
		BAceptarSocio = new JButton("Aceptar");
		BAceptarSocio.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
			}
		});
		BAceptarSocio.setBounds(88, 215, 97, 25);
		PSocio.add(BAceptarSocio);
		BAceptarSocio.setEnabled(false);
		
		BCancelar1 = new JButton("Cancelar");
		BCancelar1.setBounds(239, 215, 97, 25);
		PSocio.add(BCancelar1);
		
		BGSocio= new ButtonGroup();
		BGSocio.add(RBColegiado);
		BGSocio.add(RBPrecolegiado);
		
//**************************************************Externo******************************************************//
		PExterno = new JPanel();
		TPPestañas.addTab("Externo", null, PExterno, null);
		PExterno.setLayout(null);
		
		LInscribirmeEnCurso2 = new JLabel("Inscribirme en Curso");
		LInscribirmeEnCurso2.setHorizontalAlignment(SwingConstants.CENTER);
		LInscribirmeEnCurso2.setFont(new Font("Verdana", Font.BOLD, 18));
		LInscribirmeEnCurso2.setBounds(0,0,440,31);
		PExterno.add(LInscribirmeEnCurso2);
		
		separator2 = new JSeparator();
		separator2.setBounds(0, 31, 440, 2);
		PExterno.add(separator2);
		
		LCurso2 = new JLabel("");
		LCurso2.setFont(new Font("Verdana", Font.BOLD, 16));
		LCurso2.setHorizontalAlignment(SwingConstants.CENTER);
		LCurso2.setBounds(0, 44, 440, 31);
		PExterno.add(LCurso2);
		
		LInscribirmeComo2 = new JLabel("Inscribirme como:");
		LInscribirmeComo2.setBounds(53, 103, 116, 16);
		PExterno.add(LInscribirmeComo2);
		
		RBEstudiante = new JRadioButton("Estudiante");
		RBEstudiante.setBounds(177, 99, 113, 25);
		PExterno.add(RBEstudiante);
		RBEstudiante.setSelected(true);
		
		RBExterno = new JRadioButton("Externo");
		RBExterno.setBounds(294, 99, 113, 25);
		PExterno.add(RBExterno);
		
		BAceptarExterno = new JButton("Aceptar");
		BAceptarExterno.setBounds(72, 231, 97, 25);
		PExterno.add(BAceptarExterno);
		BAceptarExterno.setEnabled(false);
		
		BCancelar2 = new JButton("Cancelar");
		BCancelar2.setBounds(232, 231, 97, 25);
		PExterno.add(BCancelar2);
		
		LDNI = new JLabel("DNI");
		LDNI.setBounds(43, 168, 26, 16);
		PExterno.add(LDNI);
		
		TFDNI = new JTextField();
		TFDNI.setBounds(93, 165, 89, 22);
		PExterno.add(TFDNI);
		TFDNI.setColumns(10);
		
		LNombre = new JLabel("Nombre");
		LNombre.setBounds(216, 168, 56, 16);
		PExterno.add(LNombre);
		
		TFNombre = new JTextField();
		TFNombre.setBounds(291, 165, 116, 22);
		PExterno.add(TFNombre);
		TFNombre.setColumns(10);
		
		BGExternos= new ButtonGroup();
		BGExternos.add(RBEstudiante);
		BGExternos.add(RBExterno);
	}
	
	
//*************************************************Métodos************************************************************//
	public JFrame getFrame() { return this.frame; }
	
	public JLabel getLCurso() { return this.LCurso1; }
	public JRadioButton getRBColegiado() { return this.RBColegiado; }
	public JRadioButton getRBPrecolegiado() { return this.RBPrecolegiado; }
	public JTextField getTFNSocio() { return this.TFSocio; }
	public JButton getBAceptarSocio() { return this.BAceptarSocio; }
	public JButton getBCancelarSocio() { return this.BCancelar1; }
	
	public JLabel getLCurso2() { return this.LCurso2; }
	public JRadioButton getRBEstudiante() { return this.RBEstudiante; }
	public JRadioButton getRBExterno() { return this.RBExterno; }
	public JTextField getDNI() { return this.TFDNI; }
	public JTextField getTFNombre() { return this.TFNombre; }
	public JButton getBAceptarExterno() { return this.BAceptarExterno; }
	public JButton getBCancelarExterno() { return this.BCancelar2; }
	
	
	public void HabilitarBotonSocio() {
		if(!TFSocio.getText().isEmpty()) {
			BAceptarSocio.setEnabled(true);
		}else {
			BAceptarSocio.setEnabled(false);
		}
	}
	public void HabilitarBotonExterno() {
		if(!(TFDNI.getText().isEmpty()) && !(TFNombre.getText().isEmpty())) {
			BAceptarExterno.setEnabled(true);
		}else {
			BAceptarExterno.setEnabled(false);
		}
	}
}
