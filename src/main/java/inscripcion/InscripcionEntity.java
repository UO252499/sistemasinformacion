package inscripcion;

public class InscripcionEntity {
	private int id_colegiado;
	private int id_curso;
	private int precio;
	private String fecha_inscripcion;
	private String estado;
	
	public int getId_colegiado() {
		return id_colegiado;
	}
	public void setId_colegiado(int id_colegiado) {
		this.id_colegiado = id_colegiado;
	}
	public int getId_curso() {
		return id_curso;
	}
	public void setId_curso(int id_curso) {
		this.id_curso = id_curso;
	}
	public int getPrecio() {
		return precio;
	}
	public void setPrecio(int precio) {
		this.precio = precio;
	}
	public String getFecha_inscripcion() {
		return fecha_inscripcion;
	}
	public void setFecha_inscripcion(String fecha_inscripcion) {
		this.fecha_inscripcion = fecha_inscripcion;
	}
	public String getEstado() {
		return estado;
	}
	public void setEstado(String estado) {
		this.estado = estado;
	}	
}
