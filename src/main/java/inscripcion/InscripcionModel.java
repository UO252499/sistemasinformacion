package inscripcion;

import java.util.List;

import cursos.AlumnosDisplayDTO;
import Colegiado.ColegiadoAltaEntity;
import cursos.CursoDisplayDTO;
import cursos.CursoEntity;
import util.Database;

public class InscripcionModel {
	
	private Database db=new Database();

	/**
	 * @return
	 */
	public List<CursoDisplayDTO> getListaCursos() {
		String sql="SELECT curso.id_curso, curso.titulo, curso.fecha_inicio_inscripcion, curso.fecha_fin_inscripcion, "
				+ "curso.fecha_curso, curso.precio_colegiado, curso.precio_precolegiado, curso.precio_estudiante, curso.precio_pers_autorizado, curso.plazas-COUNT(inscripcion.id_curso)-COUNT(externo.id_curso) as plazas_disponibles,"
				+ "curso.plazas, curso.estado FROM curso LEFT JOIN externo on curso.id_curso=externo.id_curso LEFT JOIN inscripcion ON curso.id_curso=inscripcion.id_curso where curso.estado='Abierta' "
				+ "OR curso.estado='Planificada' GROUP BY curso.id_curso ORDER BY curso.fecha_curso";
		return db.executeQueryPojo(CursoDisplayDTO.class, sql);
	}
	
	public List<CursoDisplayDTO> getCursosEsteAño() {
		String sql="SELECT curso.id_curso, curso.titulo, curso.fecha_inicio_inscripcion, curso.fecha_fin_inscripcion, \r\n" + 
				"curso.fecha_curso, curso.precio_colegiado, curso.precio_precolegiado, curso.precio_estudiante, curso.precio_pers_autorizado, curso.plazas-COUNT(inscripcion.id_curso)-COUNT(externo.id_curso) as plazas_disponibles,\r\n" + 
				"curso.plazas, curso.estado FROM curso LEFT JOIN externo on curso.id_curso=externo.id_curso LEFT JOIN inscripcion ON curso.id_curso=inscripcion.id_curso where (curso.estado='Abierta' \r\n" + 
				"OR curso.estado='Planificada') and strftime('%Y',curso.fecha_curso)=strftime('%Y',CURRENT_DATE) GROUP BY curso.id_curso ORDER BY curso.fecha_curso";
		return db.executeQueryPojo(CursoDisplayDTO.class, sql);
		
	}
	
	public int getIDcurso(String titulo) {
		String sql="SELECT id_curso FROM curso WHERE titulo=?";
		List<Object[]>rows=db.executeQueryArray(sql, titulo);
		return (int)rows.get(0)[0];
	}
	
	public boolean admiteInscripcion(int id_curso) {
		String sql="SELECT estado FROM curso WHERE id_curso=?";
		List<Object[]>rows=db.executeQueryArray(sql, id_curso);
		String estado = (String)rows.get(0)[0];
		if(estado.equals("Abierta")){
			return true;
		}
		else return false;
	}
	
	public List<AlumnosDisplayDTO> getListaAlumnos(int id_curso) {
		String sql="SELECT colegiado.apellidos||', '||colegiado.nombre as nombre, inscripcion.fecha_inscripcion, inscripcion.estado, "
				+"inscripcion.precio FROM inscripcion INNER JOIN colegiado on inscripcion.id_colegiado=colegiado.numero_colegiado "
				+"WHERE inscripcion.id_curso=? "
				+"ORDER by nombre asc ";
		return db.executeQueryPojo(AlumnosDisplayDTO.class, sql, id_curso);
	}
	
	public int getIngresosCurso(int id_curso) {
		String sql="SELECT SUM(precio) FROM inscripcion WHERE id_curso=?";
		List<Object[]>rows=db.executeQueryArray(sql, id_curso);
		return (int)rows.get(0)[0];
	}
	
	public String TipoColegiado(String colegiado) {
		String sql="SELECT estado FROM colegiado WHERE numero_colegiado=?";
		List<Object[]>rows=db.executeQueryArray(sql, colegiado);
		String estado = (String)rows.get(0)[0];
		return estado;	
	}

	public CursoEntity obtenCurso(int cursoNumero) {
		String sql="select * from curso WHERE id_curso=" + cursoNumero;
		List<CursoEntity> cursoData=db.executeQueryPojo(CursoEntity.class, sql);
		if (!cursoData.isEmpty())
			return cursoData.get(0);
		return null;
	}

	public List<CursoDisplayDTO> getListaCursosTot() {
		String sql="SELECT * from curso order by fecha_curso;";
		return db.executeQueryPojo(CursoDisplayDTO.class, sql);
	}

	public void modificarCurso(CursoEntity cursoModificado) {
		db.executeUpdate("update curso SET fecha_inicio_inscripcion='"+cursoModificado.getFecha_inicio_inscripcion()+
				"', fecha_fin_inscripcion='"+cursoModificado.getFecha_fin_inscripcion()+
				"', plazas="+Integer.parseInt(cursoModificado.getPlazas())+
				", estado='"+cursoModificado.getEstado()+ "' where id_curso="+cursoModificado.getId_curso());
	}

	public InscripcionEntity ObtenInscripcion(int cursoNumero, String colegiadoNumero) {
		String sql="select * from inscripcion WHERE id_curso=" + cursoNumero + " and id_colegiado=?";
		List<InscripcionEntity> inscripcionData=db.executeQueryPojo(InscripcionEntity.class, sql,colegiadoNumero);
		if (!inscripcionData.isEmpty())
			return inscripcionData.get(0);
		return null;
	}
	
	public boolean existeColegiado(String nc) {
		String sql= "SELECT MAX(numero_colegiado) FROM colegiado WHERE numero_colegiado=? AND estado = 'Colegiado'";
		List<Object[]>rows=db.executeQueryArray(sql,nc);
		if (rows.get(0)[0]==null) {
			return false;
		}
		else {
			return true;
		}
	}
	
	public boolean existePreColegiado(String nc) {
		String sql= "SELECT MAX(numero_colegiado) FROM colegiado WHERE numero_colegiado=? AND estado = 'PreColegiado'";
		List<Object[]>rows=db.executeQueryArray(sql,nc);
		if (rows.get(0)[0]==null) {
			return false;
		}
		else {
			return true;
		}
	}
	
	public boolean socioInscrito(String nSocio, int id_curso) {
		String sql="select * from inscripcion WHERE id_curso=? and id_colegiado=?";
		List<InscripcionEntity> inscripcionData=db.executeQueryPojo(InscripcionEntity.class, sql,  id_curso, nSocio);
		if (inscripcionData.isEmpty())
			return false;
		return true;
	}
//**********************************************************************************************Externo*****************************************************************//
	
	public boolean externoInscrito(String DNI, int id_curso) {
		String sql= "SELECT * FROM externo WHERE dni=? AND id_curso=?";
		List<Externo> rows = db.executeQueryPojo(Externo.class, sql, DNI, id_curso);
		if (rows.isEmpty()) {
			return false;
		}
		else {
			return true;
		}
	}
	
	public int obtenerId() {
		String sql="SELECT MAX(id) FROM externo";
		List<Object[]>rows=db.executeQueryArray(sql);
		if(rows.get(0)[0]==null) {
			return 1;
		}else {
			return (Integer)rows.get(0)[0]+1;
		}
	}

	public boolean existeDNI(String DNI) {
		String sql= "SELECT MAX(dni) FROM colegiado WHERE dni=?";
		List<Object[]>rows=db.executeQueryArray(sql,DNI);
		if (rows.get(0)[0]==null) {
			return false;
		}
		else {
			return true;
		}
	}
	
	public Externo getMiembroDNI(String dNI) {
		String sql="select * from externo WHERE dni=?";
		List<Externo> contesta;
		contesta=db.executeQueryPojo(Externo.class, sql,dNI);
		if (!contesta.isEmpty())
			return contesta.get(0); 
		return null;
}

public ColegiadoAltaEntity getMiembroID(String id) {
	String sql="select * from colegiado WHERE numero_colegiado=? and(estado='Colegiado' or estado='PreColegiado')";
	List<ColegiadoAltaEntity> contesta;
	contesta=db.executeQueryPojo(ColegiadoAltaEntity.class, sql,id);
	if (!contesta.isEmpty())
		return contesta.get(0);
	return null;
}

public InscripcionEntity ObtenInscripcion(CursoEntity curso, String DNI) {
	String sql="select * from inscripcion inner join colegiado on(id_colegiado=numero_colegiado) WHERE inscripcion.id_curso=? and colegiado.dni=?";
	List<InscripcionEntity> inscripcionData=db.executeQueryPojo(InscripcionEntity.class, sql,curso.getId_curso(), DNI);
	if (!inscripcionData.isEmpty())
		return inscripcionData.get(0);
	return null;
}

public CursoEntity ObtenCurso(int id_curso) {
	String sql="select * from curso WHERE id_curso=?";
	List<CursoEntity> inscripcionData=db.executeQueryPojo(CursoEntity.class, sql, id_curso);
	if (!inscripcionData.isEmpty())
		return inscripcionData.get(0);
	return null;
}

public void inscribirTemporal(Persona miembro, CursoEntity curso, String tipo) {
	String sql;
	ColegiadoAltaEntity colegiado;
	Externo externo;
	switch (tipo) {
	case "colegiado":
		colegiado = (ColegiadoAltaEntity) miembro;
		sql="insert into inscripcion (id_colegiado, id_curso, precio, fecha_inscripcion, estado) "
				+ "values('"+colegiado.getNumero_Colegiado()+"',"+curso.getId_curso()+","+curso.getPrecio_colegiado()+",current_date,'Pre-inscrito')";
		db.executeUpdate(sql);
		break;
	case "precolegiado":
		colegiado = (ColegiadoAltaEntity) miembro;
		sql="insert into inscripcion (id_colegiado, id_curso, precio, fecha_inscripcion, estado) "
				+ "values('"+colegiado.getNumero_Colegiado()+"',"+curso.getId_curso()+","+curso.getPrecio_precolegiado()+",current_date,'Pre-inscrito')";
		db.executeUpdate(sql);
		break;
	case "estudiante":
		externo = (Externo) miembro;
		sql="insert into externo (id_curso,dni,nombre,fecha,estado,estado_inscripcion,precio) "
				+ "values(?,?,?,current_date,?,?,?)";
		db.executeUpdate(sql, curso.getId_curso(), externo.getDni(), externo.getNombre(), tipo, "Pre-inscrito", curso.getPrecio_estudiante());
		break;
	case "externo":
		externo = (Externo) miembro;
		sql="insert into externo (id_curso,dni,nombre,fecha,estado,estado_inscripcion,precio) "
				+ "values(?,?,?,current_date,?,?,?)";
		db.executeUpdate(sql, curso.getId_curso(), externo.getDni(), externo.getNombre(), tipo, "Pre-inscrito", curso.getPrecio_pers_autorizado());
		break;
	}
}

public void inscribirDefinitivo(Persona miembro, CursoEntity curso, String tipo) {
	String sql;
	ColegiadoAltaEntity colegiado;
	Externo externo;
	switch (tipo) {
	case "colegiado":
		colegiado = (ColegiadoAltaEntity) miembro;
		sql="insert into inscripcion (id_colegiado, id_curso, precio, fecha_inscripcion, estado) "
				+ "values("+colegiado.getNumero_Colegiado()+","+curso.getId_curso()+","+curso.getPrecio_colegiado()+",current_date,'Pagado')";
		db.executeUpdate(sql);
		break;
	case "precolegiado":
		colegiado = (ColegiadoAltaEntity) miembro;
		sql="insert into inscripcion (id_colegiado, id_curso, precio, fecha_inscripcion, estado) "
				+ "values('"+colegiado.getNumero_Colegiado()+"',"+curso.getId_curso()+","+curso.getPrecio_precolegiado()+",current_date,'Pagado')";
		db.executeUpdate(sql);
		break;
	case "estudiante":
		externo = (Externo) miembro;
		sql="insert into externo (id_curso,dni,nombre,fecha,estado,estado_inscripcion,precio) "
				+ "values(?,?,?,current_date,?,?,?)";
		db.executeUpdate(sql, curso.getId_curso(), externo.getDni(), externo.getNombre(), tipo, "Pagado", curso.getPrecio_estudiante());
		break;
	case "externo":
		externo = (Externo) miembro;
		sql="insert into externo (id_curso,dni,nombre,fecha,estado,estado_inscripcion,precio) "
				+ "values(?,?,?,current_date,?,?,?)";
		db.executeUpdate(sql, curso.getId_curso(), externo.getDni(), externo.getNombre(), tipo, "Pagado", curso.getPrecio_pers_autorizado());
		break;
	}
}
}
