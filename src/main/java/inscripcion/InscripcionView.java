package inscripcion;

import javax.swing.JFrame;
import javax.swing.JPanel;
import javax.swing.border.EmptyBorder;

import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JButton;
import javax.swing.JScrollPane;
import javax.swing.JTable;
import javax.swing.ListSelectionModel;
import java.awt.event.ActionListener;
import java.awt.event.ActionEvent;
import java.awt.event.FocusAdapter;
import java.awt.event.FocusEvent;
import javax.swing.JRadioButton;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;

public class InscripcionView extends JFrame {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	private JPanel contentPane;
	protected JTable table;
	protected InscripcionController controlador;
	private JButton btnInscribirse;
	protected JRadioButton RbAñoEnCurso;
	/**
	 * Create the frame.
	 */
	public InscripcionView() {
		setTitle("Inscripcion en cursos");
		setLocationRelativeTo(null);
		setVisible(true);
		
		setDefaultCloseOperation(JFrame.DISPOSE_ON_CLOSE);
		setBounds(100, 100, 731, 365);
		contentPane = new JPanel();
		contentPane.setBorder(new EmptyBorder(5, 5, 5, 5));
		setContentPane(contentPane);
		contentPane.setLayout(null);
		{
			JLabel lblNewLabel = new JLabel("Cursos disponibles: ");
			lblNewLabel.setBounds(10, 8, 133, 21);
			contentPane.add(lblNewLabel);
		}
		{
			JScrollPane scrollPane = new JScrollPane();
			scrollPane.addFocusListener(new FocusAdapter() {
				@Override
				public void focusLost(FocusEvent e) {
					btnInscribirse.setEnabled(false);
				}
			});
			scrollPane.setBounds(10, 40, 685, 239);
			contentPane.add(scrollPane);
			{
				table = new JTable();
				table.addFocusListener(new FocusAdapter() {
					@Override
					public void focusGained(FocusEvent e) {
						btnInscribirse.setEnabled(true);
					}
				});
				table.setSelectionMode(ListSelectionModel.SINGLE_SELECTION);
				scrollPane.setViewportView(table);
				table.setName("tabDetalle");
				{
					btnInscribirse = new JButton("Inscribirse");
					btnInscribirse.addActionListener(new ActionListener() {
						public void actionPerformed(ActionEvent e) {
							if (table.getSelectedRow()!=-1)
								if (table.getModel().getValueAt(table.getSelectedRow(), 11).equals("Planificada"))
									CursoNoAbierto();
								else
									controlador.inscribir(table.getSelectedRow());
							else
								FilaNoSeleccionada();
						}
						});
					btnInscribirse.setEnabled(false);
					btnInscribirse.setBounds(588, 290, 107, 23);
					contentPane.add(btnInscribirse);
				}
				{
					RbAñoEnCurso = new JRadioButton("Mostrar solo año en curso");
					RbAñoEnCurso.addMouseListener(new MouseAdapter() {
						@Override
						public void mouseClicked(MouseEvent e) {
							if (RbAñoEnCurso.isSelected())
								controlador.actualizarTablaAñoActual();
							else
								controlador.actualizarTabla();
						}
					});
					RbAñoEnCurso.setBounds(135, 7, 219, 23);
					contentPane.add(RbAñoEnCurso);
				}
				table.setDefaultEditor(Object.class, null); //readonly;
			}
		}
	}
	
	private void FilaNoSeleccionada() {
		JOptionPane.showMessageDialog(this, "Seleccione un curso");							
	}

	public void NoEsUnNumeroException() {
		JOptionPane.showMessageDialog(this, "Introduzca un número de colegiado válido");
		
	}

	public void NoExisteElColegiado() {
		JOptionPane.showMessageDialog(this, "No existe el colegiado buscado");
	}

	public void YaEstaInscrito() {
		JOptionPane.showMessageDialog(this, "Ya está inscrito en el curso");
		
	}
	
	private void CursoNoAbierto() {
		JOptionPane.showMessageDialog(this, "No puede inscribirse en un curso que aun no está abierto");
		
	}
}
