package inscripcion;

import java.util.Date;

import Colegiado.ColegiadoAltaEntity;
import cursos.CursoEntity;

public class PagoCursosControler {
	
	PagoCursosView PagoVista;
	InscripcionModel modelo;
	private Resguardo resguardo;
	private Persona miembro;
	private String tipo;

	public PagoCursosControler(int id_curso, String id, String nombre, String tipo) {
		PagoVista = new PagoCursosView(this);
		modelo = new InscripcionModel();
		resguardo = new Resguardo();
		this.tipo = tipo; 
		
		CursoEntity curso = modelo.ObtenCurso(id_curso);
		String precio = null;
		switch (tipo) {
		case "colegiado":
			miembro = (ColegiadoAltaEntity) modelo.getMiembroID(id);
			precio = curso.getPrecio_colegiado();
			break;
		case "precolegiado":
			miembro = (ColegiadoAltaEntity) modelo.getMiembroID(id);
			precio = curso.getPrecio_precolegiado();
			break;
		case "estudiante":
			precio = curso.getPrecio_estudiante();
			miembro = new Externo(id_curso, id, nombre, new Date().toString(), tipo, "provisional",Integer.parseInt(precio));
			break;
		case "externo":
			precio = curso.getPrecio_pers_autorizado();
			miembro = new Externo(id_curso, id, nombre, new Date().toString(), tipo, "provisional",Integer.parseInt(precio));
			break;
		}
		PagoVista.init(curso, precio);
		PagoVista.init(modelo.ObtenCurso(id_curso), precio);
	}
	
	void mostrarResguardo(CursoEntity curso) {
		String precio = null;
		switch (tipo) {
		case "colegiado":
			precio = curso.getPrecio_colegiado();
			break;
		case "precolegiado":
			precio = curso.getPrecio_precolegiado();
			break;
		case "estudiante":
			precio = curso.getPrecio_estudiante();
			break;
		case "externo":
			precio = curso.getPrecio_pers_autorizado();
			break;
		}
		resguardo.pintaResguardo(miembro.getNombre(), miembro.getApellidos(), curso,  new Date().toString(), precio);
	}

	public void mostrarResguardoTarjeta(CursoEntity curso) {
		String precio = null;
		switch (tipo) {
		case "colegiado":
			precio = curso.getPrecio_colegiado();
			break;
		case "precolegiado":
			precio = curso.getPrecio_precolegiado();
			break;
		case "estudiante":
			precio = curso.getPrecio_estudiante();
			break;
		case "externo":
			precio = curso.getPrecio_pers_autorizado();
			break;
		}
		resguardo.pintaResguardoTarjeta(miembro.getNombre(), miembro.getApellidos(), curso, new Date().toString(), precio);
	}

	public void inscribirTemporal(CursoEntity curso) {
		modelo.inscribirTemporal(miembro, curso, tipo);		
	}

	public void inscribirDefinitivo(CursoEntity curso) {
		modelo.inscribirDefinitivo(miembro, curso, tipo);		
		
	}

}
