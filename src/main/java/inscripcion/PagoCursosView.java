package inscripcion;

import java.awt.EventQueue;

import javax.swing.JFrame;
import javax.swing.JPanel;
import javax.swing.border.EmptyBorder;

import cursos.CursoEntity;

import javax.swing.JLabel;
import javax.swing.JSeparator;
import javax.swing.JButton;
import java.awt.event.ActionListener;
import java.text.ParseException;
import java.awt.event.ActionEvent;

public class PagoCursosView extends JFrame {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	private JPanel contentPane;
	private PagoCursosControler pagoControlador;
	private JLabel lbPrecio;
	private JLabel lbAsignatura;
	private CursoEntity curso;

	/**
	 * Launch the application.
	 */
	public static void main(String[] args) {
		EventQueue.invokeLater(new Runnable() {
			public void run() {
				try {
					PagoCursosView frame = new PagoCursosView();
					frame.setVisible(true);
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		});
	}

	/**
	 * Create the frame.
	 */
	public PagoCursosView() {
		setResizable(false);
		setTitle("Pago curso");
		setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		setBounds(100, 100, 362, 230);
		setLocationRelativeTo(null);
		contentPane = new JPanel();
		contentPane.setBorder(new EmptyBorder(5, 5, 5, 5));
		setContentPane(contentPane);
		contentPane.setLayout(null);
		{
			JLabel lblNewLabel = new JLabel("Va a inscribirse en:");
			lblNewLabel.setBounds(10, 11, 122, 21);
			contentPane.add(lblNewLabel);
		}
		{
			JLabel lblNewLabel_1 = new JLabel("Precio:");
			lblNewLabel_1.setBounds(10, 43, 52, 21);
			contentPane.add(lblNewLabel_1);
		}
		{
			JSeparator separator = new JSeparator();
			separator.setBounds(10, 75, 326, 11);
			contentPane.add(separator);
		}
		{
			JLabel lblSeleccioneFormaDe = new JLabel("Seleccione forma de pago:");
			lblSeleccioneFormaDe.setBounds(10, 97, 166, 21);
			contentPane.add(lblSeleccioneFormaDe);
		}
		{
			JButton btTarjeta = new JButton("Tarjeta");
			btTarjeta.addActionListener(new ActionListener() {
				public void actionPerformed(ActionEvent e) {
					try {
						PagoTarjeta pago = new PagoTarjeta();
						if (pago.ok) {
							pagoControlador.inscribirDefinitivo(curso);
							pagoControlador.mostrarResguardoTarjeta(curso);
						}
						dispose();
					} catch (ParseException e1) {
						e1.printStackTrace();
					}
				}
			});
			btTarjeta.setBounds(10, 150, 89, 30);
			contentPane.add(btTarjeta);
		}
		{
			JButton btTransferenciaBancaria = new JButton("Transferencia bancaria");
			btTransferenciaBancaria.addActionListener(new ActionListener() {
				public void actionPerformed(ActionEvent e) {
					pagoControlador.inscribirTemporal(curso);
					pagoControlador.mostrarResguardo(curso);
					dispose();
				}
			});
			btTransferenciaBancaria.setBounds(142, 150, 194, 30);
			contentPane.add(btTransferenciaBancaria);
		}
		{
			lbPrecio = new JLabel("New label");
			lbPrecio.setBounds(65, 43, 67, 21);
			contentPane.add(lbPrecio);
		}
		{
			lbAsignatura = new JLabel("New label");
			lbAsignatura.setBounds(142, 11, 194, 21);
			contentPane.add(lbAsignatura);
		}
	}

	public PagoCursosView(PagoCursosControler pagoCursosControler) {
		this();
		pagoControlador = pagoCursosControler;
	}

	public void init(CursoEntity curso, String precio_colegiado) {
		this.curso = curso;
		lbAsignatura.setText(curso.getTitulo());
		lbPrecio.setText(precio_colegiado);
		setVisible(true);
	}
}
