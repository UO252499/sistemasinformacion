package inscripcion;

import java.awt.BorderLayout;
import javax.swing.JButton;
import javax.swing.JDialog;
import javax.swing.JPanel;
import javax.swing.border.EmptyBorder;
import javax.swing.text.MaskFormatter;

import util.Util;

import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JSeparator;
import java.awt.event.ActionListener;
import java.text.ParseException;
import java.util.Date;
import java.awt.event.ActionEvent;
import javax.swing.JFormattedTextField;
import java.awt.event.KeyAdapter;
import java.awt.event.KeyEvent;

public class PagoTarjeta extends JDialog {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	private final JPanel contentPanel = new JPanel();
	private JFormattedTextField ftfNumero;
	private JFormattedTextField ftfCVV;
	private JFormattedTextField ftfFecha;
	boolean ok = false;
	private JButton okButton;

	/**
	 * Launch the application.
	 */
	public static void main(String[] args) {
		try {
			PagoTarjeta dialog = new PagoTarjeta();
			dialog.setDefaultCloseOperation(JDialog.DISPOSE_ON_CLOSE);
			dialog.setVisible(true);
		} catch (Exception e) {
			e.printStackTrace();
		}
	}
	
	public PagoTarjeta() throws ParseException {
		setModal(true);
		setTitle("Pago tarjeta");
		setBounds(100, 100, 346, 225);
		setLocationRelativeTo(null);
		getContentPane().setLayout(new BorderLayout());
		contentPanel.setBorder(new EmptyBorder(5, 5, 5, 5));
		getContentPane().add(contentPanel, BorderLayout.CENTER);
		contentPanel.setLayout(null);
		ftfNumero = new JFormattedTextField(new MaskFormatter("#### #### #### ####"));
		ftfFecha = new JFormattedTextField(new MaskFormatter("####/##/##"));
		ftfCVV = new JFormattedTextField(new MaskFormatter("###"));
		{
			JLabel lblNmeroDeTarjeta = new JLabel("Número de tarjeta:");
			lblNmeroDeTarjeta.setBounds(10, 11, 113, 26);
			contentPanel.add(lblNmeroDeTarjeta);
		}
		{
			JLabel lblFechaDeValidez = new JLabel("Fecha de validez(aaaa/mm/dd):");
			lblFechaDeValidez.setBounds(10, 48, 172, 26);
			contentPanel.add(lblFechaDeValidez);
		}
		{
			JLabel lblCvv = new JLabel("CVV:");
			lblCvv.setBounds(10, 85, 47, 26);
			contentPanel.add(lblCvv);
		}
		{
			JSeparator separator = new JSeparator();
			separator.setBounds(10, 139, 313, 2);
			contentPanel.add(separator);
		}
		{
			okButton = new JButton("Pagar");
			okButton.addActionListener(new ActionListener() {

				public void actionPerformed(ActionEvent e) {
					String modified = ftfFecha.getText().replace("/", "-");
					Date fechaImp=Util.isoStringToDate(modified);
					if(fechaImp.before(new Date())) {
						fechaNoValida();
					}
					else {
						ok = true;
						dispose();
					}
				}
			});
			okButton.setEnabled(false);
			okButton.setBounds(121, 152, 96, 23);
			contentPanel.add(okButton);
			okButton.setActionCommand("OK");
			getRootPane().setDefaultButton(okButton);
		}
		{
			JButton cancelButton = new JButton("Cancelar");
			cancelButton.addActionListener(new ActionListener() {
				public void actionPerformed(ActionEvent e) {
					dispose();
				}
			});
			cancelButton.setBounds(227, 152, 96, 23);
			contentPanel.add(cancelButton);
			cancelButton.setActionCommand("Cancel");
		}
		{
			
			ftfNumero.addKeyListener(new KeyAdapter() {
				@Override
				public void keyTyped(KeyEvent e) {
					comprobarValidez();
				}
			});
			ftfNumero.setBounds(189, 14, 134, 20);
			contentPanel.add(ftfNumero);
		}
		{
			ftfFecha.addKeyListener(new KeyAdapter() {
				public void keyTyped(KeyEvent e) {
					comprobarValidez();
				}
			});
			ftfFecha.setBounds(189, 51, 75, 20);
			contentPanel.add(ftfFecha);
		}
		{
			ftfCVV.addKeyListener(new KeyAdapter() {
				@Override
				public void keyReleased(KeyEvent e) {
					comprobarValidez();
				}
			});
			ftfCVV.setBounds(189, 88, 34, 20);
			contentPanel.add(ftfCVV);
		}
		setVisible(true);
	}

	protected void comprobarValidez() {
		if (ftfNumero.getText().endsWith(" ") || ftfFecha.getText().endsWith(" ") || ftfCVV.getText().endsWith(" ")) {
			okButton.setEnabled(false);
		}
		else
			okButton.setEnabled(true);		
	}

	protected void fechaNoValida() {
		JOptionPane.showMessageDialog(this, "La fecha debe ser posterior a la actual");			
	}
}
