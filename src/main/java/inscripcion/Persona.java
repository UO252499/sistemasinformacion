package inscripcion;

public class Persona {
	
	String Nombre;
	String Apellidos;
	String dni;
	String estado;
	
	public Persona(String nombre2, String dni2, String estado2) {
		Nombre = nombre2;
		dni = dni2;
		estado = estado2;
	}
	public Persona() {
	}
	public String getNombre() {
		return Nombre;
	}
	public void setNombre(String nombre) {
		Nombre = nombre;
	}
	public String getApellidos() {
		return Apellidos;
	}
	public void setApellidos(String apellidos) {
		Apellidos = apellidos;
	}
	public String getDni() {
		return dni;
	}
	public void setDni(String dni) {
		this.dni = dni;
	}
	public String getEstado() {
		return estado;
	}
	public void setEstado(String estado) {
		this.estado = estado;
	}
}