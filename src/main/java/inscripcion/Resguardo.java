package inscripcion;

import javax.swing.JDialog;
import cursos.CursoEntity;
import javax.swing.JLabel;
import javax.swing.JSeparator;

import java.awt.Font;
import java.awt.Color;
import javax.swing.JButton;
import java.awt.event.ActionListener;
import java.awt.event.ActionEvent;

public class Resguardo extends JDialog {

	/**
	 * 
	 */
	private static final long serialVersionUID = -3851959033465579724L;
	private JLabel lb1;
	private JLabel lblDisponeDeh;
	private JSeparator separator;
	private JLabel lblNmeroDeCuenta;
	private JLabel lbPrecio;
	private JLabel lbFecha;
	private JLabel lbNombre;
	private JLabel lbCurso;
	private JLabel lblNewLabel_3;

	/**
	 * Create the dialog.
	 */
	public Resguardo() {
		setDefaultCloseOperation(JDialog.DISPOSE_ON_CLOSE);
		setModal(true);
		setTitle("Resguardo de inscripción provisional");
		setBounds(100, 100, 444, 315);
		setLocationRelativeTo(null);
		getContentPane().setLayout(null);
		{
			lb1 = new JLabel("Nombre: \t");
			lb1.setBounds(10, 11, 57, 27);
			getContentPane().add(lb1);
		}
		{
			 lbFecha = new JLabel("");
			 lbFecha.setFont(new Font("Tahoma", Font.BOLD, 12));
			lbFecha.setBounds(116, 87, 308, 27);
			getContentPane().add(lbFecha);
		}
		{
			 lbPrecio = new JLabel("");
			 lbPrecio.setFont(new Font("Tahoma", Font.BOLD, 15));
			lbPrecio.setBounds(146, 173, 65, 21);
			getContentPane().add(lbPrecio);
		}
		{
			 lblNmeroDeCuenta = new JLabel("Número de cuenta: ES66 0019 0020 9612 3456 7890");
			lblNmeroDeCuenta.setBounds(10, 135, 414, 27);
			getContentPane().add(lblNmeroDeCuenta);
		}
		{
			 separator = new JSeparator();
			separator.setBounds(12, 125, 412, 11);
			getContentPane().add(separator);
		}
		{
			 lblDisponeDeh = new JLabel("Dispone de 48h para realizar el pago o será cancelada su preinscripción");
			lblDisponeDeh.setForeground(Color.RED);
			lblDisponeDeh.setFont(new Font("Tahoma", Font.BOLD, 11));
			lblDisponeDeh.setBounds(10, 205, 414, 27);
			getContentPane().add(lblDisponeDeh);
		}
		{
			lbNombre = new JLabel("");
			lbNombre.setFont(new Font("Tahoma", Font.BOLD, 12));
			lbNombre.setBounds(116, 11, 302, 27);
			getContentPane().add(lbNombre);
		}
		{
			JLabel lblNewLabel_2 = new JLabel("Fecha solicitud: \t");
			lblNewLabel_2.setBounds(12, 87, 94, 27);
			getContentPane().add(lblNewLabel_2);
		}
		{
			lblNewLabel_3 = new JLabel("Cantidad a abonar: \t");
			lblNewLabel_3.setBounds(10, 173, 120, 21);
			getContentPane().add(lblNewLabel_3);
		}
		{
			JLabel lblNewLabel_4 = new JLabel("Curso: ");
			lblNewLabel_4.setBounds(10, 49, 45, 27);
			getContentPane().add(lblNewLabel_4);
		}
		{
			lbCurso = new JLabel("");
			lbCurso.setFont(new Font("Tahoma", Font.BOLD, 12));
			lbCurso.setBounds(116, 49, 308, 27);
			getContentPane().add(lbCurso);
		}
		{
			JButton btnAceptar = new JButton("Aceptar");
			btnAceptar.addActionListener(new ActionListener() {
				public void actionPerformed(ActionEvent e) {
					dispose();
				}
			});
			btnAceptar.setBounds(184, 243, 89, 23);
			getContentPane().add(btnAceptar);
		}
	}

	public void pintaResguardo(String nombre, String Apellidos, CursoEntity curso, String fecha_inscripcion, String precio) {
		lbNombre.setText(nombre + " " + Apellidos);
		lbFecha.setText(fecha_inscripcion);
		lbPrecio.setText(precio+"€");
		lbCurso.setText(curso.getTitulo());
		setVisible(true);
	}

	public void pintaResguardoTarjeta(String nombre, String apellidos, CursoEntity curso,
			String fecha_inscripcion, String precio) {
		lblNewLabel_3.setText("Cantidad abonada");
		lblDisponeDeh.setText("");
		lblNmeroDeCuenta.setText("");
		lbNombre.setText(nombre + " " + apellidos);
		lbFecha.setText(fecha_inscripcion);
		lbPrecio.setText(precio+"€");
		lbCurso.setText(curso.getTitulo());
		setVisible(true);
	}
}
