package util;

import java.awt.EventQueue;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JPanel;
import javax.swing.border.EmptyBorder;

import Colegiado.ColegiadoAltaController;
import Colegiado.ColegiadoAltaModel;
import Colegiado.ColegiadoAltaView;
import InscripcionPeritos.InscripcionPeritosController;
import InscripcionPeritos.InscripcionPeritosModel;
import InscripcionPeritos.InscripcionPeritosView;
import inscripcion.InscripcionController;
import inscripcion.InscripcionModel;
import inscripcion.InscripcionView;

public class ColegiadoView extends JFrame {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	private JPanel contentPane;

	/**
	 * Launch the application.
	 */
	public static void main(String[] args) {
		EventQueue.invokeLater(new Runnable() {
			public void run() {
				try {
					ColegiadoView frame = new ColegiadoView();
					frame.setVisible(true);
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		});
	}

	/**
	 * Create the frame.
	 */
	public ColegiadoView() {
		setTitle("Miembros");
		setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		setBounds(100, 100, 263, 185);
		contentPane = new JPanel();
		contentPane.setBorder(new EmptyBorder(5, 5, 5, 5));
		setContentPane(contentPane);
		contentPane.setLayout(null);
		JButton BColegiado = new JButton("Darse de alta");
		BColegiado.setBounds(10, 11, 209, 31);
		setLocationRelativeTo(null);
		BColegiado.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				ColegiadoAltaController colegiado=new ColegiadoAltaController(new ColegiadoAltaModel(), new ColegiadoAltaView());
				colegiado.initController();
			}
		});
		getContentPane().setLayout(null);
		getContentPane().add(BColegiado);
		
		JButton btnEjecutarInscripcion = new JButton("Inscribirse en un curso");
		btnEjecutarInscripcion.setBounds(10, 95, 209, 31);
		btnEjecutarInscripcion.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				InscripcionController controller= new InscripcionController(new InscripcionModel(), new InscripcionView());
				controller.initController();
			}
		});
		getContentPane().add(btnEjecutarInscripcion);
		JButton BInscripcionPeritos = new JButton("Inscribirse como perito");
		BInscripcionPeritos.setBounds(10, 53, 209, 31);
		BInscripcionPeritos.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				InscripcionPeritosController peritos=new InscripcionPeritosController(new InscripcionPeritosModel(), new InscripcionPeritosView());
				peritos.initController();
			}
		});
		getContentPane().add(BInscripcionPeritos);
	}

}
