package util;

import java.awt.EventQueue;
import java.awt.Font;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JLabel;

import AltaCursos.AltaControler;
import AltaCursos.AltaView;
import AsignarPericialesPeritos.AsignarPericialesPeritosController;
import AsignarPericialesPeritos.AsignarPericialesPeritosModel;
import AsignarPericialesPeritos.AsignarPericialesPeritosView;
import Colegiado.GestionCuotasController;
import Colegiado.GestionCuotasModel;
import Colegiado.GestionCuotasView;
import Colegiado.GestionSolicitudController;
import Colegiado.GestionSolicitudModel;
import Colegiado.GestionSolicitudView;
import cursos.CancelarCursoController;
import cursos.CancelarCursoView;
import cursos.CursoController;
import cursos.CursosModel;
import cursos.CursosView;
import inscripcion.InscripcionModel;
import javax.swing.JSeparator;

public class GestionView {

	private JFrame frmGestinAfministrativa;

	/**
	 * Launch the application.
	 */
	public static void main(String[] args) {
		EventQueue.invokeLater(new Runnable() {
			public void run() {
				try {
					GestionView window = new GestionView();
					window.frmGestinAfministrativa.setVisible(true);
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		});
	}

	/**
	 * Create the application.
	 */
	public GestionView() {
		initialize();
		
		crearBotonPlanificar();
		crearBotonGestionar();
		crearBotonCancelar();
		crearBotonGestionarSolicitudes();
		crearBotonCuotas();
		crearBotonAsignarPericiales();
	}

	/**
	 * Initialize the contents of the frame.
	 */
	private void initialize() {
		frmGestinAfministrativa = new JFrame();
		frmGestinAfministrativa.setTitle("Gestión afministrativa");
		frmGestinAfministrativa.setBounds(100, 100, 435, 279);
		frmGestinAfministrativa.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		frmGestinAfministrativa.getContentPane().setLayout(null);
		frmGestinAfministrativa.setLocationRelativeTo(null);
	}

	public void setVisible(boolean b) {
		frmGestinAfministrativa.setVisible(true);
	}
	
	private void crearBotonPlanificar() {
		JButton btnEjecutarCursos = new JButton("Planificar curso");
		btnEjecutarCursos.setBounds(10, 51, 191, 29);
		btnEjecutarCursos.addActionListener(new ActionListener() { //NOSONAR codigo autogenerado
			public void actionPerformed(ActionEvent e) {
				CursoController controller= new CursoController(new CursosModel(), new CursosView());
				controller.initController();
			}
		});
		frmGestinAfministrativa.getContentPane().setLayout(null);
		frmGestinAfministrativa.getContentPane().add(btnEjecutarCursos);		
	}
	
	private void crearBotonGestionar() {
	JButton btnDarDeAlta = new JButton("Gestionar Cursos");
	btnDarDeAlta.setBounds(214, 91, 191, 31);
	btnDarDeAlta.addActionListener(new ActionListener() {
		public void actionPerformed(ActionEvent e) {
			AltaControler controller= new AltaControler(new InscripcionModel(), new AltaView());
			controller.initController();
		}
	});
	frmGestinAfministrativa.getContentPane().add(btnDarDeAlta);
	}
	
	private void crearBotonCancelar() {
		JButton btnDarDeBaja = new JButton("Cancelar Cursos");
		btnDarDeBaja.setBounds(10, 91, 191, 31);
		btnDarDeBaja.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				CancelarCursoController controller=new CancelarCursoController(new CursosModel(), new CancelarCursoView());
				controller.initController();
			}
		});
		frmGestinAfministrativa.getContentPane().add(btnDarDeBaja);
	}
	
	private void crearBotonGestionarSolicitudes() {
	JButton btnGestionSol = new JButton("Gestionar Solicitudes");
	btnGestionSol.setBounds(214, 51, 191, 29);
	btnGestionSol.addActionListener(new ActionListener() {
		public void actionPerformed(ActionEvent e) {
			GestionSolicitudController controller=
					new GestionSolicitudController(new GestionSolicitudModel(), new GestionSolicitudView());
			controller.initController();
		}
	});
	frmGestinAfministrativa.getContentPane().add(btnGestionSol);
	}
	
	private void crearBotonCuotas() {
	JButton BtnCuotas = new JButton("Gestionar Cuotas");
	BtnCuotas.setBounds(10, 133, 191, 30);
	BtnCuotas.addActionListener(new ActionListener() {
		public void actionPerformed(ActionEvent e) {
			GestionCuotasController controller=new GestionCuotasController(new GestionCuotasModel(), new GestionCuotasView());
			controller.initController();
		}
	});
	frmGestinAfministrativa.getContentPane().add(BtnCuotas);
	}
	
	private void crearBotonAsignarPericiales() {
	JButton BAsignar = new JButton("Aginar Periciales a Peritos");
	BAsignar.setBounds(214, 198, 191, 30);
	BAsignar.addActionListener(new ActionListener() {
		public void actionPerformed(ActionEvent e) {
			AsignarPericialesPeritosController asignar=new  AsignarPericialesPeritosController(new  AsignarPericialesPeritosModel(), new  AsignarPericialesPeritosView());
			asignar.initController();
		}
	});
	frmGestinAfministrativa.getContentPane().add(BAsignar);
	{
		JSeparator separator = new JSeparator();
		separator.setBounds(10, 174, 395, 13);
		frmGestinAfministrativa.getContentPane().add(separator);
	}
	{
		JLabel lblCursos = new JLabel("Cursos:");
		lblCursos.setFont(new Font("Tahoma", Font.PLAIN, 16));
		lblCursos.setBounds(10, 11, 70, 29);
		frmGestinAfministrativa.getContentPane().add(lblCursos);
	}
	{
		JLabel lblPericiales = new JLabel("Periciales:");
		lblPericiales.setFont(new Font("Tahoma", Font.PLAIN, 16));
		lblPericiales.setBounds(10, 196, 101, 31);
		frmGestinAfministrativa.getContentPane().add(lblPericiales);
	}
	}
}
