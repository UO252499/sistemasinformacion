package util;

import java.awt.EventQueue;
import javax.swing.JFrame;
import javax.swing.ImageIcon;
import javax.swing.JButton;
import java.awt.event.ActionListener;
import java.awt.event.ActionEvent;
import SolicitudInformePericial.SolicitudInformePericialController;
import SolicitudInformePericial.SolicitudInformePericialModel;
import SolicitudInformePericial.SolicitudInformePericialView;

import javax.swing.JSeparator;
import javax.swing.UIManager;
import javax.swing.UnsupportedLookAndFeelException;
import javax.swing.JLabel;
import java.awt.Font;
import java.awt.Graphics;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;
import java.awt.font.TextAttribute;
import java.util.Map;
import java.awt.Color;
import java.awt.Dimension;
import javax.swing.JPanel;
import java.awt.Toolkit;
import javax.swing.JMenuBar;
import javax.swing.JMenu;
import javax.swing.JMenuItem;

/**
 * Punto de entrada principal que incluye botones para la ejecucion de las pantallas 
 * de las aplicaciones de ejemplo
 * y acciones de inicializacion de la base de datos.
 * No sigue MVC pues es solamente temporal para que durante el desarrollo se tenga posibilidad
 * de realizar acciones de inicializacion
 */
public class SwingMain {

	private JFrame frmCoiipa;

	/**
	 * Launch the application.
	 */
	public static void main(String[] args) {
		EventQueue.invokeLater(new Runnable() { //NOSONAR codigo autogenerado
			public void run() {
				try {
					SwingMain window = new SwingMain();
					window.frmCoiipa.setVisible(true);
				} catch (Exception e) {
					e.printStackTrace(); //NOSONAR codigo autogenerado
				}
			}
		});
	}

	/**
	 * Create the application.
	 */
	public SwingMain() {
		initialize();
	}

	/**
	 * Initialize the contents of the frame.
	 */
	@SuppressWarnings({ "rawtypes", "unchecked" })
	private void initialize() {
		try {
			UIManager.setLookAndFeel("com.sun.java.swing.plaf.windows.WindowsLookAndFeel");
		} catch (ClassNotFoundException | InstantiationException | IllegalAccessException
				| UnsupportedLookAndFeelException e1) {
			// TODO Auto-generated catch block
			e1.printStackTrace();
		}
		frmCoiipa = new JFrame();
		frmCoiipa.setResizable(false);
		frmCoiipa.setIconImage(Toolkit.getDefaultToolkit().getImage("C:\\Users\\joelv\\git\\sistemasinformacion\\src\\main\\resources\\coiipa.jpg"));
		frmCoiipa.setTitle("COIIPA");
		frmCoiipa.setBounds(0, 0, 548, 468);
		frmCoiipa.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		frmCoiipa.setLocationRelativeTo(null);
		frmCoiipa.getContentPane().setLayout(null);
		frmCoiipa.getContentPane().setLayout(null);
		{
			JSeparator separator = new JSeparator();
			separator.setBounds(12, 361, 503, 12);
			frmCoiipa.getContentPane().add(separator);
		}
		{
			JButton btnPersonalAdministrativo = new JButton("Personal administrativo");
			btnPersonalAdministrativo.addActionListener(new ActionListener() {
				public void actionPerformed(ActionEvent e) {
					GestionView adm = new GestionView();
					adm.setVisible(true);
					frmCoiipa.dispose();
				}
			});
			btnPersonalAdministrativo.setBounds(129, 319, 184, 31);
			frmCoiipa.getContentPane().add(btnPersonalAdministrativo);
		}
		{
			JButton btnMiembroOAlumno = new JButton("Miembro o alumno");
			btnMiembroOAlumno.addActionListener(new ActionListener() {
				public void actionPerformed(ActionEvent e) {
					new ColegiadoView().setVisible(true);
					frmCoiipa.dispose();
				}
			});
			btnMiembroOAlumno.setBounds(331, 319, 184, 31);
			frmCoiipa.getContentPane().add(btnMiembroOAlumno);
		}
		
		JLabel lblBienvenidoAlColegio = new JLabel("Bienvenido al Colegio Oficial de Ingenieros Informaticos de Asturias");
		lblBienvenidoAlColegio.setFont(new Font("Tahoma", Font.PLAIN, 16));
		lblBienvenidoAlColegio.setBounds(32, 11, 503, 31);
		frmCoiipa.getContentPane().add(lblBienvenidoAlColegio);
		
		JLabel lblCulEsTu = new JLabel("Cuál es tu rol? ");
		lblCulEsTu.setBounds(32, 319, 87, 31);
		frmCoiipa.getContentPane().add(lblCulEsTu);
		
		JLabel lblSoyUnaEmpresa = new JLabel("Soy una empresa (solicitar informe pericial)");
		lblSoyUnaEmpresa.setFont(new Font("Tahoma", Font.PLAIN, 11));
		Font font = lblSoyUnaEmpresa.getFont();
		Map attributes = font.getAttributes();
		attributes.put(TextAttribute.UNDERLINE, TextAttribute.UNDERLINE_ON);
		lblSoyUnaEmpresa.setFont(font.deriveFont(attributes));
		lblSoyUnaEmpresa.setForeground(Color.blue);
		lblSoyUnaEmpresa.addMouseListener(new MouseAdapter() {
			@Override
			public void mouseClicked(MouseEvent e) {
				SolicitudInformePericialController informepericial=new SolicitudInformePericialController(new SolicitudInformePericialModel(), new SolicitudInformePericialView());
				informepericial.initController();	
				frmCoiipa.dispose();
			}
			@Override
			public void mouseEntered(MouseEvent e) {
				lblSoyUnaEmpresa.setForeground(Color.cyan);			}
			@Override
			public void mouseExited(MouseEvent e) {
				lblSoyUnaEmpresa.setForeground(Color.blue);
			}
		});
		
		lblSoyUnaEmpresa.setBounds(302, 371, 220, 20);
		frmCoiipa.getContentPane().add(lblSoyUnaEmpresa);
		
		panelImage panel = new panelImage();
		panel.setBounds(119, 53, 301, 251);
		frmCoiipa.getContentPane().add(panel);
		
		JMenuBar menuBar = new JMenuBar();
		frmCoiipa.setJMenuBar(menuBar);
		{
			JMenu mnNewMenu = new JMenu("Desarrollador");
			menuBar.add(mnNewMenu);
			{
				JMenuItem mntmNewMenuItem = new JMenuItem("Inicializar base de datos en blanco");
				mntmNewMenuItem.addActionListener(new ActionListener() {
					public void actionPerformed(ActionEvent e) {
						Database db=new Database();
						db.createDatabase(false);
					}
				});
				mnNewMenu.add(mntmNewMenuItem);
			}
			{
				JMenuItem mntmNewMenuItem_1 = new JMenuItem("Inicializar y cargar datos de prueba");
				mntmNewMenuItem_1.addActionListener(new ActionListener() {
					public void actionPerformed(ActionEvent e) {
						Database db=new Database();
						db.createDatabase(false);
						db.loadDatabase();
					}
				});
				mnNewMenu.add(mntmNewMenuItem_1);
			}
		}
	}

	public JFrame getFrame() { return this.frmCoiipa; }
	
	public class panelImage extends JPanel{

	    /**
		 * 
		 */
		private static final long serialVersionUID = 1L;

		@Override
	    public void paint(Graphics g){
	        Dimension dimension = this.getSize();
	        ImageIcon icon = new ImageIcon(getClass().getResource("/coiipa.jpg"));
	        g.drawImage(icon.getImage(), 0, 0, dimension.width, dimension.height, null);
	        setOpaque(false);
	        super.paintChildren(g);
	    }
	}
}
