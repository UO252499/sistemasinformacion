--Datos para carga inicial de la base de datos
delete from curso;
insert into curso(id_curso,titulo,fecha_inicio_inscripcion,fecha_fin_inscripcion,fecha_curso,precio_colegiado,precio_precolegiado,precio_estudiante,precio_pers_autorizado,plazas,estado) values 
    (104,'Nuevo Curso1','2020-02-15','2020-03-15','2020-03-20',140,180,130,200,30,'Abierta'),
    (105,'Nuevo Curso2','2020-02-15','2016-03-15','2020-03-20',100,190,80,200,50,'Abierta'),
    (106,'Nuevo Curso3','2020-02-17','2020-03-20','2020-03-24',100,140,70,170,2,'Abierta'),
    (107,'Nuevo Curso4','2020-01-15','2020-03-15','2020-03-20',100,160,200,70,50,'Abierta'),
    (108,'Nuevo Curso5','2020-02-15','2020-02-19','2020-03-24',100,140,180,70,1,'Abierta'), 
    (109,'Nuevo Curso6','2020-01-15','2020-03-15','2020-03-20',100,160,180,50,50,'Abierta'),
    (110,'Nuevo Curso7','2020-02-15','2020-03-15','2020-03-20',125,130,150,120,0,'Abierta');
 
insert into curso(id_curso,titulo,fecha_curso,estado) values    
    (100,'Programacion','2020-05-01','Planificada'),
    (101,'Paradigmas','2021-05-10','Planificada'),
    (102,'Metodologia de la Prog.','2020-06-14','Planificada'),
    (103,'Algoritmia','2020-05-25','Planificada'),
    (111,'Nuevo Curso8','2020-03-20','Planificada');
    
delete from inscripcion;
insert into inscripcion(id_colegiado, id_curso, precio, fecha_inscripcion, estado) values
	(2020000001, 104, 140, '2020-03-4', 'Pre-inscrito'),
	(2020000001, 105, 100, '2020-03-25', 'Pre-inscrito'),
	(2020000002, 104, 140, '2020-03-4', 'Pre-inscrito');
    
delete from colegiado;
insert into colegiado(numero_colegiado,dni,nombre,apellidos,direccion,poblacion,telefono,titulacion,centro,añoobtencion,cuentabancaria,fecha,estado) values 
    ('2020000001','11111111C','Juan','Perez Lopez','dir1','Gijon',669751423,'Ing.Informático','EPI Gijon',2015,'12345678900000000000','01999-01-01','Colegiado'),
    ('2020000002','22222222J','Jorge','Sampedro Vazquez','dir2','Avilés',672345235,'Doctor Ing. Informatico','EPI Gijon',2015,'12345678901234567890','2009-02-01','Colegiado'),
    ('2020000003','33333333P','Adrian','Garcia Fanjul','dir3','Gijon',635732456,'Ing.Informático','EPI Gijon',1973,'12345678900987654321','2020-05-23','Pendiente'),
    ('2020000006','44444444A','Sofía','Grande Cabezón','dir4','Oviedo',622456238,'Ing.Informático','EII Oviedo',2007,'11111111111111111111','2019-04-03','Pendiente'),
    ('P2020000001','44421235Z','Tu','Great Cabezón','dir7','Oviedo',622456238,'Ing.Informático','EII Oviedo',2020,'11111112211111431111','2019-04-03','PreColegiado'),
    ('P2020000002','44430991D','Kristaz','Pozingood','dir4','Oviedo',622456238,'Ing.Informático','EII Oviedo',2019,'11111131111156111111','2019-04-03','PreColegiado'), 
    ('2020000007','55555555P','Pepe','Garcia Solares','dir3','Gijon',635732456,'Ing.Informático','EPI Gijon',1973,'12345678900987654321','2020-05-22','Pendiente'),
    ('2020000008','66666666B','Manolo','Suarez Diaz','dir3','Gijon',635732456,'Ing.Informático','EPI Gijon',1973,'12345678900987654321','2020-05-02','Pendiente'),
    ('2020000009','77777777C','Pepito','Costales Menendez','dir3','Gijon',635732456,'Ing.Informático','EPI Gijon',1973,'12345678900987654321','2020-05-03','Pendiente'),
    ('2020000010','12345678Z','Alex','Cifuentes Vazquez','dir3','Gijon',635732456,'Ing.Informático','EPI Gijon',1973,'12345678900987654321','2020-05-12','Pendiente');
    
    
delete from perito;
insert into perito(id_perito,numero_colegiado,experiencia,fecha,estado,orden) values 
    (1,'2020000001','Ey','2020-01-23','Pendiente',1),
	(2,'2020000010','Te conoci en el centro de Paris','2020-01-03','Pendiente',2),
	(3,'2020000009','con ese vestido de anis','2020-04-03','Pendiente',3),
	(5,'2020000008','aqui llego tu tiburon','2020-04-22','Pendiente',4);    
    
delete from pericial;
insert into pericial(id_pericial,nombre,telefono,correoelectronico,descripcion,fechalimite,fechaemision,estado) values
	(1,'Bogdan',985333222,'oli@uniovi.es','Buenas tardes','2020-06-02','2020-03-27','Asignado'),
	(2,'Bojdan',985333212,'oka@arcelor.es','Buenos dias',null,'2020-03-28','Pendiente'),
	(3,'Mercadona',985777666,'oli@mercaona.es','Mercadona Mercadona Mercadona','2020-12-28','2020-03-27','Pendiente'),
	(4,'Philips',985777666,'oli@mopinho.es','Soy un ingles',null,'2020-12-31','Pendiente');
	
delete from pericia;
insert into pericia(id_pericia,id_perito,id_pericial,fecha) values
	(1,1,1,'2020-05-03');

delete from profesor;
insert into profesor(dni,nombre,apellidos,poblacion,telefono) values 
    ('11111111C','Juan','Perez Lopez','Gijon',669751423),
    ('12345678B','Alex','Garcia Lopez','Oviedo',669751423),
    ('22222222D','Adrian','Diaz Vazquez','Gijon',669751423),
    ('33333333E','Sofia','Perez Lopez','Mieres',669751423),
    ('44444444F','Jorge','Lopez Perez','Gijon',669751423),
    ('55555555G','Pablo','Perez Lopez','Gijon',669751423);
    
delete from externo;
insert into externo(id_curso,dni,nombre,fecha,estado,estado_inscripcion,precio) values
	(105,'43554376G','El pana','2020-08-12','Estudiante','Pagado',80),
	(106,'43553276G','Le Externx','2020-03-8','Externo','Pre-inscrito',170);
	
--delete from recibo;
--insert into recibo(id_recibo, dni, fecha, cuota, estado, observaciones) values
	--(1, '11111111C', '2020-04-4', 40, 'pendiente','');