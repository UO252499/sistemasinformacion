--Primero se deben borrar todas las tablas (de detalle a maestro) y lugo anyadirlas (de maestro a detalle)
--(en este caso en cada una de las aplicaciones (tkrun y descuento) se usa solo una tabla, por lo que no hace falta)
drop table curso;
create table curso (id_curso int primary key not null, 
					titulo varchar(32) not null, 
					fecha_inicio_inscripcion date,
					fecha_fin_inscripcion date,
					fecha_curso not null,
					fecha_cancelacion,
					profesor varchar(9),
					recursos varchar(30),
					precio_colegiado int,
					precio_precolegiado int,
					precio_estudiante int,
					precio_pers_autorizado int, 
					plazas int, estado varchar(15),
					FOREIGN KEY (profesor) REFERENCES profesor(dni));
						
drop table inscripcion;
create table inscripcion(id_colegiado varchar(12) not null, id_curso int not null,
						precio int,
						fecha_inscripcion date not null,
						fecha_cancelacion date,
						observaciones varchar(30),
						estado varchar(20), 
						FOREIGN KEY (id_colegiado) REFERENCES colegiado(numero_colegiado),
						FOREIGN KEY (id_curso) REFERENCES curso(id_curso),
						PRIMARY KEY (id_colegiado, id_curso));
						
drop table externo;
create table externo(id_curso int not null,
					dni varchar(9) not null,
					nombre varchar(30) not null,
					fecha date not null,
					fecha_cancelacion date,
					estado varchar(20) not null,
					estado_inscripcion varchar(20) not null,
					precio int not null,
					observaciones varchar(30),
					FOREIGN KEY (id_curso) REFERENCES curso(id_curso)
					PRIMARY KEY (id_curso, dni)
					);
					
drop table recibo;
create table recibo(id_recibo int PRIMARY KEY not null,
                    dni varchar(9) not null,
                    fecha date not null,
                    cuota int not null,
                    estado varchar(20) not null,
                    observaciones varchar(40),
                    FOREIGN KEY (dni) REFERENCES colegiado(dni)
                    );
						
drop table colegiado;
create table colegiado(numero_colegiado varchar(12) primary key unique not null,
						dni varchar(9) unique not null, 
						nombre varchar(20) not null,
						apellidos varchar(40) not null,
						direccion varchar(30) not null,
						poblacion varchar(20) not null,
						telefono integer not null,
						titulacion varchar(50) not null,
						centro varchar(30) not null,
						añoobtencion integer not null,
						cuentabancaria varchar(20) not null,
						fecha date not null,
						estado varchar(30) not null
						);
						
DROP TRIGGER CursoCompleto;
					
drop table profesor;
create table profesor(dni varchar(9) primary key not null,
                        nombre varchar(20) not null,
                        apellidos varchar(40) not null,
                        poblacion varchar(20) not null,
                        telefono integer not null);
         
                        
drop table pericial;
create table pericial(id_pericial integer primary key,
						nombre varchar(20) not null,
						telefono integer not null,
						correoelectronico varchar(30) not null,
						descripcion varchar(100) not null,
						fechalimite date,
						fechaemision date not null,
						estado varchar(30) not null
						);
						
drop table perito;
create table perito(id_perito integer primary key,
						numero_colegiado varchar(9) not null,
						experiencia varchar(100) not null,
						fecha date not null,
						estado varchar(30) not null,
						orden integer not null,
						foreign key (numero_colegiado) references colegiado (numero_colegiado)
						);

drop table pericia;
create table pericia(id_pericia integer primary key,
					id_perito integer not null,
					id_pericial integer not null,
					fecha date not null,
					foreign key (id_perito) references perito (id_perito),
					foreign key (id_pericial) references pericial (id_pericial)
					);