
import org.junit.*;
import static org.junit.Assert.assertEquals;
import org.junit.rules.ExpectedException;
import org.junit.runner.RunWith;
import org.junit.runners.Parameterized;
import org.junit.runners.Parameterized.Parameter;
import org.junit.runners.Parameterized.Parameters;
import cursos.AlumnosDisplayDTO;
import cursos.CursosModel;
import util.ApplicationException;
import util.Util;
import util.Database;
import java.util.Arrays;
import java.util.Collection;
import java.util.List;

/**
 * Pruebas del ejemplo de Inscripciones en carreras populares (primer ejemplo) con JUnit4
 */
@RunWith(Parameterized.class)
public class TestAlumnosInscritos {
	private static Database db=new Database();
	
	@Before
	public void setUp() {
		db.createDatabase(true);
		loadCleanDatabase(db); 
	}
	
	@After
	public void tearDown(){
	}
	public static void loadCleanDatabase(Database db) {
		db.executeBatch(new String[] {
				"delete from curso",
				"insert into curso(id_curso,titulo,fecha_inicio_inscripcion,fecha_fin_inscripcion,fecha_curso,precio_colegiado,precio_precolegiado,precio_estudiante,precio_pers_autorizado,plazas,estado) "
				+ "values (104,'Nuevo Curso1','2020-02-15','2020-03-15','2020-03-20',140,180,130,200,30,'Abierta'),"
				+ "(105,'Nuevo Curso2','2020-02-15','2016-03-15','2020-03-20',100,190,80,200,50,'Cancelado'),"
				+ "(106,'Nuevo Curso3','2020-02-17','2020-03-20','2020-03-24',100,140,70,170,20,'Abierta'),"
				+ "(107,'Nuevo Curso4','2020-01-15','2020-03-15','2020-03-20',100,160,200,70,50,'Planificado'),"
				+ "(108,'Nuevo Curso5','2020-02-15','2020-02-19','2020-03-24',100,140,180,70,30,'Abierta'), "
				+ "(109,'Nuevo Curso6','2020-01-15','2020-03-15','2020-03-20',100,160,180,50,50,'Terminado'),"
				+ "(110,'Nuevo Curso7','2020-02-15','2020-03-15','2020-03-20',125,130,150,120,10,'Cancelado'),"
				+ "(111,'Nuevo Curso8','2020-02-15','2020-03-15','2020-03-20',125,130,150,120,20,'Abierta');",
				"delete from colegiado",
				"insert into colegiado(numero_colegiado,dni,nombre,apellidos,direccion,poblacion,telefono,titulacion,centro,añoobtencion,cuentabancaria,fecha,estado) values" 
				+ "('2020000001','11111111C','Juan','Perez Lopez','dir1','Gijon',669751423,'Ing.Informático','EPI Gijon',2015,'12345678900000000000','01999-01-01','Colegiado'),"
				+ "('2020000002','22222222J','Jorge','Sampedro Vazquez','dir2','Avilés',672345235,'Doctor Ing. Informatico','EPI Gijon',2015,'12345678901234567890','2009-02-01','Colegiado'),"
				+ "('2020000003','33333333P','Adrian','Garcia Fanjul','dir3','Gijon',635732456,'Ing.Informático','EPI Gijon',1973,'12345678900987654321','2020-05-23','Colegiado'),"
				+ "('2020000004','44444444A','Sofía','Grande Cabezón','dir4','Oviedo',622456238,'Ing.Informático','EII Oviedo',2007,'11111111111111111111','2019-04-03','Colegiado');",
				"delete from inscripcion",
				"insert into inscripcion(id_colegiado, id_curso, precio, fecha_inscripcion, estado) values"
				+ "(2020000001, 104, 140, '2020-03-15', 'Pre-inscrito'),"
				+ "(2020000002, 104, 100, '2020-03-16', 'Pre-inscrito'),"
				+ "(2020000003, 104, 100, '2020-03-16', 'Pre-inscrito'),"
				+ "(2020000001, 106, 140, '2020-03-17', 'Pagado'),"
				+ "(2020000002, 106, 100, '2020-03-17', 'Pagado'),"
				+ "(2020000001, 108, 140, '2020-03-17', 'Pre-inscrito'),"
				+ "(2020000002, 108, 140, '2020-03-17', 'Pagado'),"
				+ "(2020000003, 108, 140, '2020-03-17', 'Pre-inscrito'),"
				+ "(2020000004, 108, 140, '2020-03-17', 'Pagado'),"
				+ "(2020000001, 110, 140, '2020-03-17', 'Cancelado'),"
				+ "(2020000002, 110, 140, '2020-03-17', 'Cancelado'),"
				+ "(2020000003, 110, 140, '2020-03-17', 'Cancelado');"
		});
	}
	
	/**
	 * Comprueba la lista de alumnos que ve la secretaria en el momento en el que selecciona un curso.
	 * Debe mostrar todos los alumnos inscritos ordenados por nombre, indicando su nombre y apellidos, 
	 * la fecha de inscripcion, el estado y el precio. Si no hay alumnos inscritos una lista vacía.
	 * Con la base de datos del setUp cubre la clase de equivalencia valida con un curso abierto con
	 * inscripciones y con un curso abierto sin ellas.
	 */
	@Test
	public void testAlumnosInscritosListValido() {
		CursosModel inscr=new CursosModel();
		List<AlumnosDisplayDTO> alumnosIns=inscr.getListaAlumnos("104");
		assertEquals("el numero de alumnos mostrados es incorrecto",3,alumnosIns.size());

		assertEquals("Garcia Fanjul, Adrian,2020-03-16,Pre-inscrito,100\n"
				+"Perez Lopez, Juan,2020-03-15,Pre-inscrito,140\n" 
				+"Sampedro Vazquez, Jorge,2020-03-16,Pre-inscrito,100\n", 
        		Util.pojosToCsv(alumnosIns,new String[] {"nombre", "fecha_inscripcion", "estado", "precio"}));
		
		List<AlumnosDisplayDTO> NoAlumnosIns=inscr.getListaAlumnos("111");
		assertEquals("el numero de alumnos mostrados es incorrecto",0,NoAlumnosIns.size());
		assertEquals("", 
        		Util.pojosToCsv(NoAlumnosIns,new String[] {"nombre", "fecha_inscripcion", "estado", "precio"}));
	}
	
	/**
	 * En estos métodos se comprobaran por separado las clases invalidas de cursos existentes.
	 * A getListaAlumnos solo hay que pasarle un id de un curso, que podria ser un id invalido si es invocado incorrectmente.
	 * La salida esperada en cualquier caso es:El curso no esta abierto no admite inscripciones.
	 * Para evitar duplicacion de codigo se utiliza un metodo generico invocado desde los tres tests
	 */
	@Rule
	public ExpectedException thrown = ExpectedException.none();
	
	@Test public void testAlumnosInscritosCursoCancel() {
		AlumnosInscritosListException("105");
	}
	@Test public void testAlumnosInscritosCursoPlanif() {
		AlumnosInscritosListException("107");
	}
	@Test public void testAlumnosInscritosCursoTerm() {
		AlumnosInscritosListException("109");
	}
		
	public void AlumnosInscritosListException(String id_curso) {
		CursosModel inscr=new CursosModel();
		thrown.expect(ApplicationException.class);
		thrown.expectMessage("El curso no esta abierto no admite inscripciones");
		inscr.getListaAlumnos(id_curso);
	}
	
	/**
	 * En este método se comprueba que el id del curso pasado como parametro no existe.
	 * A getListaAlumnos solo hay que pasarle un id de un curso, que podria ser un id de un curso inexistente.
	 * La salida esperada es:El curso no existe.
	 */
	@Test public void testAlumnosInscritosCursoInex() {
		CursosModel inscr=new CursosModel();
		thrown.expect(ApplicationException.class);
		thrown.expectMessage("El curso no existe");
		inscr.getListaAlumnos("112");
	}
	
	
	
	/**
	 * En JUnit4 existe la posibilidad de crear tests parametrizdos:
	 * - definimos por separado los valores de los parametros
	 * - establecemos el mapeo de los parametros a variables que serán las utilizadas por el test
	 * - solo se necesita implementar un test, que se ejecutara tantas veces como parametros
	 * Este metodo comprueba que para las clases de equivalencia validas de los ingresos del
	 * curso salen los valores esperados.
	 * Para un curso con solo inscripciones en estado "Pre-inscrito" devolvera 0
	 * Para un curso con solo inscripciones en estado "Pagado" devolvera la suma total
	 * Para un curso con inscripciones en estado "Pre-inscrito" y "Pagado" devolvera la suma de las pagadas unicamente
	 */
	@Parameters
	public static Collection<Object[]> data() {
		return Arrays.asList(new Object[][]{
			//Comprobamos que los ingresos totales del curso para el
			//id del curso valido son los correctos
			{0,"104"},
			{240,"106"},
			{280,"108"},
		});
	}
	@Parameter(value=0) public int ingresoTot;
	@Parameter(value=1) public String idCurso;
	@Test
	public void testIngresosTotCursoValidas() {
		CursosModel inscr=new CursosModel();
		assertEquals(ingresoTot,inscr.getIngresosCurso(idCurso));
	}
	
	
	/**
	 * En este método se comprueba que el id del curso pasado como parametro esta cancelado.
	 * A getIngresosCurso solo hay que pasarle un id de un curso, que podria ser un id de un curso cancelado.
	 * Para un curso con inscripciones en estado "Cancelado" devolvera el mensaje:El curso esta cancelado
	 */
	@Test public void testIngresosTotCursoCancel() {
		CursosModel inscr=new CursosModel();
		thrown.expect(ApplicationException.class);
		thrown.expectMessage("El curso esta cancelado");
		inscr.getIngresosCurso("110");
	}

}
