
import org.junit.*;
import static org.junit.Assert.assertEquals;
import Colegiado.ColegiadoAltaModel;
import Colegiado.GestionSolicitudController;
import Colegiado.GestionSolicitudModel;
import Colegiado.GestionSolicitudView;
import util.Database;

import java.io.File;

/**
 * Pruebas del ejemplo de Inscripciones en carreras populares (primer ejemplo) con JUnit4
 */
public class TestGestionSolicitud {
	private static Database db=new Database();
	File archivo=new File("src/main/resources/prueba.csv");
	File archivoExcep=new File("src/main/resources/prueba2.csv");


	
	@Before
	public void setUp() {
		db.createDatabase(true);
		loadCleanDatabase(db); 
	}
	
	@After
	public void tearDown(){
	}
	public static void loadCleanDatabase(Database db) {
		db.executeBatch(new String[] {
				"delete from colegiado",
				"insert into colegiado(numero_colegiado,dni,nombre,apellidos,direccion,poblacion,telefono,titulacion,centro,añoobtencion,cuentabancaria,fecha,estado) values" 
				+ "('2020000001','11111111C','Juan','Perez Lopez','dir1','Gijon',669751423,'Ing.Informático','EPI Gijon',2015,'12345678900000000000','01999-01-01','Emitido'),"
				+ "('2020000002','22222222J','Jorge','Sampedro Vazquez','dir2','Avilés',672345235,'Doctor Ing. Informatico','EPI Gijon',2015,'12345678901234567890','2009-02-01','Emitido'),"
				+ "('2020000003','33333333P','Adrian','Garcia Fanjul','dir3','Gijon',635732456,'Ing.Informático','EPI Gijon',1973,'12345678900987654321','2020-05-23','Colegiado'),"
				+ "('2020000004','44444444A','Sofía','Grande Cabezón','dir4','Oviedo',622456238,'Ing.Informático','EII Oviedo',2007,'11111111111111111111','2019-04-03','Pendiente');",
		});
	}
	
	/**
	 * Comprueba para cada entrada del fichero (que se corresponde con los casos de prueba)
	 * se procesa el fichero correctamente.
	 * Si recibe una entrada con todos los campos validos el colegiado pasara de estar emitido a
	 * enviado, si por el contrario algun campo es invalido el estado del colegiado no cambiara.
	 * Para la comprobacion se utiliza la funcion obtenerColegiado que devuelve el estado de un colegiado
	 * pasandole el dni de este, asi podemos comrpbar sin analizar los ficheros devueltos que el cambio se ha producido 
	 */
	@Test
	public void testRecibirFichero() {
		GestionSolicitudController controller=new GestionSolicitudController(new GestionSolicitudModel(), new GestionSolicitudView());
		ColegiadoAltaModel coleg=new ColegiadoAltaModel();
		controller.ArchivoSeleccionado(archivo);
		assertEquals("Colegiado",coleg.ObtenerColegiado("11111111C"));
		assertEquals("Emitido",coleg.ObtenerColegiado("22222222J"));
		assertEquals("Colegiado",coleg.ObtenerColegiado("33333333P"));
		assertEquals("Pendiente",coleg.ObtenerColegiado("44444444A"));
	}
	
}
